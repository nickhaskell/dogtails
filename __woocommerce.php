<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package Shikoku_Inu
 */

get_header();
?>

<div class="template-default woocommerce-storefront">
  <div class="container pad-b-120 pad-t-30">  

    <div class="row shop-content">
      <?php if ( is_active_sidebar('customer-store-sidebar') ) : ?>
     
      <div class="col-sm-3">
        <?php the_widget('customer-store-sidebar'); ?>
      </div>
      <div class="col-sm-9">
        <?php woocommerce_content(); ?>
      </div>  
     
      <?php else: ?>

      <div class="col-sm-12">
        <?php woocommerce_content(); ?>
      </div>  
     
      <?php endif; ?>
    </div>  
  </div>
</div><!-- #primary -->
<?php get_footer(); ?>
