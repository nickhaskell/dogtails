<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Shikoku_Inu
 */
$categories = get_the_terms($id, 'category');
$tags = get_the_terms($id, 'post_tag');
$cta_headline = get_post_meta($id, 'meta_cta_headline', true);
$cta_button_url = get_post_meta($id, 'meta_cta_button_url', true);
$cta_button_text = get_post_meta($id, 'meta_cta_button_text', true);
$video_url = get_post_meta($id, 'meta_video_url', true);

function return_tags_list($tags)
{
    $tags_list = "";
    foreach ($tags as $key => $tag) {
        $tags_list .= $tag->name;
        if ($key != (count($tags) - 1)) {
            $tags_list .= ", ";
        }
    }
    return $tags_list;
}

?>
<!--noptimize-->
<script type="application/ld+json">
{ "@context": "http://schema.org",
 "@type": "BlogPosting",
 "mainEntityOfPage": {
	  	"@type": "WebPage",
  		"@id": "<?php the_permalink();?>"
 },
 "headline": "<?php the_title();?>",
 <?php if (get_post_thumbnail_id()): ?>
 "image": "<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog_column@2x')[0]; ?>",
 <?php endif;?>
 "author": "DogWatch",
 "publisher": {
		"@type": "Organization",
		"name": "DogWatch Hidden Fences, Inc.",
		"logo": [
				{
						"@type": "ImageObject",
						"url": "https://www.dogwatch.com/wp-content/uploads/2017/03/dogwatch-logo-google-151.png"
				}
		]
	},
 <?php if ($tags && count($tags) > 0): ?>
 "keywords": "<?php echo return_tags_list($tags); ?>",
 <?php endif;?>
 "url": "<?php the_permalink();?>",
 "datePublished": "<?php echo get_the_date(); ?>",
 "dateCreated": "<?php echo get_the_date(); ?>",
 "dateModified": "<?php the_modified_date();?>",
 "description": "<?php echo htmlspecialchars(get_the_excerpt()); ?> ",
 "articleBody": "<?php echo htmlspecialchars(get_the_content()); ?>"
 }
</script>
<!--/noptimize-->
<article id="post-<?php the_ID();?>" <?php post_class('sub-page entry-content container pad-b-120');?>>

  <?php include locate_template("inc/partials/blog-breadcrumb.php");?>
		<div class="row blog-content">
      <div class="col-sm-12">
        <div class="blog-post-single">
					<p class="post-date"><strong><?php echo the_date(); ?></strong></p>
          <?php if ($video_url): ?>
            <?php echo wp_oembed_get($video_url); ?>
          <?php else: ?>
            <?php if (get_post_thumbnail_id()): ?>
              <div class="post-image">
              <?php
$image_id = get_post_thumbnail_id();
$image_class = "picture";
$image_size = "blog_column";
include locate_template("inc/partials/picture.php");
?>
              </div>
            <?php endif;?>
          <?php endif;?>
					<h2 class="post-title">
						<?php the_title();?>
					</h2>
					<div class="post-content unmargin-last">
						<?php the_content();?>
					</div>
					<div class="post-meta unmargin-last">
						<?php if ($categories && count($categories) > 0): ?>
							<p class="post-meta-categories">
								<strong>Categories: </strong>
								<?php foreach ($categories as $key => $type): ?>
	                <a href="<?php echo get_term_link($type->term_id); ?>"><?php echo $type->name; ?></a><?php if ($key != (count($categories) - 1)) {echo ", ";}?>
	              <?php endforeach;?>
							</p>
						<?php endif;?>
						<?php if ($tags && count($tags) > 0): ?>
							<p class="post-meta-tags">
								<strong>Tags: </strong>
								<?php foreach ($tags as $key => $type): ?>
	                <a href="<?php echo get_term_link($type->term_id); ?>"><?php echo $type->name; ?></a><?php if ($key != (count($tags) - 1)) {echo ", ";}?>
	              <?php endforeach;?>
							</p>
						<?php endif;?>
						<?php include locate_template("inc/partials/share-nav.php");?>
					</div>
				</div>
        <?php include locate_template("inc/partials/blog-posts-sidebar.php");?>
			</div>
		</div>

</article>

<?php include locate_template("inc/partials/blog-cta.php");?>
