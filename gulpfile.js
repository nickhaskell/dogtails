var gulp = require("gulp"),
  plumber = require("gulp-plumber"),
  sass = require("gulp-ruby-sass"),
  autoprefix = require("gulp-autoprefixer"),
  browserSync = require("browser-sync"),
  reload = browserSync.reload,
  minifycss = require("gulp-minify-css"),
  newer = require("gulp-newer"),
  concat = require("gulp-concat"),
  uglify = require("gulp-uglify"),
  imagemin = require("gulp-imagemin");

// New
stripDebug = require("gulp-strip-debug");
rename = require("gulp-rename");
concat_util = require("gulp-concat-util");
cleancss = require("gulp-clean-css");
livereload = require("gulp-livereload");

var imgSrc = "assets/images/originals/*";
var imgDest = "assets/images";

var paths = {
  scripts: ["assets/scripts/*.js", "assets/scripts/**/*.js"],
  scripts_compile: [
    "assets/scripts/vendor/picturefill.js",
    "assets/scripts/vendor/fastclick.js",
    "assets/scripts/vendor/bootstrap-dropdown.js",
    "assets/scripts/vendor/jpanel.js",
    "assets/scripts/vendor/sod.js",
    "assets/scripts/vendor/skrollr.js",
    "assets/scripts/theme.js"
  ],
  scripts_dest: "dist/scripts",
  styles: ["assets/styles/*.scss", "assets/styles/**/*.scss"],
  styles_dest: "dist/styles",
  styles_critical: "dist/styles/critical.css",
  images: ["assets/images/*.png", "assets/images/*.gif", "assets/images/*.jpg"],
  images_dest: "dist/images",
  images_dest_small: "dist/images/small",
  includes: "inc"
};

gulp.task("browser-sync", function() {
  browserSync({});
});

gulp.task("php", function() {
  return gulp.src(["./*.php", "./inc/*.php"]).pipe(reload({ stream: true }));
});

//gulp.task('js', function () {
//  return gulp.src(['./assets/js/**/*.js', './assets/js/*.js'])
//    .pipe(concat('scripts.min.js')) //the name of the resulting file
//    .pipe(uglify())
//    .pipe(gulp.dest('js')) //the destination folder
//    .pipe(reload({stream:true}));
//});

// NEW
gulp.task("scripts", function() {
  return gulp
    .src(paths.scripts)
    .pipe(concat("scripts.min.js")) //the name of the resulting file
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts_dest))
    .pipe(livereload()); //the destination folder
});

/*
gulp.task('styles', function(){
  return sass('scss/', {sourcemap: true})
      .on('error', function (err) {
        console.error('Error', err.message);
      })
      .pipe(plumber())
      .pipe(gulp.dest(''))
      .pipe(autoprefix('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
	    .pipe(gulp.dest(''))
	    .pipe(minifycss())
	    .pipe(gulp.dest(''))
      .pipe(reload({stream:true}));
});
*/

// NEW
gulp.task("styles", function() {
  return sass(paths.styles)
    .on("error", function(err) {
      console.error("Error!", err.message);
    })
    .pipe(gulp.dest(paths.styles_dest))
    .pipe(autoprefix())
    .pipe(minifycss())
    .pipe(gulp.dest(paths.styles_dest))
    .pipe(livereload());
});

// FIGURE THIS OUT
gulp.task("styles:critical", function() {
  return gulp
    .src(paths.styles_critical)
    .pipe(concat_util.header("<style>"))
    .pipe(concat_util.footer("</style>"))
    .pipe(
      rename({
        basename: "critical",
        extname: ".html"
      })
    )
    .pipe(gulp.dest(paths.styles_dest))
    .pipe(livereload());
});

/*
gulp.task('images', function() {
  return gulp.src(imgSrc, {base: 'assets/images/originals'})
        .pipe(newer(imgDest))
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest(imgDest))
        .pipe(reload({stream:true}));
});
*/

gulp.task("default", function() {
  livereload({ start: true });
  livereload.listen();
  gulp.watch(paths.styles, ["styles"]);
  gulp.watch(paths.styles_critical, ["styles:critical"]);
  gulp.watch(paths.scripts, ["scripts"]);
});
