<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Shikoku_Inu
 */
$prev_link = get_previous_posts_link(__('Previous Page'));
$next_link = get_next_posts_link(__('Next Page'));
$mySearch = new WP_Query("s=$s & showposts=-1");
$num = $mySearch->post_count;
get_header();
?>


<div class="sub-page search-template">
	<article>
		<div class="entry-header container align-center no-subhead">
			<h1 class="entry-title headline">
				<?php printf( __( 'Search Results for: %s', 'shikoku-inu' ), '<span>' . get_search_query() . '</span>' ); ?>
			</h1>
			<?php if (have_posts()) : ?>
			<h4 class="headline-sub subhead"><?php echo $num; ?> Entries Found</h4>
			<?php endif; ?>
		</div>
		<?php if ( have_posts() ) : ?>

			<div class="searches pad-b-100">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'search'); ?>
				<?php endwhile; ?>
				<div class="search-pagination container">
					<div class="row">
						<div class="col-sm-12">
							<?php get_template_part( 'inc/pagination-full' ); ?>
						</div>
					</div>
				</div>
			</div>

			

		<?php else : // if there are no posts ?>

			<div class="pad-b-100">
				<div class="post no-results not-found">
					<div class="entry-content">
					<h4 class="headline-sub subhead align-center">
					No matches were found for your search terms. <br/>Please try again with different keywords.
					</h4>
				</div>
			</div>

		<?php endif; // end posts loop?>
		<?php include 'inc/partials/search-panel.php'; ?>
	</article><!-- #primary -->
</div>

<?php get_footer(); ?>
