<?php
/**
 * Template Name: Store Template
 *
 * @package Shikoku_Inu
 */

include 'inc/partials/role-redirect.php';

get_header(); ?>

<!--noptimize-->
<style media="screen">
	<?php include "dist/styles/templates/store.css"; ?>
</style>
<!--/noptimize-->

<div class="template-default template-store">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

		<?php endwhile; // end of the loop. ?>

</div>
<?php get_footer(); ?>
