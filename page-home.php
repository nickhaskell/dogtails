<?php
/**
 * Template Name: Home Template
 *
 * @package Shikoku_Inu
 */

get_header(); ?>

<?php get_template_part( 'inc/partials/hero' ); ?>

<div class="container template-home">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

		<?php endwhile; // end of the loop. ?>

</div>
<?php get_footer(); ?>
