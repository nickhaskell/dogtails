<?php
/**
 * Template Name: Redirect to URL
 *
 * @package Shikoku_Inu
 */
//include "inc/meta-vars.php"; // Need this to be an include so vars can be used in partials
global $post;
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		if ( get_post_meta($post->ID,'url', true) ) :
			$redirecturl = get_post_meta($id,'url', true);
			wp_redirect( $redirecturl );
			exit;
		else :
			_e('Your theme is not set up to use this template, or you have not specified a URL.', 'shikoku-inu');
		endif;
	endwhile;
endif;
?>
