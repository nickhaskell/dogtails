<?php
/**
 * Single blog post template
 *
 * @package Shikoku_Inu
 */

get_header(); ?>

<div class="template-default template-blog-new">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'blog' ); ?>

	<?php endwhile; // end of the loop. ?>

</div>
<?php get_footer(); ?>
