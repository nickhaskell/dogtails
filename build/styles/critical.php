html {
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box; }

* {
  position: relative;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box; }

*:before,
*:after {
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box; }

html, body {
  height: 100%; }

div {
  position: relative; }

.clear {
  clear: both; }

picture {
  line-height: 0;
  display: block; }
  picture img {
    line-height: 0;
    display: block; }

/* 2 RESETS
================================================== */
/* 2A GLOBAL RESET
Based on 'reset.css' in the Yahoo! User Interface Library: http://developer.yahoo.com/yui */
*, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td, address, caption, cite, code, em, strong, th {
  margin: 0;
  padding: 0;
  border: 0;
  font-family: inherit; }

article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
nav,
section,
summary,
svg {
  display: block; }

table {
  border-collapse: collapse;
  border-spacing: 0; }

fieldset {
  border: 0; }

img {
  border: 0;
  -ms-interpolation-mode: bicubic;
  /* Read more > http://bit.ly/qh1V0T */ }

caption, th {
  text-align: center; }

q:before, q:after {
  content: ''; }

html {
  overflow-y: scroll;
  font-size: 100%;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%; }

/* The below restores some sensible defaults */
strong {
  font-weight: bold; }

em {
  font-style: italic; }

a img {
  border: none; }

/* 2B MEDIA
Based on the Bootstrap toolkit from Twitter http://twitter.github.com/bootstrap */
video {
  display: inline-block;
  *display: inline;
  *zoom: 1; }

audio:not([controls]) {
  display: none; }

/* 2C FORMS
Based on the Bootstrap toolkit from Twitter http://twitter.github.com/bootstrap */
button, input, select, textarea {
  font-size: 100%;
  margin: 0;
  vertical-align: baseline;
  *vertical-align: middle; }

button, input {
  line-height: normal;
  *overflow: visible; }

button::-moz-focus-inner, input::-moz-focus-inner {
  border: 0;
  padding: 0; }

button {
  cursor: pointer;
  -webkit-appearance: button; }

input[type="button"], input[type="reset"], input[type="submit"] {
  cursor: pointer;
  -webkit-appearance: button; }

input[type="search"] {
  -webkit-appearance: textfield; }
  input[type="search"]::-webkit-search-decoration {
    -webkit-appearance: none; }

textarea {
  overflow: auto;
  vertical-align: top; }

/* BASE VARIABLES
 *******************/
/* Strings */
/* Colors */
/* Base size */
/* Fonts */
/* Weights */
/* Borders */
/* Breakpoints */
/* Scaffolding */
/* Z-Index */
/* Misc */
.align-center {
  width: 100%;
  text-align: center; }

.hardware {
  -webkit-transform: translateZ(0px);
  -webkit-perspective: 1000;
  -webkit-backface-visibility: hidden;
  -webkit-transform-style: preserve-3d; }

.hidden {
  display: none !important; }

.group:before, header .header-state-nav:before,
.group:after,
header .header-state-nav:after {
  content: "";
  display: table; }


.group:after,
header .header-state-nav:after {
  clear: both; }

.group, header .header-state-nav {
  zoom: 1;
  /* For IE 6/7 (trigger hasLayout) */ }

ul.unstyled {
  list-style-type: none; }

.rounded, .btn, header li.find-a-dealer-trig a {
  border-radius: 22px;
  height: 40px;
  padding-left: 17px;
  padding-right: 17px; }

.btn, header li.find-a-dealer-trig a {
  display: inline-block;
  text-align: center;
  min-width: 190px;
  overflow: hidden;
  background-color: #85b52e;
  font-weight: 700;
  letter-spacing: 1px !important;
  text-transform: uppercase;
  color: #ffffff !important;
  text-decoration: none !important;
  line-height: 2.5;
  font-size: 14px;
  height: 40px;
  padding-top: 3px; }
  .btn:hover, header li.find-a-dealer-trig a:hover, .btn:active, header li.find-a-dealer-trig a:active {
    background-color: #55910c;
    text-decoration: none !important; }
  .btn.large, header li.find-a-dealer-trig a.large {
    min-width: 220px; }
  .btn.small, header li.find-a-dealer-trig a.small {
    height: 28px;
    min-width: 60px;
    font-size: 12px;
    padding-top: 2px;
    line-height: 2; }
  .btn.green-green-green:hover, header li.find-a-dealer-trig a.green-green-green:hover, .btn.green-green-green:active, header li.find-a-dealer-trig a.green-green-green:active {
    background-color: #235608;
    text-decoration: none !important; }
  .btn.halloween, header li.find-a-dealer-trig a.halloween {
    color: #4d4b4e !important;
    background-color: #ffffff; }
    .btn.halloween:hover, header li.find-a-dealer-trig a.halloween:hover, .btn.halloween:active, header li.find-a-dealer-trig a.halloween:active {
      color: #ffffff !important;
      background-color: #85b52e; }
  .btn.another-green, header li.find-a-dealer-trig a.another-green {
    color: #ffffff !important;
    background-color: #85b52e; }
    .btn.another-green:hover, header li.find-a-dealer-trig a.another-green:hover, .btn.another-green:active, header li.find-a-dealer-trig a.another-green:active {
      color: #4d4b4e !important;
      background-color: #ffffff; }
  .btn.deep-green, header li.find-a-dealer-trig a.deep-green {
    background-color: #55910c; }
    .btn.deep-green:hover, header li.find-a-dealer-trig a.deep-green:hover, .btn.deep-green:active, header li.find-a-dealer-trig a.deep-green:active {
      background-color: #235608; }
  .btn.grey, header li.find-a-dealer-trig a.grey {
    background-color: #4d4b4e !important;
    color: #ffffff; }
    .btn.grey:hover, header li.find-a-dealer-trig a.grey:hover, .btn.grey:active, header li.find-a-dealer-trig a.grey:active {
      color: #ffffff !important;
      background-color: #85b52e !important; }
  .btn.grey-grey, header li.find-a-dealer-trig a.grey-grey {
    background-color: #4d4b4e !important;
    color: #ffffff; }
    .btn.grey-grey:hover, header li.find-a-dealer-trig a.grey-grey:hover, .btn.grey-grey:active, header li.find-a-dealer-trig a.grey-grey:active {
      color: #ffffff !important;
      background-color: #4d4b4e !important; }
  .btn.orange, header li.find-a-dealer-trig a.orange {
    background-color: #ff7800; }
    .btn.orange:hover, header li.find-a-dealer-trig a.orange:hover, .btn.orange:active, header li.find-a-dealer-trig a.orange:active {
      background-color: #ff5b00; }
  .btn.grey, header li.find-a-dealer-trig a.grey {
    background-color: #b8b3b0; }
    .btn.grey:hover, header li.find-a-dealer-trig a.grey:hover, .btn.grey:active, header li.find-a-dealer-trig a.grey:active {
      background-color: #a09c9a; }
  .btn.white, header li.find-a-dealer-trig a.white {
    background-color: #ffffff;
    color: #55910c !important; }
    .btn.white:hover, header li.find-a-dealer-trig a.white:hover, .btn.white:active, header li.find-a-dealer-trig a.white:active {
      background-color: #55910c;
      color: #ffffff !important; }
  .btn.dark-green, header li.find-a-dealer-trig a.dark-green {
    background-color: #235608;
    color: #ffffff; }
    .btn.dark-green:hover, header li.find-a-dealer-trig a.dark-green:hover, .btn.dark-green:active, header li.find-a-dealer-trig a.dark-green:active {
      background-color: rgba(85, 145, 12, 0.95); }
  .btn.no-min, header li.find-a-dealer-trig a {
    min-width: inherit !important; }

.display-inline-block {
  display: inline-block !important; }

.aligncenter {
  display: block;
  margin: 0 auto; }

.textcenter {
  text-align: center; }

.alignleft {
  float: left; }

.alignright {
  float: right; }

img.alignleft {
  margin-right: 1em;
  margin-bottom: 1.8em; }

img.alignright {
  margin-left: 1em;
  margin-bottom: 1.8em; }

.text-right {
  text-align: right; }

.hide {
  /* Hide stuff without resorting to display:none; */
  visibility: hidden;
  width: 0 !important;
  height: 0 !important;
  line-height: 0 !important;
  padding: 0 !important;
  margin: 0 !important; }

.unmargin-last *:last-child {
  margin-bottom: 0 !important; }

html {
  font-size: 62.5%;
  font-smoothing: antialiased;
  -moz-font-smoothing: antialiased;
  -webkit-font-smoothing: antialiased; }

body {
  font-size: 16px;
  font-size: 1.6rem;
  font-family: "proxima-nova", sans-serif;
  font-weight: 400;
  color: #4d4b4e; }

a {
  text-decoration: none; }

header {
  position: fixed;
  width: 100%;
  display: block;
  z-index: 5000;
  /* Site Search and Zip Search */ }
  header .main-header {
    min-height: 100px;
    background-color: rgba(255, 255, 255, 0.95);
    transition: all 0.2s ease; }
    @media screen and (max-width: 767px) {
      header .main-header {
        min-height: 72px; } }
  header .logos {
    display: inline-block;
    z-index: 1000; }
  header .main-header {
    padding-top: 10px; }
    header .main-header nav {
      float: right;
      top: 7px;
      transition: background-color 0.15s linear; }
      header .main-header nav li {
        margin-right: 25px; }
        header .main-header nav li:last-child {
          margin-right: 0; }
        header .main-header nav li.open a .caret {
          transform: rotate(180deg); }
      header .main-header nav a {
        padding-bottom: 3px; }
        header .main-header nav a .caret {
          margin-top: -3px;
          border-width: 5px;
          margin-left: 3px; }
  header .menu-trigger {
    float: right;
    right: 0px;
    top: 5px;
    cursor: pointer; }
    header .menu-trigger:before {
      margin: 0; }
    @media screen and (min-width: 1101px) {
      header .menu-trigger {
        display: none; } }
  @media screen and (max-width: 1100px) {
    header .header-state-nav #header-state-main-nav {
      display: none; } }
  header .header-state-nav .main-nav-options {
    margin-left: 25px;
    float: right;
    top: -6px; }
    @media screen and (max-width: 767px) {
      header .header-state-nav .main-nav-options {
        top: -6px; } }
  header li.site-search {
    margin-right: 18px !important; }
    header li.site-search a:before {
      font-size: 17px !important;
      font-size: 1.7rem !important; }
    @media screen and (max-width: 767px) {
      header li.site-search {
        display: none; } }
  header li.find-a-dealer-trig {
    margin-right: 0 !important; }
    header li.find-a-dealer-trig a {
      display: block !important;
      overflow: visible !important;
      padding-top: 3px;
      background-color: #55910c !important;
      margin-right: -4px; }
      header li.find-a-dealer-trig a:hover, header li.find-a-dealer-trig a:active {
        background-color: #85b52e !important; }
      @media screen and (max-width: 1051) {
        header li.find-a-dealer-trig a {
          margin-right: 0 !important; } }
    @media screen and (max-width: 1100px) {
      header li.find-a-dealer-trig {
        margin-right: 25px !important; }
        header li.find-a-dealer-trig a {
          margin-right: 0; } }
    @media screen and (max-width: 767px) {
      header li.find-a-dealer-trig {
        display: none; } }

header .logos a {
  top: 18px;
  text-indent: -9999px;
  display: inline-block;
  z-index: 1000; }
  header .logos a svg {
    transition: fill 0.2s; }
  header .logos a.logo-large {
    display: block; }
    @media screen and (max-width: 767px) {
      header .logos a.logo-large {
        display: none; } }
  header .logos a.logo-small {
    display: none; }
    @media screen and (max-width: 767px) {
      header .logos a.logo-small {
        display: block; } }

@media screen and (min-width: 768px) {
  header.scroll-header .logos a {
    top: 20px; } }

header.scroll-header .logos a.logo-large {
  display: none; }
  @media screen and (max-width: 767px) {
    header.scroll-header .logos a.logo-large {
      display: none; } }

header.scroll-header .logos a.logo-small {
  display: block; }
  @media screen and (max-width: 767px) {
    header.scroll-header .logos a.logo-small {
      display: block; } }
  header.scroll-header .logos a.logo-small svg path {
    fill: #fff; }

header .main-header.state-green .logos a svg path {
  fill: #ffffff; }

@media screen and (max-width: 479px) {
  .logo-small svg {
    width: 281px;
    height: 21px; } }

@media screen and (max-width: 380px) {
  .logo-small svg {
    width: 230px;
    height: 17px; } }

/* TYPOGRAPHY ***************************************************/
header nav a {
  color: #55910c;
  font-size: 14px;
  font-size: 1.4rem;
  font-weight: 700;
  text-transform: uppercase; }

header nav .dropdown-menu li a {
  color: #ffffff; }

header .menu-trigger {
  color: #55910c;
  font-size: 21px;
  font-size: 2.1rem; }

header .find-a-dealer a {
  color: #ffffff;
  font-size: 14px;
  font-size: 1.4rem;
  letter-spacing: 0 !important; }

header.scroll-header nav a {
  color: #ffffff; }

header.scroll-header .menu-trigger {
  color: #ffffff; }

/**
 *
 * New Blog Nav Overrides
 *
 */
header .main-header {
  display: flex;
  align-content: stretch;
  padding-top: 0 !important; }
  header .main-header .container {
    display: flex;
    align-content: flex-end;
    width: 100%; }
    header .main-header .container .logos {
      margin-right: auto;
      display: flex;
      align-items: center; }
      header .main-header .container .logos a {
        top: inherit !important; }
    header .main-header .container .header-state {
      padding-top: 0 !important; }
      header .main-header .container .header-state.header-state-nav {
        align-items: center; }
        header .main-header .container .header-state.header-state-nav.active {
          display: flex; }
        header .main-header .container .header-state.header-state-nav nav {
          top: inherit !important; }
          header .main-header .container .header-state.header-state-nav nav.main-nav-options {
            width: auto !important;
            top: inherit !important; }
            header .main-header .container .header-state.header-state-nav nav.main-nav-options ul {
              display: flex;
              align-items: center; }
              @media screen and (max-width: 1100px) {
                header .main-header .container .header-state.header-state-nav nav.main-nav-options ul > * + * {
                  margin-left: 25px; } }
            header .main-header .container .header-state.header-state-nav nav.main-nav-options li.site-search {
              position: relative !important;
              right: inherit !important;
              top: inherit !important;
              margin-right: auto !important; }
              header .main-header .container .header-state.header-state-nav nav.main-nav-options li.site-search a:before {
                margin: 0 !important; }
            header .main-header .container .header-state.header-state-nav nav.main-nav-options li.menu-trig {
              position: relative;
              top: inherit !important; }
              @media screen and (min-width: 1151px) {
                header .main-header .container .header-state.header-state-nav nav.main-nav-options li.menu-trig {
                  display: none; } }
              header .main-header .container .header-state.header-state-nav nav.main-nav-options li.menu-trig .menu-trigger {
                top: inherit !important; }
      header .main-header .container .header-state.header-state-site-search {
        align-items: center; }
        header .main-header .container .header-state.header-state-site-search.active {
          display: flex; }
        header .main-header .container .header-state.header-state-site-search .state-content {
          display: flex;
          margin-top: inherit !important;
          padding-top: inherit !important; }
          header .main-header .container .header-state.header-state-site-search .state-content .nav-align {
            display: flex;
            align-items: center;
            margin-top: inherit !important;
            margin-left: 25px !important; }
            header .main-header .container .header-state.header-state-site-search .state-content .nav-align a {
              margin-left: 0 !important; }
          @media screen and (max-width: 767px) {
            header .main-header .container .header-state.header-state-site-search .state-content form {
              display: none; } }

/**
 *
 * Mobile Nav Overrides
 *
 */
#jPanelMenu-menu a.logo-large {
  display: none; }
  @media screen and (max-width: 767px) {
    #jPanelMenu-menu a.logo-large {
      display: none; } }

#jPanelMenu-menu a.logo-small {
  display: none; }
  @media screen and (max-width: 767px) {
    #jPanelMenu-menu a.logo-small {
      display: block;
      position: absolute;
      top: 23px;
      left: 20px; }
      #jPanelMenu-menu a.logo-small svg path {
        fill: #fff; } }
  @media screen and (max-width: 479px) {
    #jPanelMenu-menu a.logo-small {
      top: 25px; }
      #jPanelMenu-menu a.logo-small svg {
        width: 281px;
        height: 21px; } }
  @media screen and (max-width: 380px) {
    #jPanelMenu-menu a.logo-small {
      top: 27px; }
      #jPanelMenu-menu a.logo-small svg {
        width: 230px;
        height: 17px; } }

@media screen and (max-width: 479px) {
  .scroll-body #jPanelMenu-menu a.logo-small {
    top: 16px !important; } }

@media screen and (max-width: 380px) {
  .scroll-body #jPanelMenu-menu a.logo-small {
    top: 18px !important; } }


/*# sourceMappingURL=critical.css.map*/