<?php
/**
 * Theme functions
 *
 * Sets up the theme and provides some helper functions.
 *
 * @package Shikoku_Inu
 */


/* OEMBED SIZING
 ========================== */

if ( ! isset( $content_width ) )
	$content_width = 600;


/* THEME SETUP
 ========================== */

if ( ! function_exists( 'shikoku_inu_setup' ) ):
function shikoku_inu_setup() {

	// Let wordpress manage the title for us
	add_theme_support( 'title-tag' );

	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// Add custom nav menu support
	register_nav_menu( 'primary', __( 'Primary Menu', 'shikoku-inu' ) );
	register_nav_menu( 'domestic', __( 'Domestic Dealer Menu', 'emi' ) );
	register_nav_menu( 'international', __( 'International Dealer Menu', 'emi' ) );
	register_nav_menu( 'dealer', __( 'Dealer Area Menu', 'emi' ) );
	register_nav_menu( 'shop', __( 'Customer Shop', 'emi' ) );

	// Add featured image support
	add_theme_support( 'post-thumbnails' );

	// Add custom image sizes
	add_image_size('hero_product', 1440, 504, TRUE);

	# add_image_size('hero_home', 1440, 510, TRUE); replaced

	add_image_size('hero_home_new', 1440, 575, TRUE);

	# add_image_size('hero_menu', 1440, 417, TRUE); not used

	add_image_size('feature_panel', 1440, 417, TRUE);
	add_image_size('feature_panel_s@1x', 622, 180, TRUE);
	add_image_size('feature_panel_s@2x', 1244, 360, TRUE);

	add_image_size('pwt_small', 1440, 528, TRUE);
	add_image_size('pwt_large', 1440, 648, TRUE);

	add_image_size('hero_product_header_new@1x', 1440, 642, TRUE);
	add_image_size('hero_product_header_new@2x', 2880, 1284, TRUE);

	add_image_size('video_placeholder@1x', 688, 387, TRUE);
	add_image_size('video_placeholder@2x', 1376, 774, TRUE);

	add_image_size('hero_product_new@1x', 1440, 772, TRUE);
	add_image_size('hero_product_new@2x', 2880, 1544, TRUE);
	add_image_size('customer_story@1x', 720, 637, TRUE);
	add_image_size('customer_story@2x', 1440, 1274, TRUE);
	add_image_size('customer_story_m@1x', 416, 368, TRUE);
	add_image_size('customer_story_m@2x', 832, 736, TRUE);

	add_image_size('home_feature', 270, 260, TRUE);
	add_image_size('home_feature_new', 324, 312, TRUE);
	add_image_size('home_feature_new_s@1x', 105, 100, TRUE);
	add_image_size('home_feature_new_s@2x', 210, 200, TRUE);

	add_image_size('product_thumb', 330, 250, TRUE);
	add_image_size('product_thumb_large', 500, 378, TRUE);

	add_image_size('product_model_thumb', 270, 193, TRUE);
	add_image_size('product_model_thumb_2_col', 450, 310, TRUE);

	//add_image_size('image_nav_image', 610, 340, TRUE);
	add_image_size('ribbon@1x', 150, 138, TRUE);
	add_image_size('image_nav_image', 738, 462, TRUE);

	// Used on blog-width columns
	add_image_size('blog_column@1x', 745, 9999, FALSE);
	add_image_size('blog_column@2x', 980, 9999, FALSE);
}
endif;
add_action( 'after_setup_theme', 'shikoku_inu_setup' );

// Filter Yoast Meta Priority
function yoasttobottom() {
	return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


/* SIDEBARS & WIDGET AREAS
 ========================== */

function shikoku_inu_widgets_init() {
	/*
	register_sidebar( array(
		'name' => __( 'Sidebar', 'shikoku-inu' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	*/
	register_sidebar( array(
		'name' => __( 'Footer Nav Position 1', 'shikoku-inu' ),
		'id' => 'footer-position-1',
		'before_title' => '<strong>',
		'after_title' => '</strong>',
		'before_widget' => '<div class="col-sm-12 footer-col">',
		'after_widget' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Copyright', 'shikoku-inu' ),
		'id' => 'footer-copyright',
		'before_title' => '<span style="display:none;">',
		'after_title' => '</span>',
		'before_widget' => '<p>',
		'after_widget' => '</p>',
	) );
	register_sidebar( array(
		'name' => __( 'Blog Right Column', 'shikoku-inu' ),
		'id' => 'dw-blog-widgets-right',
		'before_title' => '<p class="widget-title"><strong>',
		'after_title' => '</strong></p>',
		'before_widget' => '<div class="dealer-widget">',
		'after_widget' => '</div>',
	) );
}
add_action( 'widgets_init', 'shikoku_inu_widgets_init' );

add_filter('widget_text', 'do_shortcode');

/* ENQUEUE SCRIPTS & STYLES
 ========================== */
function shikoku_inu_scripts() {
	// theme style.css file
	wp_enqueue_style( 'shikoku-inu-style', get_template_directory_uri() . '/dist/styles/style.css' );

	/*
wp_enqueue_script(
  'modernizr',
  get_template_directory_uri() . '/dist/scripts/modernizr.js',
  array('jquery')
);
*/

wp_enqueue_script(
  'theme-init',
	get_template_directory_uri() . '/dist/scripts/scripts.min.js',
  array('jquery'),
	'',
	true
);

wp_dequeue_style( 'se-link-styles' ); // Remove search everything css
remove_action('wp_head', 'se_global_head');
remove_action('wp_head', 'feed_links_extra', 3); // Remove category feeds
remove_action('wp_head', 'feed_links', 2); // Remove Post and Comment Feeds
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Remove emoji support
remove_action( 'wp_print_styles', 'print_emoji_styles' );
global $wp_widget_factory;
remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}
add_action('wp_enqueue_scripts', 'shikoku_inu_scripts');


// Remove version number from css file
function remove_cssjs_ver( $src ) {
   if( strpos( $src, '?ver=' ) )
       $src = remove_query_arg( 'ver', $src );
   return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );


// Hide WYSIWYG editor on pages
function remove_post_type_support_for_pages()
{
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// check for a template type
	if ($template_file != 'page-forum.php' || $template_file != 'page-store.php') {
		// UNCOMMENT if you want to remove some stuff
    remove_post_type_support( 'page', 'editor' );
    remove_post_type_support( 'page', 'thumbnail' );
    remove_post_type_support( 'page', 'excerpt' );
    remove_post_type_support( 'page', 'comments' );
	}

}
add_action( 'admin_init', 'remove_post_type_support_for_pages' );


add_filter( 'wp_default_scripts', 'removeJqueryMigrate' );
function removeJqueryMigrate(&$scripts){
 if(!is_admin()){
  $scripts->remove('jquery');
  $scripts->add('jquery', false, array('jquery-core'), '1.10.2');
 }
}

// Include CMB2 metaboxes
include('inc/functions/cmb2.php');

// Custom post type - Dealers
// include('inc/functions/custom_post_dealer.php');

// Optional Customizations
// Includes: TinyMCE tweaks, admin menu & bar settings, query overrides
include('inc/functions/customizations.php');

// Nav walker for bootstrap dropdowns
require_once('inc/functions/wp_bootstrap_navwalker.php');

// Include file to add social media links ot general settings page
// include('inc/functions/sociallinks.php');


// move radio input inside label
add_filter("gform_field_choices", "radio_input_inside_label", 10, 2);
function radio_input_inside_label($choices, $field){
    if($field["type"] != "radio")
        return $choices;

    $choices = "";

    if(is_array($field["choices"])){
        $choice_id = 0;
        $count = 1;

        $logic_event = !empty($field["conditionalLogicFields"]) ? "onclick='gf_apply_rules(" . $field["formId"] . "," . GFCommon::json_encode($field["conditionalLogicFields"]) . ");'" : "";

        foreach($field["choices"] as $choice){
            $id = $field["formId"] . '_' . $field["id"] . '_' . $choice_id++;
            $field_value = !empty($choice["value"]) || rgar($field, "enableChoiceValue") ? $choice["value"] : $choice["text"];
            $checked = rgar($choice,"isSelected") ? "checked='checked'" : "";
            $tabindex = GFCommon::get_tabindex();

            $input = sprintf("<input name='input_%d' type='radio' value='%s' %s id='choice_%s' $tabindex $logic_event />", $field["id"], esc_attr($field_value), $checked, $id);
            $choices .= sprintf("<li class='gchoice_$id'><label for='choice_%s'>%s %s</label></li>", $id, $choice["text"], $input);

            $count++;
        }

    }
    return $choices;
}

// Enhanced pagination
function base_pagination() {
    global $wp_query;

    $big = 999999999; // This needs to be an unlikely integer

    // For more options and info view the docs for paginate_links()
    // http://codex.wordpress.org/Function_Reference/paginate_links
    $paginate_links = paginate_links( array(
        'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'mid_size' => 2,
				'prev_text' => '«',
				'next_text' => '»'
    ) );

    // Display the pagination if more than one page is found
    if ( $paginate_links ) {
        echo '<div class="container pagination regular-pagination">';
				echo '<div class="row">';
				echo '<div class="col-sm-12">';
        echo $paginate_links;
				echo '</div>';
				echo '</div>';
        echo '</div>';
    }
}

// move checkbox input inside label
add_filter("gform_field_choices", "checkbox_input_inside_label", 10, 2);
function checkbox_input_inside_label($choices, $field){
    if($field["type"] != "checkbox")
        return $choices;

    $choices = "";

    if(is_array($field["choices"])){
        //$choice_id = 0;
				$choice_id = 1;
        $count = 1;

        $logic_event = !empty($field["conditionalLogicFields"]) ? "onclick='gf_apply_rules(" . $field["formId"] . "," . GFCommon::json_encode($field["conditionalLogicFields"]) . ");'" : "";

        foreach($field["choices"] as $choice){
            //$id = $field["id"] . '_' . $choice_id++;

						$fieldname = $field["id"] . '.' . $choice_id;

						$id = $field["formId"] . '_' . $field["id"] . '_' . $choice_id++;
						$field_value = !empty($choice["value"]) || rgar($field, "enableChoiceValue") ? $choice["value"] : $choice["text"];
            $checked = rgar($choice,"isSelected") ? "checked='checked'" : "";
            $tabindex = GFCommon::get_tabindex();


            $input = sprintf("<input name='input_%s' type='checkbox' value='%s' %s id='choice_%s' $tabindex $logic_event />", $fieldname, esc_attr($field_value), $checked, $id);
            $choices .= sprintf("<li class='gchoice_$id'><label for='choice_%s'>%s %s</label></li>", $id, $choice["text"], $input);

            $count++;
        }

    }
    return $choices;
}

// Remove private from private page titles
function bfa_remove_word_private($string) {
$string = str_ireplace("private: ", "", $string);
return $string;
}
add_filter('the_title', 'bfa_remove_word_private');

function my_login_redirect( $redirect_to, $request, $user ) {

	//is there a user to check?
	global $user;
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return home_url('/wp-admin/');
		} elseif ( in_array( 'dealer', $user->roles ) ) {
			return get_option('general_setting_dealer_redirect');
		} else {
			return home_url();
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

function wpse_131562_redirect() {
		// If user isn't logged in
    if (!is_user_logged_in()) {
			// If the user is on woocommerce
			if (function_exists ( "is_woocommerce" ) && is_woocommerce()) {
				// If the user is on the cart page, the checkout, or whatever page 706 is
				if (is_cart() || is_checkout() || is_page( array( 706 ) )) {
					// feel free to customize the following line to suit your needs
					// Redirect to the general settings login link
					wp_redirect(get_option('general_setting_login_link'));
					exit;
				}
			}
			// If you are on bbpress, redirect to general setting login link
			if (is_bbpress()) {
				// feel free to customize the following line to suit your needs
				wp_redirect(get_option('general_setting_login_link'));
				exit;
			}
    }
}
//add_action('template_redirect', 'wpse_131562_redirect');

// Fix for breadcrumbs on bbpress
function mycustom_breadcrumb_options() {
	// Home - default = true
	$args['include_home']    = false;
	// Forum root - default = true
	$args['include_root']    = false;
	// Current - default = true
	$args['include_current'] = false;

	return $args;
}

add_filter('bbp_before_get_breadcrumb_parse_args', 'mycustom_breadcrumb_options' );

// Get current user role
function get_current_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles = $current_user->roles;
	$role = array_shift($roles);
	return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role] ) : false;
}



/**
 * Dequeue bbPress CSS and .js on non-forum pages.
 */
function isa_dequeue_bbp_style() {
	if ( class_exists('bbPress') ) {
		if ( ! is_bbpress() ) {
			wp_dequeue_style('bbp-default');
			wp_dequeue_style( 'bbp_private_replies_style');
			wp_dequeue_script('bbpress-editor');
		}
	}
}
add_action( 'wp_enqueue_scripts', 'isa_dequeue_bbp_style', 99 );

// Exclude custom post types from search results
add_action( 'init', 'update_my_custom_type', 99 );

function update_my_custom_type() {
	global $wp_post_types;

	/*  - THIS SCREWS UP WOOCOMMERCE CATEGORIES
	if ( post_type_exists( 'product' ) ) {
		$wp_post_types['product']->exclude_from_search = true;
	}
	*/
	if ( post_type_exists( 'forum' ) ) {
		$wp_post_types['forum']->exclude_from_search = true;
	}
	if ( post_type_exists( 'topic' ) ) {
		$wp_post_types['topic']->exclude_from_search = true;
	}
	if ( post_type_exists( 'reply' ) ) {
		$wp_post_types['reply']->exclude_from_search = true;
	}
	if ( post_type_exists( 'post' ) ) {
		$wp_post_types['post']->exclude_from_search = true;
	}
}

// Remove quickdraft from dashboard options
function remove_dashboard_meta() {
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}
add_action( 'admin_init', 'remove_dashboard_meta' );

// Attempt to fix upload issues
add_filter( 'wp_image_editors', 'change_graphic_lib' );
function change_graphic_lib($array) {
return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

// Automatically add rel=0 to oembeds
add_filter('oembed_dataparse','strip_related_videos',10,3);
function strip_related_videos($return, $data, $url) {
    if ($data->provider_name == 'YouTube') {
        $data->html = str_replace('feature=oembed', 'feature=oembed&#038;rel=0', $data->html);
        return '<div class="video-player"><div class="video-player-container">' . $data->html. '</div></div>';;
    } else return $return;
}

// Shorthand to retrieve cmb2 meta
function getMeta($field) {
	$id   = get_the_ID();
	$pre  = "_cmb2_";
	$meta = get_post_meta($id, $pre . $field, true);
	return $meta;
}



// Add WooCommerce functions
// require_once('inc/functions/functions-woocommerce.php');	

// Add Blog custom post type
require_once('inc/custom-post-types/blog/settings.php');
require_once('inc/custom-post-types/blog/fields.php');

// Add Custom woocommerce settings
// require_once('inc/custom-post-types/woocommerce/settings.php');
