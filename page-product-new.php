<?php
/**
 * Template Name: Product Template New
 *
 * @package Shikoku_Inu
 */

include 'inc/partials/role-redirect.php';

get_header(); ?>

<div class="template-default template-product template-product-new">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

		<?php endwhile; // end of the loop. ?>

</div>
<?php get_footer(); ?>
