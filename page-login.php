<?php /* Template Name: Login Template */  ?>

<?php get_header(); ?>
	<!-- section -->
    <div class="container template-default template-login">

        <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'content', 'page' ); ?>

        <?php endwhile; // end of the loop. ?>

	</div>
	<!-- /section -->

<?php get_footer(); ?>
