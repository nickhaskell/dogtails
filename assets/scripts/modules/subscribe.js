import Cookies from 'js-cookie';
import { modal } from './modal';

const subscribe = {
    data: {
        time_to_fire: 5000,
        args: {
            button: false,
            message_is_element: true,
            message: '.subscribe-form',
            additional_classes: 'notice'
        },
        cookie: {
            name: "subscribeexp",
            expiration: 30 * 24 * 60 * 60 * 1000, // 30 (days) * 24 (hours) * 60 (minutes) * 60 (seconds) * 1000 (milliseconds),
        }
    },
    init() {
        subscribe.methods.add_handlers();
        window.subscribe = subscribe;
    },
    methods: {
        add_handlers() {
            document.addEventListener("DOMContentLoaded", function (event) {
                subscribe.methods.open();
            });
        },
        open() {
            if (subscribe.methods.is_ready() === false) return;

            setTimeout(() => {
                modal.set(subscribe.data.args);
                subscribe.methods.set_cookie();
            }, subscribe.data.time_to_fire)
        },
        is_ready() {
            // Check if a exp cookie exists, if it doesn't fire the form
            if (Cookies.get(subscribe.data.cookie.name) == undefined) {
                console.log("cookie does not exist");
                return true;
                // If cookie exists, check if cookie is expired
            } else {
                console.log("cookie exists");
                // If cookie is expired
                if (subscribe.methods.is_cookie_expired()) {
                    // Fire check-in & set new cookie (true)
                    console.log("cookie is expired");
                    return true;
                    // If cookie isn't expired
                } else {
                    // Don't do anything (false)
                    console.log("cookie is fresh");
                    return false;
                }
            }
        },
        is_cookie_expired() {

            // Get cookie exp and convert to int
            let last_checkin_time = parseInt(Cookies.get(subscribe.data.cookie.name));
            let current_time = new Date().getTime();

            //console.log("current time", current_time);
            //console.log("time to check against", last_checkin_time + expiration_time);

            // If the current time is over 24 hours past the last checkin time
            if (current_time > (last_checkin_time + subscribe.data.cookie.expiration)) {
                // Cookie is expired
                console.log("cookie found to be expired, get a new cookie");
                return true;
            } else {
                // Cookie is fresh
                console.log("cookie found to be fresh, do nothing");
                return false;
            }
        },
        set_cookie() {
            console.log("setting cookie");
            // Clear existing cookie
            Cookies.remove(subscribe.data.cookie.name);

            // Setup a new cookie using the current time
            let current_time = new Date().getTime();
            Cookies.set(subscribe.data.cookie.name, current_time);
        },
        force_open() {
            Cookies.remove(subscribe.data.cookie.name);
            modal.set(subscribe.data.args);
        }
    }
}

subscribe.init();

export { subscribe };