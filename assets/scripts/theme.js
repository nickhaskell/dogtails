import { modal } from './modules/modal';
import { subscribe } from './modules/subscribe';
import jPanelMenu from '../library/jpanel';
import jRespond from '../library/jrespond';
import skrollr from '../library/skrollr';

/* GLOBAL VARIABLES *********************************************************/

var bp_phone = false;
var bp_under_desktop = false;
var jPM = "";
var scroll_pos = 0;
var SCROLL_SPEED = 350;
var SKROLLR = null;

(function($) {
  // Initial load functions
  $(document).ready(function() {
    // Needed for custom forms
    $("body").addClass("has-js");

    onLoadFunctions();
    onLoadChecks();
    onLoadButtonHandlers();
  });

  function onLoadFunctions() {
    loadResponsive();
  }

  /* ######################################################################### */
  /* HANDLERS                                                                  */
  /* ######################################################################### */

  function onLoadButtonHandlers() {
    toTopHandler();
    headerStateHandler();
  }

  // Add scroll to top functionality to to top handler
  function toTopHandler() {
    $(".totop").on("click", function(event) {
      scrollToTop();
    });
  }

  // Buttons to switch the header states
  function headerStateHandler() {
    $(".site-search").on("click", function(event) {
      event.preventDefault();
      $(document).off("click.closestatehandler");
      setHeaderState("site-search");
      $(".header-state-site-search .sitesearch input.field").focus();
    });

    $("header .find-a-dealer, #jPanelMenu-menu .find-a-dealer").on(
      "click",
      function(event) {
        event.preventDefault();
        $(document).off("click.closestatehandler");
        setHeaderState("zip-search");
        //$('.header-state-zip-search input.zipcode').focus();
      }
    );

    $(".main-content-find-a-dealer-button .find-a-dealer").on("click", function(
      event
    ) {
      event.preventDefault();
      clearHeaderStates("clear");
      jPM.close();
      $("header .header-state-zip-search").addClass("active");
      $("header .main-header").addClass("state-green");
      addCloseStateHandler();
    });

    $(".reset-header-state").on("click", function(event) {
      event.preventDefault();
      clearHeaderStates("reset");
    });
  }

  function registerBingConversion() {
    window.uetq = window.uetq || [];
    window.uetq.push({ ea: "locate dealer" });
  }

  // Clear the header state
  function clearHeaderStates(str) {
    switch (str) {
      case "clear":
        $("header .header-state").each(function() {
          $(this).removeClass("active");
        });
        break;

      case "reset":
        setHeaderState("nav");
        break;

      default:
        break;
    }
  }

  // Sets header state
  function setHeaderState(str) {
    clearHeaderStates("clear");
    jPM.close();
    if (str != "nav") {
      $("header .main-header").addClass("state-green");
      addCloseStateHandler();
    } else {
      $("header .main-header").removeClass("state-green");
    }
    $("header .header-state-" + str).addClass("active");
  }

  /* ######################################################################### */
  /* CHECKS                                                                    */
  /* ######################################################################### */

  function onLoadChecks() {
    checkForScroll();
    loadSkrollr();
    checkForShowHides();
    checkForStyledInputs();
    //checkForDealerStore();
    //checkForRssLinks();

    checkForCustomFormHandlers();
    checkForCustomForms();
    //checkForWooCommerce();
  }

  // Check for Show Hide sections
  function checkForShowHides() {
    if ($(".show-hide-container").length > 0) {
      $(".show-hide-container").on("click.showhide", function() {
        sh = $(this);
        sh_content = sh.find(".show-hide-content");

        sh_content.slideToggle("fast");
        sh.toggleClass("open");
      });

      // Stop parent from closing when you click parent content
      $(".show-hide-actual-content, .show-hide-actual-content p").on(
        "click",
        function(e) {
          e.stopPropagation();
        }
      );
    }
  }

  // Check to see if you've scrolled down already
  function checkForScroll() {
    var header = $("header");
    var body = $("body");
    var totop = $(".totop");
    var st = $(document).scrollTop();

    var totop_top = totop.offset().top;
    var footer_top = $("footer").offset().top;

    // Check header on page load, may be useful if we add ajax pageload
    if (st > 78) {
      header.addClass("scroll-header");
      body.addClass("scroll-body");
      totop.addClass("active");
    } else {
      header.removeClass("scroll-header");
      body.removeClass("scroll-body");
      totop.removeClass("active");
    }

    // Change the color of the totop button if the button is in the footer
    if (totop_top >= footer_top) {
      totop.addClass("dark-bg");
    } else {
      totop.removeClass("dark-bg");
    }

    // Recheck header if we scroll
    $(window).on("scroll", function() {
      var body_top = $(document).scrollTop();

      var totop_top = totop.offset().top;
      var footer_top = $("footer").offset().top;

      // Check to see if we are covering a dark background
      var isOverDark = checkBackground(totop_top, footer_top);

      if (body_top > 78) {
        header.addClass("scroll-header");
        body.addClass("scroll-body");
        totop.addClass("active");
      } else {
        header.removeClass("scroll-header");
        body.removeClass("scroll-body");
        totop.removeClass("active");
      }

      // Change the color of the totop button if we are over a .dark panel or the footer
      if (isOverDark) {
        totop.addClass("dark-bg");
      } else {
        totop.removeClass("dark-bg");
      }
    });
  }

  function loadSkrollr() {
    if (
      !/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(
        navigator.userAgent || navigator.vendor || window.opera
      )
    ) {
      console.log("skrollr loaded");
      SKROLLR = skrollr.init($(".skrollable-hero"), {
        //forceHeight: false,
        smoothScrolling: false,
        render: function(data) {}
      });
    }
  }

  // Return bool if totop is over a green .dark element, or the footer
  function checkBackground(pos, footer_pos) {
    var check = false;

    $(".dark").each(function() {
      var panel = $(this);
      var panel_top = panel.offset().top;
      var panel_height = panel.height() + panel_top;

      if (pos >= panel_top && pos <= panel_height) {
        check = true;
      } else {
        return;
      }
    });

    // If we are over the footer element make it true, because the footer's height is the body height
    if (pos >= footer_pos) {
      check = true;
    }

    return check;
  }

  // Check for styled inputs
  function checkForStyledInputs() {
    if ($(".styled-select").length > 0) {
      $(".styled-select").selectOrDie({
        size: 8
      });
    }
    if ($(".gfield_select").length > 0) {
      $(".gfield_select").selectOrDie({
        placeholderOption: true
      });
    }
  }

  // Check for forms with custom styling and apply the styling
  function checkForCustomForms() {
    if (
      $(".gfield_checkbox label").length > 0 ||
      $(".gfield_radio label").length > 0 ||
      $(".login-remember label").length > 0
    ) {
      // console.log('Found custom forms, setting them up.');
      setupLabel();
    }
  }

  function checkForCustomFormHandlers() {
    // console.log('1. Checking for custom form handlers');
    if (
      $(".gfield_checkbox label").length > 0 ||
      $(".gfield_radio label").length > 0 ||
      $(".login-remember label").length > 0
    ) {
      // console.log('2. custom form handlers found');

      // Allows click to work on input labels on desktop
      // This is needed because we apply a strange css rule
      // To the labels so they don't accept pointer events
      // That allows iOS to select inputs by touch on labels.
      $(
        ".gfield_radio label, .gfield_checkbox label, .login-remember label"
      ).on("click", function(event) {
        if ($(event.target).is("label")) {
          // console.log('label clicked');
          setupLabel();
        }
      });

      // When you click a label, set the custom form graphic
      $(
        ".gfield_radio label, .gfield_checkbox label, .login-remember label"
      ).on("click", function(event) {
        // console.log('label set up');
        setupLabel();
      });
    }
  }

  /* ######################################################################### */
  /* LOAD FUNCTIONS                                                            */
  /* ######################################################################### */

  function loadResponsive() {
    // Create JPanel instance

    jPM = $.jPanelMenu({
      menu: ".main-nav",
      trigger: ".menu-trigger",
      animated: false,
      openPosition: "48%",
      direction: "right",
      afterOn: function() {
        moveSearchIntoJPanel();
      }
    });

    // Load JPanel
    jPM.on();

    // Create jrespond instance and define breakpoints
    var jRes = jRespond([
      {
        label: "phone",
        enter: 0,
        exit: 767
      },
      {
        label: "small",
        enter: 768,
        exit: 1019
      },
      {
        label: "large",
        enter: 1020,
        exit: 100000
      }
    ]);

    // Add functions to breakpoints
    jRes.addFunc([
      /*
    {
      breakpoint: 'small',
      enter: function() {
        bp_under_desktop = true;
      },
      exit: function() {
        bp_under_desktop = false;
        console.log('exiting small');
        jPM.close();
      }
    },
*/
      {
        breakpoint: "phone",
        enter: function() {
          bp_phone = true;
          closeSiteSearch();
        },
        exit: function() {
          bp_phone = false;
        }
      },

      {
        breakpoint: "small",
        enter: function() {
          bp_under_desktop = true;
          bp_phone = false;
        },
        exit: function() {
          bp_phone = false;
          bp_under_desktop = false;
        }
      },

      {
        breakpoint: "large",
        enter: function() {
          jPM.close();
          bp_phone = false;
        },
        exit: function() {}
      }
    ]);
  }

  function QuantityAdjustor(elem) {
    this.elem = elem;
    var obj = this;
    var reduce_button = this.elem.find(".quantity-minus");
    var add_button = this.elem.find(".quantity-plus");
    var input = this.elem.find("input");

    this.init = function() {
      this.setHandlers();
    };

    this.setHandlers = function() {
      reduce_button.on("click", this.reduceIncrement);
      add_button.on("click", this.addIncrement);
    };

    this.addIncrement = function() {
      var current_value = parseInt(input.val());
      var new_val = (current_value += 1);
      input.val(new_val.toString());
    };

    this.reduceIncrement = function() {
      var current_value = parseInt(input.val());
      if (current_value > 0) {
        var new_val = (current_value -= 1);
        input.val(new_val.toString());
      }
    };
  }

  /* ######################################################################### */
  /* UTILITY FUNCTIONS                                                         */
  /* ######################################################################### */

  // Scroll to the top
  function scrollToTop() {
    $("html, body").animate({ scrollTop: 0 }, SCROLL_SPEED);
  }

  function jumpToTopOf(elem, offset) {
    var e = elem;
    var elem_offset = e.offset().top;

    var goto = elem_offset + offset;

    $("html, body").animate({ scrollTop: goto }, SCROLL_SPEED);
  }

  // Sets up the form labels for custom form label designs
  function setupLabel() {
    // console.log('setting up custom forms');
    if ($(".gfield_checkbox label input, .login-remember label input").length) {
      $(".gfield_checkbox label, .login-remember label").each(function() {
        $(this).removeClass("r_on");
      });
      $(
        ".gfield_checkbox label input:checked, .login-remember label input:checked"
      ).each(function() {
        $(this)
          .parent("label")
          .addClass("r_on");
      });
    }

    if ($(".gfield_radio label input").length) {
      $(".gfield_radio label").each(function() {
        $(this).removeClass("r_on");
      });
      $(".gfield_radio label input:checked").each(function() {
        $(this)
          .parent("label")
          .addClass("r_on");
      });
    }
  }

  // Move a copy of the search into the jpanel menu
  function moveSearchIntoJPanel() {
    var menu = $("#jPanelMenu-menu");
    var close =
      "<a href='#' class='reset-header-state'><i class='icon-cancel'></i></a>";
    var search =
      "<a href='#' class='site-search'><i class='icon-search-2'></i></a>";

    $("header a.find-a-dealer")
      .clone()
      .prependTo(menu);
    $("header .logos a")
      .clone()
      .prependTo(menu);
    menu.prepend(close);
    menu.prepend(search);

    $("header form.sitesearch")
      .clone()
      .appendTo(menu);
  }

  function closeSiteSearch() {
    clearHeaderStates("reset");
  }

  // Close the state header if you click anywhere else
  function addCloseStateHandler() {
    $(document).on("click.closestatehandler", function(event) {
      //if ($('.header-state-zip-search').hasClass('active') || $('.header-state-site-search').hasClass('active')) {

      if (
        !$(event.target).closest("header").length &&
        !$(event.target).closest("#jPanelMenu-menu").length &&
        !$(event.target).closest(".main-content-find-a-dealer-button a").length
      ) {
        //$('.reset-header-state').click();

        //clearHeaderStates('reset');
        //jPM.close();
        $("header .header-state").removeClass("active");
        $("header .main-header").removeClass("state-green");
        $("header .header-state-nav").addClass("active");
        $(document).off("click.closestatehandler");
      }
      // }
    });
  }
})(jQuery);
