<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Shikoku_Inu
 */

get_header(); ?>

<section id="primary" role="main" class="sub-page">

	<article>

			<div class="entry-header container align-center no-subhead pad-b-70">
			  <h1 class="entry-title headline">
					Error 404: Not Found
			  </h1>
				<h4 class="headline-sub subhead">
					<?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. <br/>Perhaps a search would help?', 'shikoku-inu' ); ?>
				</h4>
			</div>

		<div class="entry-content">

			<?php include 'inc/partials/search-panel.php'; ?>

		</div><!-- .entry-content -->
	</article><!-- #post-0 -->

</section><!-- #primary -->
<?php get_footer(); ?>?>
