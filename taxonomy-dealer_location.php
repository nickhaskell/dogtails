<?php
/**
 * Main Template File
 *
 * This file is used to display a page when nothing more specific matches a query.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shikoku_Inu
 */

// Vars to check to see if the taxonomy has children or parents
$term     = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); // get current term
$parent   = get_term($term->parent, get_query_var('taxonomy') ); // get parent term
$children = get_term_children($term->term_id, get_query_var('taxonomy')); // get children

get_header(); ?>

<div class="template-default template-dealer dealer-archive">
	<div class="sub-page">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if ( have_posts() ) : ?>

				<?php get_template_part( 'inc/archive-header' ); ?>

				<div class="entry-content">
					<div class="container">
						<div class="dealers">


							<?php if(($parent->term_id!="" && sizeof($children)>0)) { ?>

								<!--
								<p><small>has parent and child, this will probably not happen, Third level</small></p>
								-->

								<?php // while ( have_posts() ) : the_post(); ?>

									<?php // get_template_part( 'inc/partials/dealer-location' ); ?>

								<?php // endwhile; ?>



							<?php } elseif(($parent->term_id!="") && (sizeof($children)==0)) { ?>

								<!--
								<p><small>has parent, no child eg. Second level - Massachusetts</small></p>
								-->

								<ul class="dealer-archive-list pad-b-30">

									<?php
									$args = array(
											'posts_per_page' => -1,
									    'orderby' => 'menu_order',
									    'order' => 'ASC',
									    'post_type' => 'dealer',
											'tax_query' => array(
										    array(
										      'taxonomy' => 'dealer_location',
										      'field' => 'id',
										      'terms' => $term->term_id, // Where term_id of Term 1 is "1".
										      'include_children' => true
										    )
										  )
									);

									?>

									<?php
										$my_query = null;
										$my_query = new WP_Query($args);
										if( $my_query->have_posts() ) {
										  while ($my_query->have_posts()) : $my_query->the_post(); ?>

												<li class="dealer-archive-item">

												<?php get_template_part( 'inc/partials/dealer-location-child' ); ?>
												</li>

												<?php
										  endwhile;
										}
										wp_reset_query();

									?>


								</ul>


							<?php } elseif(($parent->term_id=="") && (sizeof($children)>0)) { ?>

								<!--
								<p><small>no parent, has child eg. Top level - United States</small></p>
								-->

									<div class="pad-b-30">

										<?php
											// Pass the children variable to the dealer location parent partial

											set_query_var( 'children', $children );
											get_template_part( 'inc/partials/dealer-location-parent' );
										?>

									</div>

							<?php } ?>

						<!--
						</div>
					-->





						<?php else : ?>

							<?php // get_template_part( 'content', 'none' ); ?>

							<div class="entry-content">
								<div class="container">
									<div class="dealers">
										<div class="entry-header container align-center  <?php if (!$subhead) { echo 'no-subhead'; } ?>">
											<h1 class="entry-title">
												<?php

													if ( is_tax() ) :
														single_term_title('Dealers: ');

													endif;
												?>
											</h1>
											<h4 class="headline-sub subhead">
												There are currently no dealers located in this region.
									    </h4>
										</div><!-- .entry-header -->


						<?php endif; ?>
						<?php include 'inc/partials/dealer-conversion-code.php'; ?>
						</div>
					</div>
					</div>
					<?php // get_template_part( 'inc/pagination' ); ?>
					<?php $options = array("find_a_dealer_form_panel" => "find_a_dealer_form_panel"); ?>
					<?php set_query_var( 'options', $options  ); ?>
					<?php include 'inc/partials/find-a-dealer-form-panel.php'; ?>
				</div>
		</article>
	</div>

<?php get_footer(); ?>
