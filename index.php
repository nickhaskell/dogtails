<?php
/**
 * Main Template File
 *
 * This file is used to display a page when nothing more specific matches a query.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shikoku_Inu
 */

get_header(); ?>
<?php include "inc/meta-vars.php"; // Need this to be an include so vars can be used in partials ?>
<div class="template-default template-blog">
	<div class="sub-page">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php include 'inc/partials/entry-header.php'; ?>

			<div class="entry-content">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', get_post_format() ); ?>

					<?php endwhile; ?>

					<?php get_template_part( 'inc/pagination' ); ?>

				<?php else : ?>

					<?php get_template_part( 'content', 'none' ); ?>

				<?php endif; ?>

			</div>
		</article>
	</div>
</div>


<?php get_footer(); ?>
