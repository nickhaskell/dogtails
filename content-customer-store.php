<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Shikoku_Inu
 */
?>

<?php include "inc/meta-vars.php"; // Need this to be an include so vars can be used in partials ?>
<div class="sub-page">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php include 'inc/partials/entry-header.php'; ?>
		<?php include 'inc/partials/hero.php'; ?>

		<div class="entry-content">

			<?php include 'inc/partials/main-content-panel.php'; ?>
			<?php include 'inc/partials/product-hero-new.php'; ?>
			<?php include 'inc/partials/slider-panel-new.php'; ?>
			<?php include 'inc/partials/page-cta.php'; ?>
			<?php include 'inc/partials/showhides-panel.php'; ?>
			<?php include 'inc/partials/video-panel.php'; ?>
			<?php include 'inc/partials/info-table.php'; ?>
			<?php include 'inc/partials/featured-panel.php'; ?>
			<?php include 'inc/partials/text-panel.php'; ?>
			<?php include 'inc/partials/other-video-panel.php'; ?>
			<?php include 'inc/partials/photo-text-panel.php'; ?>
			<?php include 'inc/partials/faq-panel.php'; ?>
			<?php include 'inc/partials/comparison-panel.php'; ?>
			<?php include 'inc/partials/find-a-dealer-form-panel.php'; ?>
			<?php include 'inc/partials/customer-story.php'; ?>
			

		</div>
	</article>
</div>

<?php include 'inc/partials/footer-image.php'; ?>
<!--noptimize-->
<?php
if ($page_scripts != null && $page_scripts != "") {
	echo $page_scripts;
}
?>
<!--/noptimize-->
