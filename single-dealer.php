<?php
/**
 * Single dealer template
 *
 * @package Shikoku_Inu
 */

get_header(); ?>

<div class="template-dealer template-default">
	<?php while ( have_posts() ) : the_post(); ?>


		<?php get_template_part( 'content', 'page' ); ?>

	<?php endwhile; // end of the loop. ?>

	<?php // $options = array("find_a_dealer_form_panel" => "find_a_dealer_form_panel"); ?>
	<?php // set_query_var( 'options', $options  ); ?>
	<?php // include 'inc/partials/find-a-dealer-form-panel.php'; ?>

</div><!-- #primary -->

<?php get_footer(); ?>
