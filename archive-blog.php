<?php
/**
 * Main Template File
 *
 * This file is used to display a page when nothing more specific matches a query.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shikoku_Inu
 */

$image_id     = blog_get_option('image_id');

if ($image_id) {
$image_1x     = wp_get_attachment_image_src($image_id, 'hero_product_header_new@1x');
$image_2x     = wp_get_attachment_image_src($image_id, 'hero_product_header_new@2x');
$image_alt    = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
$image_title  = get_the_title($image_id);
$image_width  = $image_1x[1];
$image_height = $image_1x[2];

if (exif_imagetype($image_1x[0]) == 3) {
		$im = imagecreatefrompng($image_1x[0]);
}
if (exif_imagetype($image_1x[0]) == 2) {
		$im = imagecreatefromjpeg($image_1x[0]);
}

$rgb = imagecolorat($im, 0, 0);
$colors = imagecolorsforindex($im, $rgb);
$image_background_color = "background-color: rgba(".$colors['red'].",".$colors['green'].",".$colors['blue'].",0.35);";

$skroll_to = "transform: translate3d(0px, 33.333333%, 0px)";
$skroll_from = "transform: translate3d(0px, 0%, 0px)";
$skroll_start = "transform: translate3d(0px, 0%, 0px)";
}

get_header(); ?>

<!--noptimize-->
<style media="screen">
	<?php include("dist/styles/templates/blog-new.css"); ?>
</style>
<!--/noptimize-->

<?php if ($image_id): ?>
<div class="hero dark">
	<ul class="single-image-slider product-new-slider">
		<li>
			<div class="slide-container skrollable-hero" data-start="<?php echo $skroll_from;?>" data-top-bottom="<?php echo $skroll_to;?>" style="<?php echo $skroll_start;?>">
				<picture class="intrinsic"
								 style="padding-top: 44.5833333333%; <?php echo $image_background_color; ?>">
					<source srcset="<?php echo $image_1x[0]; ?> 1x,
													<?php echo $image_2x[0]; ?> 2x">
					<img src="<?php echo $image_1x[0]; ?>"
							 alt="<?php echo $image_alt; ?>"
							 title="<?php echo $image_title; ?>"
							 width="<?php echo $image_width; ?>"
							 height="<?php echo $image_height; ?>"
							 class="intrinsic-item">
				</picture>
				<div class="slide-copy-container">
					<div class="slide-copy">
						<h1 class="fork"><?php echo blog_get_option('page_title'); ?></h1>
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>
<?php endif; ?>

<div class="template-blog-new">
	<article class="entry-content container sub-page">
		<div class="row blog-content">
			<div class="col-sm-12">
				<div class="blog-post-single blog-posts-feed">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'blog-excerpt' ); ?>
						<?php endwhile; ?>
						<?php get_template_part( 'inc/pagination-full' ); ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
				<?php include(locate_template("inc/partials/blog-posts-sidebar.php")); ?>
			</div>
		</div>
	</article>
</div>
<?php get_footer(); ?>
