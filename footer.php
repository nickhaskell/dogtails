<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Shikoku_Inu
 */
?>

</div> <?php // end of #main ?>

<footer class="dark">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 margin-b-30 left-col-widgets">
				<div class="footer-navigation">
					<div class="row">
						<div class="col-sm-10">
							<div class="row first-row margin-b-30">
								<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-position-1')): ?>
										<?php the_widget('footer-position-1');?>
								<?php endif;?>
							</div>
						</div> <?php // End footer Navigation ?>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<a href="http://www.dogwatch.com/" class="btn green-green-green" target="_blank">Visit DogWatch</a>
						</div>
					</div>
				</div>
			</div> <?php // End left column container ?>
		</div>
		<div class="row copywrite">
			<div class="col-sm-12 pad-b-30">
				<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-copyright')): ?>
						<?php the_widget('footer-copyright');?>
				<?php endif;?>
			</div>
		</div>
	</div>
</footer>
<?php get_template_part("/inc/partials/modal");?>
<?php get_template_part("/inc/partials/subscribe-form");?>
<!-- wpengine dogtails -->
<?php wp_footer();?>

</body>
</html>
