<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Shikoku_Inu
 */
?>

<?php include "inc/meta-vars.php"; // Need this to be an include so vars can be used in partials ?>
<div class="sub-page">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php include 'inc/partials/dealer-nav.php'; ?>
		<?php include 'inc/partials/entry-header.php'; ?>
		<?php include 'inc/partials/hero.php'; ?>

		<div class="entry-content">

		<?php if ($post_type == "page" && $template == "page-training-products.php") { ?>
			<?php include 'inc/partials/featured-panel.php'; ?>
			<?php include 'inc/partials/training-product-fork.php'; ?>
			<?php include 'inc/partials/home-trust-box.php'; ?>
		<?php } ?>

		</div>

	</article>
</div>

<?php include 'inc/partials/footer-image.php'; ?>
<?php
if ($page_scripts != null && $page_scripts != "") {
	echo $page_scripts;
}
?>
