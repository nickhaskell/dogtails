<?php
/**
 * Main Template File
 *
 * This file is used to display a page when nothing more specific matches a query.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shikoku_Inu
 */

get_header(); ?>

<div class="template-blog-new">
	<article class="entry-content container sub-page">
	
		<?php include(locate_template("inc/partials/blog-breadcrumb.php")); ?>


		<?php if ( have_posts() ) : ?>
		<div class="row blog-content">
			<div class="col-sm-12">
				<div class="blog-post-single blog-posts-feed">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'blog-excerpt' ); ?>
						<?php endwhile; ?>
						<?php get_template_part( 'inc/pagination-full' ); ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
				<?php include(locate_template("inc/partials/blog-posts-sidebar.php")); ?>
			</div>
		</div>
		<?php endif; ?>
	</article>
</div>

<?php get_footer(); ?>
