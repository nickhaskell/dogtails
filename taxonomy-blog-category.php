<?php
/**
 * Main Template File
 *
 * This file is used to display a page when nothing more specific matches a query.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shikoku_Inu
 */

get_header(); ?>

<?php get_template_part( 'content', 'blog-taxonomy' ); ?>

<?php get_footer(); ?>
