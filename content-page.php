<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Shikoku_Inu
 */
?>

<?php include "inc/meta-vars.php"; // Need this to be an include so vars can be used in partials ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('sub-page entry-content container pad-b-120'); ?>>

	<?php include 'inc/partials/entry-header.php'; ?>

	<div class="entry-content">

		<div class="container pad-b-70">
			<div class="row blog-content">
				<div class="col-sm-12">
					<div class="blog-post-single">
						<?php the_content(); ?>
					</div>
					<?php include(locate_template("inc/partials/blog-posts-sidebar.php")); ?>
				</div>
			</div>
		</div>

	</div>
</article>

<!--noptimize-->
<?php
if ($page_scripts != null && $page_scripts != "") {
	echo $page_scripts;
}
?>
<!--/noptimize-->
