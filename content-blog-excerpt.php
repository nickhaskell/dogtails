<?php
/**
 * The default template for displaying content
 *
 * @package Shikoku_Inu
 */
 $video_url       = get_post_meta( $id, 'meta_video_url', true);
?>

<div id="post-<?php the_ID(); ?>" class="blog-post blog-post-excerpt">

		<div class="blog-post-single unmargin-last">
			<p class="post-date">
				<strong><?php echo get_the_date(); ?></strong>
			</p>
			<?php if ($video_url): ?>
				<?php echo wp_oembed_get($video_url); ?>
			<?php else: ?>
				<?php if (get_post_thumbnail_id()): ?>
					<div class="post-image">
						<a href="<?php the_permalink(); ?>">
							<?php
								$image_id = get_post_thumbnail_id();
								$image_class = "picture";
								$image_size = "blog_column";
								include(locate_template( "inc/partials/picture.php"));
							?>
						</a>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<h2 class="post-title">
				<a class="green" href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</h2>
			<div class="post-content">
				<?php the_content('Read post »'); ?>
			</div>
		</div>
</div>
