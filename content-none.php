<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Shikoku_Inu
 */
if (get_query_var('post_type')) {
	$posttype = get_query_var('post_type');
}
?>

<div class="sub-page">
	<article class="post no-results not-found">
		<div class="entry-content">
			<?php if ( is_archive() && $posttype != 'dealer') : ?>

				<!--
				<p><?php _e( 'There are no published posts for this archive. Try searching using keywords instead.', 'shikoku-inu' ); ?></p>
				-->
			<?php elseif ( is_archive() && $posttype == 'dealer') : ?>

				<?php $options[] = 'find_a_dealer_form_panel'; ?>
				<?php include 'inc/partials/find-a-dealer-form-panel.php'; ?>

			<?php elseif ( is_search() ) : ?>
				<!--
				<p><?php _e( 'No matches were found for your search terms. Please try again with different keywords.', 'shikoku-inu' ); ?></p>
				-->
				<?php include 'inc/partials/search-panel.php'; ?>

			<?php else : ?>

				<!--
				<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps a search would help?', 'shikoku-inu' ); ?></p>
				-->
				<?php include 'inc/partials/search-panel.php'; ?>

			<?php endif; ?>
		</div><!-- .entry-content -->
	</article>
</div>
