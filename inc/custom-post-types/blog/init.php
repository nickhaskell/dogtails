<?php
  add_action( 'init', 'mytheme_custom_post_blog' );

  function mytheme_custom_post_blog() {
      $labels = array(
          'name' => _x( 'Blog Posts', 'blog' ),
          'singular_name' => _x( 'Blog Post', 'blog' ),
          'add_new' => _x( 'Add New', 'blog' ),
          'all_items' => _x( 'Blog Posts', 'blog' ),
          'add_new_item' => _x( 'Add New Blog Post', 'blog' ),
          'edit_item' => _x( 'Edit Blog Post', 'blog' ),
          'new_item' => _x( 'New Blog Post', 'blog' ),
          'view_item' => _x( 'View Blog Post', 'blog' ),
          'search_items' => _x( 'Search Blog Posts', 'blog' ),
          'not_found' => _x( 'No blog posts found', 'blog' ),
          'not_found_in_trash' => _x( 'No blog posts found in Trash', 'blog' ),
          'parent_item_colon' => _x( 'Parent Blog Post:', 'blog' ),
          'menu_name' => _x( 'Blog Posts', 'blog' ),
      );
      $args = array(
          'labels'          => $labels,
          'hierarchical'    => false,
          'public'          => true,
          'show_in_nav_menus' => true,
          'publicly_queryable' => true,
          'show_ui'         => true,
          'show_in_menu'    => true,
          'menu_position'   => 8,
          'query_var'       => true,
          'has_archive'     => 'dogwatch-blog',
          //'has_archive'     => 'false',
          'can_export'      => true,
          'capability_type' => 'post',
          'menu_icon'       => 'dashicons-format-status',
          'supports'        => array('title', 'thumbnail', 'author', 'post_tag', 'editor'),
          'rewrite'         => array('slug' => 'dogwatch-blog')
      );
      register_post_type( 'blog', $args );
  }

// ----------------------------------------------------------------------------
// Blog taxonomies
// ----------------------------------------------------------------------------

function my_taxonomies_blog() {
  $labels_topic = array(
    'name'              => _x( 'Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Blog Categories' ),
    'all_items'         => __( 'All Categories' ),
    'parent_item'       => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item'         => __( 'Edit Category' ),
    'update_item'       => __( 'Update Category' ),
    'add_new_item'      => __( 'Add New Category' ),
    'new_item_name'     => __( 'New Category' ),
    'menu_name'         => __( 'Categories' ),
  );
  $labels_topic = array(
    'labels'            => $labels_topic,
    'hierarchical'      => true,
    'show_admin_column' => true,
    'rewrite'           => array(
      'with_front'   => true,
      'slug'         => 'dogwatch-blog/category',
      'hierarchical' => true,
    ),
  );
  register_taxonomy( 'blog-category', 'blog', $labels_topic );

  $labels_publication = array(
    'name'              => _x( 'Tags', 'taxonomy general name' ),
    'singular_name'     => _x( 'Tags', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Blog Tags' ),
    'all_items'         => __( 'All Tags' ),
    'parent_item'       => __( 'Parent Tag' ),
    'parent_item_colon' => __( 'Parent Tag:' ),
    'edit_item'         => __( 'Edit Tag' ),
    'update_item'       => __( 'Update Tag' ),
    'add_new_item'      => __( 'Add New Tag' ),
    'new_item_name'     => __( 'New Tag' ),
    'menu_name'         => __( 'Tags' ),
  );
  $args_publication = array(
    'labels'            => $labels_publication,
    'hierarchical'      => false,
    'show_admin_column' => true,
    'rewrite'           => array(
      'with_front'   => true,
      'slug'         => 'dogwatch-blog/tag',
      'hierarchical' => false,
    ),
  );
  register_taxonomy( 'blog-tag', 'blog', $args_publication );


}
add_action( 'init', 'my_taxonomies_blog', 0 );



// Filter custom post type by custom taxomony
function filter_by_category() {
    global $typenow;
    $post_type = 'blog'; // change HERE
    $taxonomy = 'blog-category'; // change HERE
    if ($typenow == $post_type) {
      $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
      $info_taxonomy = get_taxonomy($taxonomy);
      wp_dropdown_categories(array(
        'show_option_all' => __("Show All {$info_taxonomy->label}"),
        'taxonomy' => $taxonomy,
        'name' => $taxonomy,
        'orderby' => 'name',
        'selected' => $selected,
        'show_count' => true,
        'hide_empty' => true,
      ));
    };
  }
  add_action('restrict_manage_posts', 'filter_by_category');

// Filter custom post type by custom taxomony
function filter_by_tag() {
    global $typenow;
    $post_type = 'blog'; // change HERE
    $taxonomy = 'blog-tag'; // change HERE
    if ($typenow == $post_type) {
      $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
      $info_taxonomy = get_taxonomy($taxonomy);
      wp_dropdown_categories(array(
        'show_option_all' => __("Show All {$info_taxonomy->label}"),
        'taxonomy' => $taxonomy,
        'name' => $taxonomy,
        'orderby' => 'name',
        'selected' => $selected,
        'show_count' => true,
        'hide_empty' => true,
      ));
    };
  }
  add_action('restrict_manage_posts', 'filter_by_tag');

  function convert_id_to_term_in_query_category($query) {
    global $pagenow;
    $post_type = 'blog'; // change HERE
    $taxonomy = 'blog-category'; // change HERE
    $q_vars = &$query->query_vars;
    if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
      $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
      $q_vars[$taxonomy] = $term->slug;
    }
  }
  add_filter('parse_query', 'convert_id_to_term_in_query_category');

  function convert_id_to_term_in_query_tag($query) {
    global $pagenow;
    $post_type = 'blog'; // change HERE
    $taxonomy = 'blog-tag'; // change HERE
    $q_vars = &$query->query_vars;
    if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
      $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
      $q_vars[$taxonomy] = $term->slug;
    }
  }
  add_filter('parse_query', 'convert_id_to_term_in_query_tag');

  require_once('fields.php');
  require_once('settings.php');
?>
