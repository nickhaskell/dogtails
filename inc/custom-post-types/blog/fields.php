<?php
add_filter( 'cmb2_meta_boxes', 'blog_meta' );
function blog_meta( array $meta_boxes ) {
  $prefix = 'meta_';

  // Toolbars ---------------------------------------------------------------

  $minimal['toolbar1'] = 'bold, italic, link, unlink, bullist, numlist, removeformat';
  $minimal['toolbar2'] = '';

  // Vars -------------------------------------------------------------------

  $blog_name_first = array(
    'name' => 'First Name',
    'id'   => $prefix . 'first_name',
    'type' => 'text',
    'description' => 'The Team Blog\'s first name, eg. "Jane"',
  );

  $blog_name_last = array(
    'name' => 'Last Name',
    'id'   => $prefix . 'last_name',
    'type' => 'text',
    'description' => 'The Team Blog\'s last name, eg. "Smith"',
  );

  $blog_accreditations = array(
    'name' => 'Professional Accreditations',
    'id'   => $prefix . 'accreditations',
    'type' => 'text',
    'description' => 'Comma separated list of accreditations that gets appended to the team blog\'s name as in, "Jane Smith, PhD"',
  );

  $blog_title = array(
    'name' => 'Professional Title',
    'id'   => $prefix . 'title',
    'type' => 'textarea_small',
    'description' => 'The team blog\'s full professional title, eg. "Director, Center for Health Enhancement Systems Studies"',
  );

  $blog_tags = array(
    'name' => 'Tags',
    'id'   => $prefix . 'tags',
    'type' => 'textarea_small',
    'description' => 'A semi-colon separated list of tags associated with the team blog, eg. "Ambient Intelligence; Ubiquitous Computing; User-Interface Design"',
  );

  $blog_image = array(
    'name' => 'Image',
    'id'   => $prefix . 'image',
    'type' => 'file',
    'text'    => array(
      'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
    ),
  );

  $blog_bio = array(
	 'name' => 'Bio',
	 'id'      => $prefix . 'bio',
	 'type'    => 'wysiwyg',
	 // 'escape_cb' => false,
	 // 'sanitization_cb' => false,
	 'options' => array(
	   'media_buttons' => false,
	   'textarea_rows' => 10,
	   'tinymce' => $minimal,
	 ),
	);

  $blog_address = array(
    'name' => 'Address',
    'id'   => $prefix . 'address',
    'type' => 'textarea_small',
    'description' => 'eg. "46 Centerra Parkway, Suite 300-301 , Lebanon, NH 03766"'
  );

  $blog_phone = array(
    'name' => 'Phone Number',
    'id'   => $prefix . 'phone',
    'type' => 'text',
  );

  $blog_email = array(
    'name' => 'Email Address',
    'id'   => $prefix . 'email',
    'type' => 'text',
  );

  $blog_publications = array(
    'name'      	=> 'Publications',
    'id'        	=> $prefix . 'publications',
    'type'      	=> 'post_search_ajax',
    'desc'			=> __( '(Start typing post publication name)', 'cmb2' ),
    'limit'      	=> 10, 		// Limit selection to X items only (default 1)
    'sortable' 	 	=> false, 	// Allow selected items to be sortable (default false)
    'query_args'	=> array(
      'post_type'			=> array( 'publication' ),
      'post_status'		=> array( 'publish' ),
      'posts_per_page'	=> -1
    )
  );

  /*
  $blog_publications = array(
    'name'           => 'Publication',
    'id'             => $prefix . 'blog_publication',
    'taxonomy'       => 'publication', // Enter Taxonomy Slug
    'type'           => 'taxonomy_radio',
    'remove_default' => 'true'
  );
  */

  /*
  $blog_topics = array(
    'name'           => 'Topics',
    'id'             => $prefix . 'blog_topics',
    'taxonomy'       => 'topic', // Enter Taxonomy Slug
    'type'           => 'taxonomy_multicheck',
    'remove_default' => 'true'
  );
  */

  // Groups -----------------------------------------------------------------

  $meta_boxes['featured_video'] = array(
    'id'         => 'featured_video',
    'title'      => __( 'Featured Video', 'cmb2' ),
    'object_types'      => array( 'post', ), // Post type
    'context'    => 'side', //  'normal', 'advanced', or 'side'
    'priority'   => 'low', //  'high', 'core', 'default' or 'low'
    'show_names' => false, // Show field names on the left
    'closed'       => false,
    'fields'     => array(
      array(
        'name' => 'Video URL',
        'id'   => $prefix . 'video_url',
        'type' => 'oembed',
        'desc' => 'For feature image video, eg "https://www.youtube.com/watch?v=T79Jzsdg0Mw&rel=0"'
      ),
    )
  );

  $meta_boxes['blog_post_options'] = array(
    'id'         => 'blog_post_options',
    'title'      => __( 'Blog Post Options', 'cmb2' ),
    'object_types'      => array( 'post', ), // Post type
    'context'    => 'normal', //  'normal', 'advanced', or 'side'
    'priority'   => 'low', //  'high', 'core', 'default' or 'low'
    'show_names' => true, // Show field names on the left
    'closed'       => false,
    'fields'     => array(
        array(
          'name' => 'CTA Headline',
          'id'   => $prefix . 'cta_headline',
          'type' => 'text',
        ),
        array(
          'name' => 'CTA Button URL',
          'id'   => $prefix . 'cta_button_url',
          'type' => 'text',
          'default' => '#'
        ),
        array(
          'name' => 'CTA Button Text',
          'id'   => $prefix . 'cta_button_text',
          'type' => 'text',
          'default' => 'Learn More'
        ),
      ),
    );

  /*
  $meta_boxes['blog_publications'] = array(
    'id'           => 'blog_publications',
    'title'        => 'Selected Publications',
    'object_types' => array( 'blog' ), // Post type
    'context'      => 'normal', //  'normal', 'advanced', or 'side'
    'priority'     => 'high', //  'high', 'core', 'default' or 'low'
    'show_names'   => true, // Show field names on the left
    'fields'       => array(
      $blog_publications,
    ),
  );
  */

  /*
  $meta_boxes['blog_sidebar_topics'] = array(
    'id'           => 'blog_sidebar_topics',
    'title'        => 'Topics',
    'object_types' => array( 'blogs', ), // Post type
    'context'      => 'side', //  'normal', 'advanced', or 'side'
    'priority'     => 'core', //  'high', 'core', 'default' or 'low'
    'show_names'   => false, // Show field names on the left
    'fields'       => array(
      // $blog_topics,
    ),
  );
  */

  /*
  $meta_boxes['blog_sidebar_publications'] = array(
    'id'           => 'blog_sidebar_publications',
    'title'        => 'Publications',
    'object_types' => array( 'blogs', ), // Post type
    'context'      => 'side', //  'normal', 'advanced', or 'side'
    'priority'     => 'core', //  'high', 'core', 'default' or 'low'
    'show_names'   => false, // Show field names on the left
    'fields'       => array(
      // $blog_publications,
    ),
  );
  */

	return $meta_boxes;
}
?>
