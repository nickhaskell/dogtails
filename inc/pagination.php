<?php
/**
 * Archive pagination
 *
 * @package Shikoku_Inu
 */
?>

<div class="panel panel-white panel-pagination pad-b-120">
		<div class="row">
			<div class="col-sm-6 nav-next">
				<?php previous_posts_link( __( 'Newer posts', 'shikoku-inu' ) ); ?>
			</div>
			<div class="col-sm-6 nav-previous">
				<?php next_posts_link( __( 'Older posts', 'shikoku-inu' ) ); ?>
			</div>
		</div>
</div>
