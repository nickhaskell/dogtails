<div id="modal" class="modal absolute-fill r r--halign-center" data-additional-classes="" aria-hidden="true">
    <div class="modal__card">
        <button class="modal__close btn--svg"><?php get_template_part("/inc/svg/close");?></button>
        <div class="modal__content">
            <div class="modal__content__image"></div>
            <div class="modal__content__text type--content-margins type--content-styles"></div>
        </div>
        <div class="modal__footer r r--justify-center r--halign-center"></div>
    </div>
    <div class="modal__background absolute-fill"></div>
</div>