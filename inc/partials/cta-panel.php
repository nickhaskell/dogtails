<?php
  $headline = get_post_meta( $id, '_cmb2_cta_headline', true );;
  $button_text = get_post_meta( $id, '_cmb2_cta_button_text', true );;
  $button_url = get_post_meta( $id, '_cmb2_cta_button_url', true );
?>

<?php if (in_array('cta_panel', $options)) { ?>
<div class="cta-panel panel">
  <div class="container pad-t-50 pad-b-50">
    <div class="row">
      <div class="col-sm-12 align-center">
        <div class="content">
          <?php if ($headline): ?>
            <h2 class="color-white"><?php echo $headline; ?></h2>
          <?php endif; ?>
          <?php if ($button_url): ?>
            <a class="btn halloween" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
