<div class="row blog-header-content">
  <div class="col-sm-12 unmargin-last">
    <p class="breadcrumb">
      <a href="<?php echo get_home_url('blog'); ?>">< Back</a>
    </p>
    <?php if (is_archive()): ?>
    <h1 class="headline">
      <?php single_term_title(); ?>
    </h1>
    <?php endif; ?>
  </div>
</div>
