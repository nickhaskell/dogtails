<?php
  $tab1_title = get_post_meta( $id, '_cmb2_tab_one_title', true );
	$tab1       = get_post_meta( $id, '_cmb2_info_table_tab_one', true );
  $tab2_title = get_post_meta( $id, '_cmb2_tab_two_title', true );
	$tab2       = get_post_meta( $id, '_cmb2_info_table_tab_two', true );

  $include_padding = get_post_meta( $id, '_cmb2_info_table_include_bottom_padding', true );

  $info_pad_class = "";

  if ($include_padding == 'yes') {
    $info_pad_class .= ' pad-b-70';
  } else {
    $info_pad_class .= '';
  }

  if ($tab1_title != "" && $tab2_title != "") {
    $info_pad_class .= ' multitab';
  } else if ($tab1_title != "" && $tab2_title == "") {
    $info_pad_class .= ' singletab';
  } else if ($tab1_title == "" && $tab2_title != "") {
    $info_pad_class .= ' singletab';
  } else {
    $info_pad_class .= ' notab';
  }
?>

<?php if (in_array('info_table', $options)) { ?>

<div class="panel panel-white info-table-panel <?php echo $info_pad_class; ?>">
  <div class="info-table container">
  	<div class="row info-tabs">
      <div class="col-sm-12">

        <?php if ($tab1_title): ?>
          <div class="info-tab active" data-name="info-table-1">
            <div class="triangle"></div>
            <span><?php echo $tab1_title; ?></span>
          </div>
          <div class="info-tab-list" data-name="info-table-1"></div>
        <?php endif; ?>

        <?php if ($tab2_title): ?>
          <div class="info-tab" data-name="info-table-2">
            <div class="triangle"></div>
            <span><?php echo $tab2_title; ?></span>
          </div>
          <div class="info-tab-list" data-name="info-table-2"></div>
        <?php endif; ?>

      </div>
  	</div>

    <div class="info-lists">

      <?php if (count($tab1) > 0): ?>
    	<div class="info-list container active" data-name="info-table-1">
    		<?php for ($i = 0; $i < count($tab1); $i ++) { ?>

    			<div class="info-container row">
    				<div class="info-container-inner">
    					<div class="info-block info-title col-sm-4">
    						<h4><?php echo $tab1[$i]['title']; ?></h4>
    					</div>
    					<div class="info-block info-description col-sm-8">
    						<p><?php echo $tab1[$i]['description']; ?></p>
    					</div>
    				</div>
    			</div>

    		<?php } ?>
      </div>
    <?php endif; ?>

    <?php if (count($tab2) > 0 && $tab2_title != ""): ?>
      <div class="info-list container" data-name="info-table-2">
    		<?php for ($i = 0; $i < count($tab2); $i ++) { ?>

    			<div class="info-container row">
    				<div class="info-container-inner">
    					<div class="info-block info-title col-sm-4">
    						<h4><?php echo $tab2[$i]['title']; ?></h4>
    					</div>
    					<div class="info-block info-description col-sm-8">
    						<p><?php echo $tab2[$i]['description']; ?></p>
    					</div>
    				</div>
    			</div>

    		<?php } ?>
    	</div>
    <?php endif; ?>

    </div>
  </div>
</div>

<?php } ?>
