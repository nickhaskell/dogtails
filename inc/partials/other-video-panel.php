<?php
  $image_id       = get_post_meta( $id, '_cmb2_other_video_image_id', true );
  $image_1x       = wp_get_attachment_image_src( $image_id, 'video_placeholder@1x' );
  $image_2x       = wp_get_attachment_image_src( $image_id, 'video_placeholder@2x' );
  $image_alt      = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
  $image_title    = get_the_title($image_id);

  $videos = get_post_meta( $id, '_cmb2_other_videos', true );
  $single_video = false;

  if (count($videos) == 1) {
    $single_video = true;
  }
?>

<?php if (in_array('other_video_panel', $options)) { ?>

<div class="panel panel-white other-video-panel pad-tb-70">
  <div class="container">
    <div class="row">

      <?php if ($single_video): ?>

      <div class="col-sm-12 other-video-holder single-video">
        <?php if ((count($videos) > 0 && $videos != null)): ?>
          <div id="player" class="video-player">
            <div class="video-player-container">
              <?php echo wp_oembed_get($videos[0]['embed']); ?>
            </div>
          </div>
        <?php endif ?>
      </div>

      <?php else: ?>

        <div class="col-l col-sm-8 other-video-holder">

          <?php if ((count($videos) > 0 && $videos != null)): ?>
          	<div id="player" class="video-player">
              <div class="video-player-container">
                <div class="play-overlay">
                  <div class="play-button"></div>
                  <div class="play-icon">
                    <?php echo file_get_contents(get_template_directory_uri() . "/assets/images/svg/play.svg"); ?>
                  </div>
                  <div class="video-placeholder">
                    <picture>
                      <source
                        srcset="<?php echo $image_1x[0]; ?> 1x,
                                <?php echo $image_2x[0]; ?> 2x">
                      <img
                        src="<?php echo $image_1x[0]; ?>"
                        alt="<?php echo $image_alt; ?>"
                        title="<?php echo $image_title; ?>"
                        width="<?php echo $image_1x[1]; ?>"
                        height="<?php echo $image_1x[2]; ?>">
                    </picture>
                  </div>
                </div>
                <iframe id="myIframe" width="660" height="331"></iframe>
              </div>
          	</div>
          <?php endif ?>

        </div>
        <div class="col-r col-sm-4 other-video-links">

          <? if (count($videos) > 0 && $videos != null) { ?>
          <?php $has_videos = true; ?>
          <div class="videos videos-left">
          	<?php for ($i = 0; $i < count($videos); $i ++) { ?>

          		<?php
          			$ov_title = $videos[$i]['title'];
          			$ov_embed = $videos[$i]['embed'];
          		?>

          		<div class="video-container">
          			<div class="video-link" data-video-embed='<?php echo addslashes($ov_embed); ?>'>
          				<span class="subhead">
          					<?php echo $ov_title; ?>
          				</span>
          			</div>
          		</div>

          	<?php } ?>
          </div>
          <?php } ?>


        </div>

        <?php /*
        <script type="text/javascript" src="https://www.youtube.com/iframe_api" defer></script>
        */ ?>
        <script type="text/javascript">

          (function( $ ) {

            var youtube_api      = 'https://www.googleapis.com/youtube/v3/videos';
            var youtube_api_key  = 'AIzaSyA-wIk7j1BKK4wMsKLzTxQZ96CQA2RCyw0';
            var youtube_api_part = "part=snippet";
            var video_panel      = $('.other-video-panel');
            var player           = video_panel.find('#player');
            var video_iframe     = video_panel.find('#myIframe');
            var video_links      = video_panel.find('.other-video-links .videos');
            var video_placeholder= video_panel.find('.play-overlay');
            var video_play_button= video_placeholder.find('.play-button');
            var first_link       = video_links.find('.video-link').first();
            var video            = null;

            setInitialState();
            addClickHandlers();

            function setInitialState () {
              first_link.toggleClass('active');
            }

            function addClickHandlers () {
              video_play_button.on('click', function() {
                var first_embed      = getVideoID(first_link);
                var first_video_url  = getVideoURL(first_embed);
                getVideo(first_video_url);
                scrollUp();
              });
              video_links.find('.video-link').on('click', function() {
                var embed            = getVideoID($(this));
                var video_url        = getVideoURL(embed);
                clearActiveLinks();
                $(this).toggleClass('active');
                getVideo(video_url);
                scrollUp();
              });
            }

            function clearActiveLinks () {
              video_links.find('.video-link').each(function() {
                $(this).removeClass('active');
              });
            }

            function scrollUp () {
              $('html, body').animate({
                scrollTop: player.offset().top - 90
              }, SCROLL_SPEED);
            }

            function removePlaceholder () {
              setTimeout(function () {
                video_placeholder.remove();
              }, 500);
            }

            function getVideoID (elem) {
              var video_id = elem.attr('data-video-embed').replace(/\\"/g, '"');
              return video_id;
            }

            function getVideoURL(embed_string) {
              var embed_slash_array  = embed_string.split('/');
              var embed_quote_array  = embed_slash_array[3].split('"');
              var embed_equals_array = embed_quote_array[0].split('=');
              var embed_id           = embed_equals_array[1];

              <?php // this is for embeds that include a list ?>
              if (embed_id.includes("?list")) {
                  temp_array = embed_id.split("?");
                  embed_id = temp_array[0];
              }

              var video_url        = youtube_api + "?" + "key=" + youtube_api_key + "&" + "id=" + embed_id + "&" + youtube_api_part;
              return video_url;
            }

            function getVideo(video_url) {
              var extension = "?wmode=transparent&rel=0&autoplay=1";
              <?php // var extension = "?wmode=transparent&rel=0&enablejsapi=1";?>

              $.get(video_url, function(response) {
                var url = "https://www.youtube.com/embed/" + response.items[0].id + extension;
                video_iframe.attr('src', url);
                <?php // createYouTubePlayer(response.items[0].id) ?>
              });

              if (!player.hasClass('active')) {
                player.toggleClass('active');
              }

              if (video_placeholder.length) {
                removePlaceholder();
              }
            }

            <?php /*
            function createYouTubePlayer(id) {
              video = new YT.Player('myIframe', {
                videoId: id,
                events: {
                  'onReady': onPlayerReady
                }
              });
            }

            function onPlayerReady(event) {
              event.target.playVideo();
            }
            */ ?>
          })(jQuery);
        </script>

      <?php endif; ?>

    </div>
  </div>
</div>
<?php } ?>
