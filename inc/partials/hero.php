<?php
	$slides = get_post_meta( $id, '_cmb2_hero_slider', true );
	$type   = get_post_meta( $id, '_cmb2_hero_type', true );

	if (is_array($slides) && count($slides) == 1) {
		$class = "single-image-slider";
	} else {
		$class = "slider";
	}

	if ($type == "product") {
		$class .= " product-slider";
	} else if ($type == "product-new") {
		$class .= " product-new-slider";
	} else {
		$class .= " home-slider";
	}

	if ($options == null) {
		$options = array("" => "");
	}

	/*
	$skroll_to = "transform: scale(1);";
	$skroll_from = "transform: scale(1.15);";
	$skroll_start = "transform: scale(1.15);";
	*/
	$skroll_to = "transform: translate3d(0px, 33.333333%, 0px)";
	$skroll_from = "transform: translate3d(0px, 0%, 0px)";
	$skroll_start = "transform: translate3d(0px, 0%, 0px)";
?>

<?php if (in_array('hero', $options)) { ?>

<?php if (count($slides) > 0 && $slides != null) { ?>
<div class="hero dark">
		<ul class="<?php echo $class; ?>">
			<?php foreach ($slides as $key => $slide) { ?>

			<?php
				$head    = $slide['headline'];
				$subhead = $slide['sub_headline'];
				$link    = $slide['link'];
				$image_1x= $slide['image'];
				$image_2x= $slide['image'];
				$mobile  = $slide['mobile_image'];
				$image_alt   = get_post_meta($slide['image_id'])['_wp_attachment_image_alt'][0];
				$image_title = get_the_title($slide['image_id']);
				$intrinsic_padding = "";

				if ($type == "home") {
					$image_1x = wp_get_attachment_image_src( $slide['image_id'], 'hero_home_new' );
					$intrinsic_padding = "min-height:440px;";
				}
				if ($type == "product") {
					$image_1x = wp_get_attachment_image_src( $slide['image_id'], 'hero_product' );
				}
				if ($type == "product-new") {
					$image_1x = wp_get_attachment_image_src( $slide['image_id'], 'hero_product_header_new@1x' );
				}

				$w                 = $image_1x[1];
				$h                 = $image_1x[2];
				$ratio             = ($h / $w) * 100;
				$intrinsic_padding .= "padding-top: " . $ratio . "%;";

				if (exif_imagetype($image_1x[0]) == 3) {
				    $im = imagecreatefrompng($image_1x[0]);
				}
				if (exif_imagetype($image_1x[0]) == 2) {
				    $im = imagecreatefromjpeg($image_1x[0]);
				}

				$rgb = imagecolorat($im, 0, 0);
				$colors = imagecolorsforindex($im, $rgb);
				$image_background_color = "background-color: rgba(".$colors['red'].",".$colors['green'].",".$colors['blue'].",0.35);";
			?>

			<!-- START SLIDE -->
			<li>

				<?php if ($link != null) { ?>
					<a href="<?php echo $link; ?>">
				<?php } else { ?>
					<div class="slide-container">
				<?php } ?>

						<div class="picture__holder skrollable-hero" data-start="<?php echo $skroll_from;?>" data-top-bottom="<?php echo $skroll_to;?>" style="<?php echo $skroll_start;?>">
							<picture
								class="intrinsic"
								style="<?php echo $intrinsic_padding; ?> <?php echo $image_background_color; ?>">
								<source
									srcset="<?php echo $image_1x[0]; ?> 1x, <?php echo $image_2x; ?> 2x">
								<img
									src="<?php echo $image_1x[0]; ?>"
									alt="<?php echo $image_alt; ?>"
									title="<?php echo $image_title; ?>"
									width="<?php echo $image_1x[1]; ?>"
									height="<?php echo $image_1x[2]; ?>"
									class="intrinsic-item"/>
							</picture>
						</div>

						<div class="slide-copy-container">
							<div class="slide-copy">
								<?php if ($subhead): ?>
									<h5 class="hero"><?php echo $head; ?></h5>
									<h1 class="hero"><?php echo $subhead; ?></h1>
								<?php else: ?>
									<h1 class="fork"><?php echo $head; ?></h1>
								<?php endif; ?>
							</div>
						</div>

				<?php if ($link != null) { ?>
					</a>
				<?php } else { ?>
					</div>
				<?php } ?>

			</li>
			<!-- END SLIDE -->

			<?php } ?>
		</ul>


	<?php if (count($slides) > 1) { ?>
	<div class="slider-nav-container container">
		<div class="slider-nav">
			<div id="slider-nav-prev"></div>
			<div id="slider-nav-next"></div>
		</div>
	</div>
	<?php } ?>

</div>
<?php } ?>

<?php if (count($slides) > 1) { ?>
	<script type="text/javascript">
	  (function( $ ) {
			$( document ).ready(function() {

				/*
				$('.slider').bxSlider({
					auto: true,
					mode: 'fade',
					controls: true,
					pager: false,
					speed: 250,
					pause: 6000,
					useCSS: true,
					easing: 'linear',
					nextSelector: '#slider-nav-next',
					prevSelector: '#slider-nav-prev',
					nextText: '<i class="icon-right-small"></i>',
					prevText: '<i class="icon-left-small"></i>'
				});
				*/


				$('.slider').slick({
		      dots: false,
					prevArrow: '<div id="slider-nav-prev"><a href=""><i class="icon-left-small"></i></a></div>',
					nextArrow: '<div id="slider-nav-next"><a href=""><i class="icon-right-small"></i></a></div>',
		      //prevArrow: '<span class="slick-arrow slick-prev"><span class="screen-reader-only">Previous</span></span>',
		      //nextArrow: '<span class="slick-arrow slick-next"><span class="screen-reader-only">Next</span></span>',
		      appendArrows: $('.slider-nav'),
		      arrows: true,
		      infinite: true,
		      autoplay: true,
		      autoplaySpeed: 6000,
		      speed: 250,
		      fade: true,
		      cssEase: 'linear',
		    });

			});
		})(jQuery);
	</script>
<?php } ?>

<?php } ?>
