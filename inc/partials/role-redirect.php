<?php
global $post;
$accessrole = get_post_meta( $post->ID, '_members_access_role', true );

if ( $accessrole == "administrator" && !is_user_logged_in()) {
    wp_redirect(get_option('general_setting_login_link'));
}
?>
