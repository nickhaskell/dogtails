<?php
	$headline = getMeta('tb_headline');
	$code = getMeta('tb_code');
	$remove_top_padding = getMeta('tb_remove_top_padding');
?>

<?php if (in_array('trustbox_panel', $options)) { ?>
<div class="panel panel-white pad-b-100 <?php if ($remove_top_padding == null || $remove_top_padding == "no") { echo 'pad-t-80'; } ?>">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="color-grey align-center"><?php echo $headline; ?></h2>
			</div>
		</div>
	</div>
	<div class="container trust-box pad-t-50">
		<?php echo $code; ?>
	</div>
</div>
<?php } ?>