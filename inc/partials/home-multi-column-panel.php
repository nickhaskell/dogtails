<?php
  $multi_heading_color = getMeta('home_multicolumn_heading_color');
  $multi_heading  = get_post_meta( $id, '_cmb2_home_multicolumn_panel_heading', true );
  $multi_btn_link = get_post_meta( $id, '_cmb2_home_multicolumn_panel_button_link', true );
  $multi_btn_text = get_post_meta( $id, '_cmb2_home_multicolumn_panel_button_text', true );

  $multi_col_1    = get_post_meta( $id, '_cmb2_home_multicolumn_col_one', true );
  $multi_col_2    = get_post_meta( $id, '_cmb2_home_multicolumn_col_two', true );
  $multi_col_3    = get_post_meta( $id, '_cmb2_home_multicolumn_col_three', true );
  $multi_col_4    = get_post_meta( $id, '_cmb2_home_multicolumn_col_four', true );
?>

<?php if (in_array('home_multicolumn_panel', $options)) { ?>

<div class="panel panel-white multi-column-panel pad-tb-70">

  <?php if ($multi_heading): ?>
    <div class="container multi-column-heading">
      <div class="row">
        <div class="col-sm-12 align-center">
          <h2 class="<?php echo $multi_heading_color; ?>"><?php echo $multi_heading; ?></h2>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <div class="container multi-column-content pad-tb-50">
    <div class="row">

      <?php // Support for only two columns ?>
      <?php if ($multi_col_3 == "" && $multi_col_4 == ""): ?>
        <div class="col-sm-6">
          <? if (count($multi_col_1) > 0 && $multi_col_1 != null) { ?>
            <?php echo apply_filters( 'the_content', $multi_col_1); ?>
          <?php } ?>
        </div>
        <div class="col-sm-6">
          <? if (count($multi_col_2) > 0 && $multi_col_2 != null) { ?>
            <?php echo apply_filters( 'the_content', $multi_col_2); ?>
          <?php } ?>
        </div>
      <?php else: ?>
        <div class="col-sm-6">
            <div class="row">
              <div class="col col-md-6">
                <? if (count($multi_col_1) > 0 && $multi_col_1 != null) { ?>
                  <?php echo apply_filters( 'the_content', $multi_col_1); ?>
                <?php } ?>
              </div>
              <div class="col col-md-6">
                <? if (count($multi_col_2) > 0 && $multi_col_2 != null) { ?>
                  <?php echo apply_filters( 'the_content', $multi_col_2); ?>
                <?php } ?>
              </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
              <div class="col col-md-6">
                <? if (count($multi_col_3) > 0 && $multi_col_3 != null) { ?>
                  <?php echo apply_filters( 'the_content', $multi_col_3); ?>
                <?php } ?>
              </div>
              <div class="col col-md-6">
                <? if (count($multi_col_4) > 0 && $multi_col_4 != null) { ?>
                  <?php echo apply_filters( 'the_content', $multi_col_4); ?>
                <?php } ?>
              </div>
            </div>
        </div>
      <?php endif; ?>

    </div>
  </div>

  <?php if ($multi_btn_link && $multi_btn_text): ?>
  <div class="container multi-column-link">
    <div class="row">
      <div class="col-sm-12 align-center">
        <a href="<?php echo $multi_btn_link; ?>" class="btn large"><?php echo $multi_btn_text; ?></a>
      </div>
    </div>
  </div>
  <?php endif ?>

</div>

<?php } ?>
