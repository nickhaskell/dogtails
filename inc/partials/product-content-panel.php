<?php
	$product_overview    = get_post_meta( $id, '_cmb2_product_overview', true );
	$product_includes    = get_post_meta( $id, '_cmb2_product_includes', true);

  //$product_image       = wp_get_attachment_image_src( get_post_meta( $id, '_cmb2_product_image_id', true ), 'product_thumb' );
	//$product_image_large = get_post_meta( $id, '_cmb2_product_image', true );

  $product_img_id      = get_post_meta( $id, '_cmb2_product_image_id', true );
  $product_img_1x      = wp_get_attachment_image_src( $product_img_id, 'product_thumb_large' );
  $product_img_2x      = wp_get_attachment_image_src( $product_img_id, 'full_size' );

  $product_logo        = get_post_meta( $id, '_cmb2_product_logo', true );

	$product_headline    = get_post_meta( $id, '_cmb2_product_headline', true );
	$product_subheadline = get_post_meta( $id, '_cmb2_product_sub_headline', true );

	$product_model       = get_post_meta( $id, '_cmb2_product_model_number', true );

	$include_product_info = get_post_meta( $id, '_cmb2_include_product_info', true );
	$include_product_models = get_post_meta( $id, '_cmb2_include_product_models', true );

	$product_model_1_name = get_post_meta( $id, '_cmb2_product_model_1_name', true );
	$product_model_1_content = get_post_meta( $id, '_cmb2_product_model_1_content', true );
	$product_model_1_img_id = get_post_meta( $id, '_cmb2_product_model_1_image_id', true );
	$product_model_1_img_1x = wp_get_attachment_image_src( $product_model_1_img_id, 'product_model_thumb' );
	$product_model_1_img_2x = wp_get_attachment_image_src( $product_model_1_img_id, 'full_size' );

	$product_model_2_name = get_post_meta( $id, '_cmb2_product_model_2_name', true );
	$product_model_2_content = get_post_meta( $id, '_cmb2_product_model_2_content', true );
	$product_model_2_img_id = get_post_meta( $id, '_cmb2_product_model_2_image_id', true );
	$product_model_2_img_1x = wp_get_attachment_image_src( $product_model_2_img_id, 'product_model_thumb' );
	$product_model_2_img_2x = wp_get_attachment_image_src( $product_model_2_img_id, 'full_size' );

	$product_model_3_name = get_post_meta( $id, '_cmb2_product_model_3_name', true );
	$product_model_3_content = get_post_meta( $id, '_cmb2_product_model_3_content', true );
	$product_model_3_img_id = get_post_meta( $id, '_cmb2_product_model_3_image_id', true );
	$product_model_3_img_1x = wp_get_attachment_image_src( $product_model_3_img_id, 'product_model_thumb' );
	$product_model_3_img_2x = wp_get_attachment_image_src( $product_model_3_img_id, 'full_size' );
?>

<div class="panel product-content-panel panel-white">
	<div class="container">

		<?php if ($product_headline || $product_subheadline) { ?>
		<div class="product-content-header align-center">
			<div class="row">
			<div class="col-sm-12">
				<div class="headline"><?php echo '<h2>' . $product_headline . '</h2>'; ?></div>
				<h4 class="subhead"><?php echo $product_subheadline; ?></h4>
			</div>
			</div>
		</div>
		<?php } ?>

		<?php if ($product_img_id) { ?>

		<div class="product-image align-center">
			<div class="row">
				<div class="col-sm-12">
					<picture style="<?php echo 'max-width: ' . $product_img_1x[1] . 'px;'; ?>">
						<source srcset="<?php echo $product_img_1x[0] . ' 1x, ' . $product_img_2x[0] . ' 2x'; ?>"/>
						<img src="<?php echo $product_img_1x[0]; ?>" alt="<?php echo $title; ?> Image"/>
					</picture>
				</div>
			</div>
		</div>

		<?php } else {	?>

			<div class="product-padder">
				<div class="row">
					<div class="col-sm-12 pad-t-30">
					</div>
				</div>
			</div>

		<?php } ?>

		<?php if ($include_product_info == 'yes'): ?>
		<div class="row product-main-info">

			<?php if ($product_logo): ?>
			<div class="container product-logo-container">
				<div class="row">
					<div class="col-sm-12">
						<img src="<?php echo $product_logo; ?>" alt="<?php echo $title; ?> Logo" class="product-logo" />
					</div>
				</div>
			</div>
			<?php endif ?>

			<div class="col-sm-6">
				<h3>
					<strong>
						<?php if ($product_model): ?>
							<?php echo $product_model; ?>
						<?php else: ?>
							<?php echo $title; ?>
						<?php endif ?>
					</strong>
				</h3>
			</div>
		</div>

		<hr class="margin-b-30"/>

	  <?php // Start product info ?>
		<div class="row product-sub-info">
			<div class="col-sm-6">
				<?php if ($product_overview) { ?>
				<h4>Overview</h4>
				<?php echo apply_filters( 'the_content', $product_overview); ?>
				<?php } ?>
			</div>
			<div class="col-sm-6">
				<?php if ($product_includes) { ?>
				<h4>Includes</h4>
					<ul class="paragraph-list">
					<?php for ($i = 0; $i < count($product_includes); $i ++) { ?>
						<li><?php echo $product_includes[$i]; ?></li>
					<?php } ?>
					</ul>
				<?php } ?>
			</div>
		</div>
	  <?php // End product info ?>

		<?php endif; ?>


		<?php if ($include_product_models == 'yes'): ?>
		<div class="product-models">

			<div class="row product-models-header">
				<div class="col-sm-12">
					<h3>
						<strong>
							Models
						</strong>
					</h3>
				</div>
			</div>

			<hr class="margin-b-30" />

			<div class="row product-models-content">
				<div class="col-sm-4 product-model">
					<?php if ($product_model_1_img_id) { ?>

						<picture style="<?php echo 'max-width: ' . $product_model_1_img_1x[1] . 'px;'; ?>">
							<source srcset="<?php echo $product_model_1_img_1x[0] . ' 1x, ' . $product_model_1_img_2x[0] . ' 2x'; ?>"/>
							<img src="<?php echo $product_model_1_img_1x[0]; ?>" alt="<?php echo $product_model_1_name; ?> Image"/>
						</picture>

					<?php } else { ?>

						<img src="http://placehold.it/270x193/cccccc/ffffff/"/>

					<?php } ?>

					<p>
						<strong><?php echo $product_model_1_name; ?></strong>
					</p>
					<?php echo apply_filters( 'the_content', $product_model_1_content); ?>

				</div>
				<div class="col-sm-4 product-model">
					<?php if ($product_model_2_img_id) { ?>

						<picture style="<?php echo 'max-width: ' . $product_model_2_img_1x[1] . 'px;'; ?>">
							<source srcset="<?php echo $product_model_2_img_1x[0] . ' 1x, ' . $product_model_2_img_2x[0] . ' 2x'; ?>"/>
							<img src="<?php echo $product_model_2_img_1x[0]; ?>" alt="<?php echo $product_model_2_name; ?> Image"/>
						</picture>

					<?php } else { ?>

						<img src="http://placehold.it/270x193/cccccc/ffffff/"/>

					<?php } ?>

					<p>
						<strong><?php echo $product_model_2_name; ?></strong>
					</p>
					<?php echo apply_filters( 'the_content', $product_model_2_content); ?>

				</div>
				<div class="col-sm-4 product-model">
					<?php if ($product_model_3_img_id) { ?>

						<picture style="<?php echo 'max-width: ' . $product_model_3_img_1x[1] . 'px;'; ?>">
							<source srcset="<?php echo $product_model_3_img_1x[0] . ' 1x, ' . $product_model_3_img_2x[0] . ' 2x'; ?>"/>
							<img src="<?php echo $product_model_3_img_1x[0]; ?>" alt="<?php echo $product_model_3_name; ?> Image"/>
						</picture>

					<?php } else { ?>

						<img src="http://placehold.it/270x193/cccccc/ffffff/"/>

					<?php } ?>

					<p>
						<strong><?php echo $product_model_3_name; ?></strong>
					</p>
					<?php echo apply_filters( 'the_content', $product_model_3_content); ?>

				</div>
			</div>

		</div>

	<?php endif; ?>

	</div>
</div>
