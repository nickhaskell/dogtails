<?php
	$headline    = getMeta('find_a_dealer_headline');
	$description = getMeta('find_a_dealer_description');

	class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{

	    // don't output children opening tag (`<ul>`)
	    public function start_lvl(&$output, $depth) {}

	    // don't output children closing tag
	    public function end_lvl(&$output, $depth) {}

	    public function start_el(&$output, $item, $depth, $args){

	      // add spacing to the title based on the current depth
	      $item->title = str_repeat("&nbsp;", $depth * 4) . $item->title;

	      // call the prototype and replace the <li> tag
	      // from the generated markup...
	      parent::start_el($output, $item, $depth, $args);

	      $output = str_replace('<li', '<option value="' . $item->url . '"', $output);
	    }

	    // replace closing </li> with the closing option tag
	    public function end_el($output, $item, $depth){
	      $output .= "</option>\n";
	    }
	}

?>

<?php if (in_array('find_a_dealer', $options)) { ?>
<div class="panel panel-green-dark find-a-dealer dark">
  <div class="container find-a-dealer-form pad-tb-50">

		<div class="row">
			<div class="content-container col-sm-12 align-center">
				<?php if ($headline): ?>
					<h3 class="headline h3--large"><?php echo $headline; ?></h3>
				<?php endif; ?>
				<?php if ($description): ?>
					<p class="description"><?php echo $description; ?></p>
				<?php endif; ?>
			</div>
		</div>

		<?php // FORM START ?>
		<div class="find-a-dealer-form-panel">
			<div class="row form-zip-search">
				<div class="col-sm-12 align-center pad-b-40">
					<?php include 'search-zip-panel.php'; ?>
				</div>
			</div>
			<div class="row form">
				<div class="col-sm-6 col-l">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'domestic',
								'walker'         => new Walker_Nav_Menu_Dropdown(),
								'items_wrap'     => '<select class="styled-select ajax-select-form" data-links="true" data-size="8" data-placeholder-option="true">%3$s</select>',
					)); ?>
				</div>
				<div class="col-sm-6 col-r">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'international',
								'walker'         => new Walker_Nav_Menu_Dropdown(),
								'items_wrap'     => '<select class="styled-select ajax-select-form" data-links="true" data-size="8" data-placeholder-option="true">%3$s</select>',
					)); ?>
				</div>
			</div>
		</div>
		<?php // FORM END ?>

  </div>
</div>
<?php } ?>

<script type="text/javascript">
	(function( $ ) {

		var parent = $('.panel.find-a-dealer');
		var hidden = parent.find('.find-a-dealer-form-panel');
		var button = parent.find('.find-a-dealer-show-hide');

		// Add button click handler and interactions
		button.on('click', function(e) {
			e.preventDefault();
			hidden.toggle();
			button.remove();
		});

	})(jQuery);
</script>
