<?php
$image_1x        = wp_get_attachment_image_src( $image_id, $image_size . '@1x' );
$image_2x        = wp_get_attachment_image_src( $image_id, $image_size . '@2x' );
$image_data      = get_post_meta($image_id);
$image_width     = $image_1x[1];
$image_height    = $image_1x[2];
$image_alt       = array_key_exists('_wp_attachment_image_alt', $image_data) ? $image_data['_wp_attachment_image_alt'][0] : "";
$image_title     = get_the_title($image_id);
?>

<picture
  class="<?php echo $image_class; ?>"
  style="<?php echo 'max-width: ' . $image_width . 'px;'; ?>"
  >
  <source
    srcset="<?php echo $image_1x[0]; ?> 1x,
            <?php echo $image_2x[0]; ?> 2x">
  <img
    src="<?php echo $image_1x[0]; ?>"
    alt="<?php echo $image_alt; ?>"
    title="<?php echo $image_title; ?>"
    width="<?php echo $image_width; ?>"
    height="<?php echo $image_height; ?>"/>
</picture>
