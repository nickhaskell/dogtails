<?php
$headline    = get_post_meta( $id, '_cmb2_smartfence_ribbon_headline', true );
$subheadline = get_post_meta( $id, '_cmb2_smartfence_ribbon_subheadline', true );
$button_text = get_post_meta( $id, '_cmb2_smartfence_ribbon_button_text', true );
$button_url  = get_post_meta( $id, '_cmb2_smartfence_ribbon_button_url', true );
?>

<?php if (in_array('smartfence_ribbon', $options)) { ?>
<div class="smartfence-ribbon panel panel-green dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="image-container">
          <div class="smartfence-image"></div>
        </div>
        <?php if ($headline): ?>
          <div class="content-container">
              <h2 class="headline color-white"><strong><?php echo $headline; ?></strong></h2>
            <?php if ($subheadline): ?>
              <h3 class="subheadline"><?php echo $subheadline; ?></h3>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if ($button_url): ?>
          <div class="button-container">
            <a href="<?php echo $button_url; ?>" class="btn deep-green"><?php echo $button_text; ?></a>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
