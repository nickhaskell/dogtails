<?php
  $headline    = getMeta('dealer_difference_headline');
  $description = getMeta('dealer_difference_description');
  $image_id    = getMeta('dealer_difference_image_id');
  $image_1x    = wp_get_attachment_image_src( $image_id, 'customer_story@1x' );
  $image_2x    = wp_get_attachment_image_src( $image_id, 'customer_story@2x' );
  $image_m_1x  = wp_get_attachment_image_src( $image_id, 'customer_story_m@1x' );
  $image_m_2x  = wp_get_attachment_image_src( $image_id, 'customer_story_m@2x' );

  $image_alt   = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
  $image_title = get_the_title($image_id);

  if ($image_id) {
    if (exif_imagetype($image_1x[0]) == 3) {
        $im = imagecreatefrompng($image_1x[0]);
    }
    if (exif_imagetype($image_1x[0]) == 2) {
        $im = imagecreatefromjpeg($image_1x[0]);
    }
    $rgb = imagecolorat($im, 0, 0);
    $colors = imagecolorsforindex($im, $rgb);
    $image_background_color = "background-color: rgba(".$colors['red'].",".$colors['green'].",".$colors['blue'].",0.35);";
  }
?>

<?php if (in_array('dealer_difference', $options)) { ?>
<div class="customer-story customer-story--dealer-difference">
  <div class="content-container">
    <?php if ($headline): ?>
      <h2 class="headline"><?php echo $headline; ?></h2>
    <?php endif; ?>
    <?php if ($description): ?>
      <p class="description"><?php echo $description; ?></p>
    <?php endif; ?>
    <div class="dealer-search">
      <form method="get" class="zipsearch combined-input" action="//www.dogwatch.com/">
        <div class="input-holder">
          <input class="zipcode combined-input-input" type="tel" name="s" placeholder="Zip Code" value = "" pattern="\d{5,5}(-\d{4,4})?" maxlength="5">
          <input type="hidden" name="post_type" value="dealer" />
          <button type="submit" class="submit combined-input-button" name="submit"><span class="icon-search-2"></span></button>
        </div>
      </form>
      <p class="outside-us-link">
        or <a href="<?php echo get_option('general_setting_dealer'); ?>" class="state-link">search by state/outside U.S.</a>
      </p>
    </div>
  </div>
  <div class="image-container">
    <?php if ($image_id): ?>
    <picture style="<?php echo $image_background_color; ?>">
      <source
        srcset="<?php echo $image_1x[0]; ?> 1x,
                <?php echo $image_2x[0]; ?> 2x"
        media="(min-width:1200px)"/>
      <source
        srcset="<?php echo $image_m_1x[0]; ?> 1x,
                <?php echo $image_m_2x[0]; ?> 2x"
        media="(min-width:960px)"/>
        <source
          srcset="<?php echo $image_1x[0]; ?> 1x,
                  <?php echo $image_2x[0]; ?> 2x"
          media="(max-width:959px)"/>
      <img
        src="<?php echo $image_1x[0]; ?>"
        alt="<?php echo $image_alt; ?>"
        title="<?php echo $image_title; ?>"
        width="<?php echo $image_1x[1]; ?>"
        height="<?php echo $image_1x[2]; ?>">
    </picture>
    <?php endif; ?>
  </div>
</div>
<?php } ?>
