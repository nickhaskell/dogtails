<?php
  $showhides_l = get_post_meta( $id, '_cmb2_left_column_showhides', true );
  $showhides_r = get_post_meta( $id, '_cmb2_right_column_showhides', true );
?>

<?php if (in_array('show_hide_panel', $options)) { ?>

<div class="container showhides-panel">
  <div class="row">
    <div class="col-l col-sm-6">


      <? if (count($showhides_l) > 0 && $showhides_l != null) { ?>
      <ul class="show-hide show-hide-left">
      	<?php for ($i = 0; $i < count($showhides_l); $i ++) { ?>

      		<?php
      			$shl_heading = $showhides_l[$i]['heading'];
      			$shl_content = $showhides_l[$i]['content'];
      		?>
      		<li class="show-hide-container">
      			<div class="show-hide-heading">
      				<span class="subhead">
      					<?php echo $shl_heading; ?>
      				</span>
      			</div>
      			<div class="show-hide-content" style="display: none;">
      				<div class="show-hide-actual-content">
      					<?php echo apply_filters( 'the_content', $shl_content); ?>
      				</div>
      			</div>
      		</li>

      	<?php } ?>
      </ul>
      <?php } ?>


    </div>
    <div class="col-r col-sm-6">


      <? if (count($showhides_r) > 0 && $showhides_r != null) { ?>
      <ul class="show-hide show-hide-right">
      	<?php for ($i = 0; $i < count($showhides_r); $i ++) { ?>

      		<?php
      			$shr_heading = $showhides_r[$i]['heading'];
      			$shr_content = $showhides_r[$i]['content'];
      		?>
      		<li class="show-hide-container">
      			<div class="show-hide-heading">
      				<span class="subhead">
      					<?php echo $shr_heading; ?>
      				</span>
      			</div>
      			<div class="show-hide-content" style="display: none;">
      				<div class="show-hide-actual-content">
      					<?php echo apply_filters( 'the_content', $shr_content); ?>
      				</div>
      			</div>
      		</li>

      	<?php } ?>
      </ul>
      <?php } ?>


    </div>
  </div>
</div>

<?php } ?>
