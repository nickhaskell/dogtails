<?php
  $image_id       = get_post_meta( $id, '_cmb2_video_panel_image_id', true );
  $image_1x       = wp_get_attachment_image_src( $image_id, 'video_placeholder@1x' );
  $image_2x       = wp_get_attachment_image_src( $image_id, 'video_placeholder@2x' );
  $image_alt      = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
  $image_title    = get_the_title($image_id);

  $videos_l         = get_post_meta( $id, '_cmb2_left_videos', true );
	$videos_r         = get_post_meta( $id, '_cmb2_right_videos', true );
	$vp_first_on_page = get_post_meta( $id, '_cmb2_video_panel_first_on_page', true );
	$vp_single_video  = get_post_meta( $id, '_cmb2_video_panel_single_video', true );

  $vp_panel_class = "";

  $vp_single_vid_class = "";

  if ($vp_single_video == "yes") {
    $vp_single_vid_class = " hidden";
    $vp_panel_class .= " single-vid";
  }

?>

<?php if (in_array('video_panel', $options)) { ?>

<div class="panel panel-white videos-panel <?php echo $vp_panel_class; ?>">
  <div class="container <?php echo $vp_single_vid_class; ?>">
    <div class="row">
      <div class="col-l col-sm-6">


        <? if (count($videos_l) > 0 && $videos_l != null) { ?>
        <?php $has_videos = true; ?>
        <div class="videos videos-left">
        	<?php for ($i = 0; $i < count($videos_l); $i ++) { ?>

        		<?php
        			$vl_title = $videos_l[$i]['title'];
        			$vl_embed = $videos_l[$i]['embed'];
        		?>

        		<div class="video-container">
        			<div class="video-link" data-video-embed='<?php echo addslashes($vl_embed); ?>'>
        				<span class="subhead">
        					<?php echo $vl_title ?>
        				</span>
        			</div>
        		</div>

        	<?php } ?>
        </div>
        <?php } ?>


      </div>
      <div class="col-r col-sm-6">


        <? if (count($videos_r) > 0 && $videos_r != null) { ?>
        <?php $has_videos = true; ?>
        <div class="videos videos-right">
        	<?php for ($i = 0; $i < count($videos_r); $i ++) { ?>

        		<?php
        			$vr_title = $videos_r[$i]['title'];
        			$vr_embed = $videos_r[$i]['embed'];
        		?>

        		<div class="video-container">
        			<div class="video-link" data-video-embed='<?php echo addslashes($vr_embed); ?>'>
        				<span class="subhead">
        					<?php echo $vr_title; ?>
        				</span>
        			</div>
        		</div>

        	<?php } ?>
        </div>
        <?php } ?>


      </div>
    </div>
  </div>

  <?php if ((count($videos_l) > 0 && $videos_l != null) || (count($videos_r) > 0 && $videos_r != null)): ?>
  	<div id="player" class="container video-player">
      <div class="row">
        <div class="col-sm-12">
          <div class="video-player-container">
            <?php if ($vp_single_video != "yes"): ?>
            <div class="play-overlay">
              <div class="play-button"></div>
              <div class="play-icon">
                <?php echo file_get_contents(get_template_directory_uri() . "/assets/images/svg/play.svg"); ?>
              </div>
              <div class="video-placeholder">
                <picture>
                  <source
                    srcset="<?php echo $image_1x[0]; ?> 1x,
                            <?php echo $image_2x[0]; ?> 2x">
                  <img
                    src="<?php echo $image_1x[0]; ?>"
                    alt="<?php echo $image_alt; ?>"
                    title="<?php echo $image_title; ?>"
                    width="<?php echo $image_1x[1]; ?>"
                    height="<?php echo $image_1x[2]; ?>">
                </picture>
              </div>
            </div>
            <iframe id="myIframe" width="660" height="331"></iframe>
          <?php else: ?>
            <?php // do this as an oembed ?>
            <?php echo wp_oembed_get($videos_l[0]['embed']); ?>
          <?php endif;?>
          </div>
        </div>
  		</div>
  	</div>

    <?php if ($vp_single_video != "yes"): ?>
    <script type="text/javascript">
      (function( $ ) {

        var youtube_api      = 'https://www.googleapis.com/youtube/v3/videos';
        var youtube_api_key  = 'AIzaSyA-wIk7j1BKK4wMsKLzTxQZ96CQA2RCyw0';
        var youtube_api_part = "part=snippet";
        var video_panel      = $('.videos-panel');
        var player           = video_panel.find('#player');
        var video_iframe     = video_panel.find('#myIframe');
        var video_links      = video_panel.find('.videos');
        var video_placeholder= video_panel.find('.play-overlay');
        var video_play_button= video_placeholder.find('.play-button');
        var first_link       = video_links.find('.video-link').first();

        setInitialState();
        addClickHandlers();

        function setInitialState () {
          first_link.toggleClass('active');
        }

        function addClickHandlers () {
          video_play_button.on('click', function() {
            var first_embed      = getVideoID(first_link);
            var first_video_url  = getVideoURL(first_embed);
            getVideo(first_video_url);
            scrollUp();
          });
          video_links.find('.video-link').on('click', function() {
            var embed            = getVideoID($(this));
            var video_url        = getVideoURL(embed)
            clearActiveLinks();
            $(this).toggleClass('active');
            getVideo(video_url);
            scrollUp();
          });
        }

        function clearActiveLinks () {
          video_links.find('.video-link').each(function() {
            $(this).removeClass('active');
          });
        }

        function scrollUp () {
          $('html, body').animate({
            scrollTop: player.offset().top - 90
          }, SCROLL_SPEED);
        }

        function removePlaceholder () {
          setTimeout(function () {
            video_placeholder.remove();
          }, 500);
        }

        function getVideoID (elem) {
          var video_id = elem.attr('data-video-embed').replace(/\\"/g, '"');
          return video_id;
        }

        function getVideoURL(embed_string) {
          var embed_slash_array  = embed_string.split('/');
          var embed_quote_array  = embed_slash_array[3].split('"');
          var embed_equals_array = embed_quote_array[0].split('=');
          var embed_id           = embed_equals_array[1];

          if (embed_id.includes("?list")) {
              temp_array = embed_id.split("?");
              embed_id = temp_array[0];
          }

          var video_url         = youtube_api + "?" + "key=" + youtube_api_key + "&" + "id=" + embed_id + "&" + youtube_api_part;
          return video_url;
        }

        function getVideo(video_url, option) {
          var extension = "?wmode=transparent&rel=0&autoplay=1";
          $.get(video_url, function(response) {
            var url = "https://www.youtube.com/embed/" + response.items[0].id + extension;
            video_iframe.attr('src', url);
          });
          if (!player.hasClass('active')) {
            player.toggleClass('active');
          }
          if (video_placeholder.length) {
            removePlaceholder();
          }
        }
      })(jQuery);
    </script>
    <?php endif; ?>
  <?php endif ?>

</div>
<?php } ?>
