<?php
  // This is the partial for the actual page of the dealer

  $name       = get_post_meta( $id, '_cmb2_dealer_name', true );
  $location   = get_post_meta( $id, '_cmb2_dealer_cities_and_towns', true );
  $owner      = get_post_meta( $id, '_cmb2_dealer_owner', true );
  $phone      = get_post_meta( $id, '_cmb2_dealer_phone', true );
  $secondary  = get_post_meta( $id, '_cmb2_dealer_secondary_phone', true );
  $toll_free  = get_post_meta( $id, '_cmb2_dealer_toll_free', true );
  $email      = get_post_meta( $id, '_cmb2_dealer_email', true );
  $link       = get_post_meta( $id, '_cmb2_dealer_link', true );

  // NEW
  $subheadline        = get_post_meta( $id, '_cmb2_dealer_subheadline', true );
  $about_us           = get_post_meta( $id, '_cmb2_dealer_about_us', true );

  $about_us_image_id  = get_post_meta( $id, '_cmb2_dealer_about_us_image_id', true );
  $about_us_image_2x  = wp_get_attachment_image_src( $about_us_image_id, 'full_size' );


  $location_header    = get_post_meta( $id, '_cmb2_dealer_serive_areas_header', true );
  $location_image_id  = get_post_meta( $id, '_cmb2_dealer_service_areas_image_id', true );

  $location_image_2x  = wp_get_attachment_image_src( $location_image_id, 'full_size' );

  $trust_icons        = get_post_meta( $id, '_cmb2_dealer_trust_icons', true );
  $testimonials       = get_post_meta( $id, '_cmb2_dealer_testimonials', true );

  $review_widget_code = get_post_meta( $id, '_cmb2_review_widget_code', true );
?>

<div class="container dealers">

  <?php if ($subheadline): ?>
    <div class="row subheadline">
      <div class="col-sm-12 align-center">
        <p class="body-intro"><?php echo $subheadline; ?></p>
      </div>
    </div>
  <?php endif; ?>
  <?php if ($subheadline && $link): ?>
    <div class="headline-item-divider"></div>
  <?php endif; ?>
  <?php if ($link): ?>
  <div class="subheadline-button row">
    <div class="col-sm-12 align-center">
      <a href=" <?php echo $link ?> " target="_blank" class="btn">Visit Our Website</a>
    </div>
  </div>
  <?php endif; ?>

    <?php if ($review_widget_code): ?>
    <div class="panel panel-white trust-box pad-t-50">
      <div class="full-width">
        <?php echo $review_widget_code; ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="row dealers-info">
    <div class="col-sm-6 dealers-info-left">
      <?php if ($about_us_image_id): ?>
        <div class="about-us-image align-center">
          <?php if ($about_us_image_2x[1] >= 600): ?>
            <picture>
              <source
                srcset="
                  <?php echo wp_get_attachment_image_src( $about_us_image_id, 'dealer_profile@1x' )[0]; ?> 1x,
                  <?php echo wp_get_attachment_image_src( $about_us_image_id, 'dealer_profile@2x' )[0]; ?> 2x">
              <img src="<?php echo wp_get_attachment_image_src( $about_us_image_id, 'dealer_profile@1x' )[0]; ?>">
            </picture>
          <?php else: ?>
            <img src="<?php echo $about_us_image_2x[0]; ?>" alt="<?php echo $owner; ?> <?php echo $name; ?> Picture"/>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if ($owner): ?>
        <h4><?php echo $owner; ?></h4>
      <?php endif; ?>
      <?php if ($email): ?>
        <p class="item dealer-info__email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
      <?php endif; ?>
      <?php if ($link): ?>
        <p class="item dealer-info__url"><a href="<?php echo $link; ?>"><?php echo $link; ?></a></p>
      <?php endif; ?>
      <?php if ($phone): ?>
        <p class="item dealer-info__main-phone"><?php echo $phone; ?></p>
      <?php endif ?>
      <?php if ($secondary): ?>
        <p class="item dealer-info__secondary-phone"><?php echo $secondary; ?></p>
      <?php endif ?>
      <?php if ($toll_free): ?>
        <p class="itemdealer-info__toll-free">Toll-free: <?php echo $toll_free; ?></p>
      <?php endif ?>
      <?php if ($about_us): ?>
        <p class="about-us-label">
          <strong>About us: </strong>
        </p>
        <div class="about-us">
          <?php echo apply_filters('the_content', $about_us); ?>
        </div>
      <?php endif; ?>

      <?php if ($testimonials != null && $testimonials[0]['testimonial'] != "" && $testimonials[0]['testimonial'] != null): ?>
        <section class="testimonials">
          <h4>From our customers:</h4>
          <ul>
            <?php foreach ($testimonials as $key => $testimonial): ?>
              <li>
                <p>
                  <?php if ($testimonial['testimonial']): ?>
                    <em><?php echo $testimonial['testimonial']; ?></em>
                  <?php endif; ?>
                  <?php if ($testimonial['name']): ?>
                    <br/>
                    <strong>
                      <?php echo $testimonial['name']; ?><?php echo ($testimonial['location'] != "") ? ", " . $testimonial['location'] : ""; ?>
                    </strong>
                  <?php endif; ?>
                </p>
              </li>
            <?php endforeach; ?>
          </ul>
        </section>
      <?php endif; ?>

    </div>
    <div class="col-sm-6 dealers-info-right">
      <?php if ($location_image_id): ?>
        <div class="location-image align-center">
          <?php if ($location_image_2x[1] >= 600): ?>
            <picture>
              <source
                srcset="
                  <?php echo wp_get_attachment_image_src( $location_image_id, 'dealer_profile@1x' )[0]; ?> 1x,
                  <?php echo wp_get_attachment_image_src( $location_image_id, 'dealer_profile@2x' )[0]; ?> 2x">
              <img src="<?php echo wp_get_attachment_image_src( $location_image_id, 'dealer_profile@1x' )[0]; ?>">
            </picture>
          <?php else: ?>
            <img src="<?php echo $location_image_2x[0]; ?>" alt="<?php echo $name; ?> Service Area"/>
          <?php endif; ?>

        </div>
      <?php endif; ?>
      <?php if ($location): ?>
        <h4>
          <?php if ($location_header): ?>
            <?php echo $location_header; ?>:
          <?php else: ?>
            Areas we serve:
          <?php endif; ?>
        </h4>
        <p>
          <?php if ($link): ?>
            <a href="<?php echo $link; ?>" target="_blank">
          <?php endif; ?>
          <?php echo $location; ?>
          <?php if ($link): ?>
            </a>
          <?php endif; ?>
        </p>
      <?php endif; ?>

      <?php if ($trust_icons): ?>
      <div class="trust-icons">
        <?php foreach ($trust_icons as $key => $icon): ?>
          <?php $image = wp_get_attachment_image_src( $icon['image_id'], 'full_size' ); ?>
          <div class="icon <?php if ($key + 1 == count($trust_icons)) { echo 'last'; } ?>">
            <?php if ($image[0]): ?>

              <?php if ($icon['url']): ?><a href="<?php echo $icon['url']; ?>" target="_blank"><?php endif; ?>
              <img src="<?php echo $image[0];?>" alt="<?php echo $icon['name']; ?>" />
              <?php if ($icon['url']): ?></a><?php endif; ?>

            <?php else: ?>

              <?php echo $icon['html']; ?>

            <?php endif; ?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>


    </div>
  </div>

  <div class="legal row pad-b-70">
    <div class="col-sm-6">
      <p>
        <small>DogWatch® is not Invisible®</small>
      </p>
      <p>
        <small>DogWatch “hidden dog fences” are often mistakenly referred to generically as “invisible fences” or “invisible dog fences.” Invisible Fence® and Invisible Fencing® are Brand names, products and registered trademarks of Radio Systems, Inc.</small>
      </p>
    </div>
  </div>
</div>
