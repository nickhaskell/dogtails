<?php
  $col_count             = get_post_meta( $post->ID, '_cmb2_main_content_columns', true );
  $two_col_left_content  = get_post_meta( $post->ID, '_cmb2_two_col_left_content', true );
  $two_col_right_content = get_post_meta( $post->ID, '_cmb2_two_col_right_content', true );
  $one_col_content       = get_post_meta( $post->ID, '_cmb2_one_col_content', true );
  $hide_sidebar          = get_post_meta( $post->ID, '_cmb2_hide_store_sidebar', true );

  $main_column_width_class = "col-sm-9 left-col";
  if ($hide_sidebar == "on") {
    $main_column_width_class = "col-sm-12 full-col";
  }
?>

<?php if ($two_col_left_content != "" || $two_col_right_content != "" || $one_col_content != ""): ?>
<div class="panel panel-white">
  <div class="container">
    <div class="row">

      <?php if ( function_exists('dynamic_sidebar') || dynamic_sidebar('store-widgets-right') ) : ?>
        <?php if ($col_count == "one"): ?>

          <div class="<?php echo $main_column_width_class; ?> pad-b-30">
            <?php echo apply_filters( 'the_content', $one_col_content); ?>
          </div>

        <?php elseif ($col_count == "one-fw"): ?>

          <div class="<?php echo $main_column_width_class; ?> pad-b-70">
            <?php echo apply_filters( 'the_content', $one_col_content); ?>
          </div>

        <?php else: ?>

          <div class="col-sm-5 col-l">
            <?php echo apply_filters( 'the_content', $two_col_left_content); ?>
          </div>
          <div class="col-sm-4 col-r">
            <?php echo apply_filters( 'the_content', $two_col_right_content); ?>
          </div>


        <?php endif; ?>


      <?php else : ?>


        <?php if ($col_count == "one"): ?>

          <div class="col-sm-6 center-col pad-b-30">
            <?php echo apply_filters( 'the_content', $one_col_content); ?>
          </div>

        <?php elseif ($col_count == "one-fw"): ?>

          <div class="col-sm-12 pad-b-70">
            <?php echo apply_filters( 'the_content', $one_col_content); ?>
          </div>

        <?php else: ?>

          <div class="col-sm-6 col-l">
            <?php echo apply_filters( 'the_content', $two_col_left_content); ?>
          </div>
          <div class="col-sm-6 col-r">
            <?php echo apply_filters( 'the_content', $two_col_right_content); ?>
          </div>

        <?php endif; ?>

    <?php endif; ?>

    <?php if ($hide_sidebar != "on"): ?>
    <div class="col-sm-3">
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('store-widgets-right') ) : ?>
      <?php the_widget('store-widgets-right'); ?>
    <?php endif; ?>
    </div>
    <?php endif; ?>

  </div>
</div>
<?php endif; ?>
