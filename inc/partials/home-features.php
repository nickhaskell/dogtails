<?php
  $features = get_post_meta( $id, '_cmb2_home_featured_items', true );
?>

<?php if (in_array('home_features_panel', $options)) { ?>

<div class="panel panel-tan home-features pad-tb-50">
  <div class="container">
    <ul class="features row unstyled group">
      <?php for ($i = 0; $i < count($features); $i ++) { ?>

        <?php
          $feature_id        = $features[$i]['link'];
          $feature_page      = get_post($feature_id);
          $feature_url       = get_permalink($feature_id);

          $feature_link_text = $features[$i]['link_text'];
          $feature_img_id    = $features[$i]['image_id'];
          $feature_img_1x    = wp_get_attachment_image_src( $feature_img_id, 'home_feature_new' );
          $feature_img_2x    = wp_get_attachment_image_src( $feature_img_id, 'full_size' );
          $feature_img_s_1x  = wp_get_attachment_image_src( $feature_img_id, 'home_feature_new_s@1x' );
          $feature_img_s_2x  = wp_get_attachment_image_src( $feature_img_id, 'home_feature_new_s@2x' );

          $image_alt         = get_post_meta($feature_img_id)['_wp_attachment_image_alt'][0];
          $image_title       = get_the_title($feature_img_id);
        ?>

      <li class="feature col-sm-4">
        <a href="<?php echo $feature_url; ?>">
          <div class="feature-image">
            <?php if ($feature_img_id): ?>
              <picture>
                <source
                  srcset="<?php echo $feature_img_s_1x[0]; ?> 1x,
                  <?php echo $feature_img_s_2x[0]; ?> 2x"
                  media="(max-width:598px)"/>
                <source
                  srcset="<?php echo $feature_img_1x[0]; ?> 1x,
                  <?php echo $feature_img_2x[0]; ?> 2x"
                  media="(min-width:599px)"/>
                <img
                  src="<?php echo $feature_img_1x[0]; ?>"
                  alt="<?php echo $image_alt; ?>"
                  title="<?php echo $image_title; ?>"
                  width="<?php echo $feature_img_1x[1]; ?>"
                  height="<?php echo $feature_img_1x[2]; ?>" />
              </picture>
            <?php else: ?>
              <img src="http://placehold.it/270x260/cccccc/ffffff/" alt="<?php echo $feature_link_text; ?>"/>
            <?php endif; ?>
          </div>
          <div class="feature-link">
              <h5><?php echo $feature_link_text; ?></h5>
          </div>
        </a>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>

<?php } ?>
