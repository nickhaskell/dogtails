<?php
  $featured_img_id      = get_post_meta( $post->ID, '_cmb2_featured_image_id', true );
  //$featured_img_1x      = wp_get_attachment_image_src( $featured_img_id, 'hero_product' );
  $featured_img_1x      = wp_get_attachment_image_src( $featured_img_id, 'feature_panel' );
  $featured_img_2x      = wp_get_attachment_image_src( $featured_img_id, 'full_size' );
  $featured_img_s_1x    = wp_get_attachment_image_src( $featured_img_id, 'feature_panel_s@1x' );
  $featured_img_s_2x    = wp_get_attachment_image_src( $featured_img_id, 'feature_panel_s@2x' );
  $image_data           = get_post_meta($featured_img_id);


  $featured_headline     = get_post_meta( $post->ID, '_cmb2_featured_headline', true );

  $featured_column_left = get_post_meta( $post->ID, '_cmb2_featured_column_left', true );
  $featured_column_right = get_post_meta( $post->ID, '_cmb2_featured_column_right', true );

  $fc_class = "";

  if ($featured_column_left == "" && $featured_column_right == "") {
    $fc_class .= "no-content";
  }
?>

<?php if (in_array('featured_panel', $options)) { ?>

<div class="panel panel-green featured-panel dark <?php echo $fc_class; ?>">
  <div class="featured-image">
    <picture>
      <source
        srcset="
          <?php echo $featured_img_s_1x[0]; ?> 1x,
          <?php echo $featured_img_s_2x[0]; ?> 2x"
        media="(max-width:480px)"/>
			<source
				srcset="
          <?php echo $featured_img_1x[0]; ?> 1x,
          <?php echo $featured_img_2x[0]; ?> 2x"
        media="(min-width:481px)">
			<img
        src="<?php echo $featured_img_1x[0]; ?>"
        alt="<?php echo $image_data['_wp_attachment_image_alt'][0];?>"
        title="<?php echo get_the_title($featured_img_id); ?>"
        width="<?php echo $featured_img_1x[1]; ?>"
        height="<?php echo $featured_img_1x[2]; ?>">
		</picture>
    <?php if ($featured_headline != ""): ?>
    <div class="featured-headline-container">
      <div class="container">
        <h6 class="romance featured-headline color-white">
          <?php echo $featured_headline; ?>
        </h6>
      </div>
    </div>
    <?php endif; ?>
  </div>
  <?php if ($featured_column_left != ""): ?>
  <div class="featured-columns-wrapper">
    <div class="featured-columns container pad-tb-50">
      <div class="row">
        <div class="featured-column col-sm-6">
          <?php echo apply_filters( 'the_content', $featured_column_left); ?>
        </div>
        <div class="featured-column col-sm-6">
          <?php echo apply_filters( 'the_content', $featured_column_right); ?>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
</div>

<?php } ?>
