<?php
  $tab1_title = get_post_meta( $id, '_cmb2_tab_one_title', true );
	$tab1       = get_post_meta( $id, '_cmb2_info_table_tab_one', true );
  $tab1_half  = count($tab1) / 2;
  $tab1_break = ceil($tab1_half);

  $tab2_title = get_post_meta( $id, '_cmb2_tab_two_title', true );
	$tab2       = get_post_meta( $id, '_cmb2_info_table_tab_two', true );
  $tab2_half  = count($tab2) / 2;
  $tab2_break = ceil($tab2_half);

  $tab3_title = get_post_meta( $id, '_cmb2_tab_three_title', true );
	$tab3       = get_post_meta( $id, '_cmb2_info_table_tab_three', true );
  $tab3_half  = count($tab3) / 2;
  $tab3_break = ceil($tab3_half);
?>

<?php if (in_array('info_table_new', $options)) { ?>

<div class="panel panel-white info-table-new-panel">

  <div class="info-table-new container">

    <div class="row info-table-button">
      <div class="col-sm-12 align-center">
        <a href="#" class="btn grey system-features-toggle" data-open-text="Show System Features" data-close-text="Hide System Features">Show System Features</a>
      </div>
    </div>

  	<div class="row info-tabs">
      <div class="col-sm-12">

        <?php if ($tab1_title): ?>
          <a href="#" class="info-tab active is-active" data-name="info-list-1">
            <?php echo $tab1_title; ?>
          </a>
        <?php endif; ?>

        <?php if ($tab2_title): ?>
          <a href="#" class="info-tab" data-name="info-list-2">
            <?php echo $tab2_title; ?>
          </a>
        <?php endif; ?>

        <?php if ($tab3_title): ?>
          <a href="#" class="info-tab" data-name="info-list-3">
            <?php echo $tab3_title; ?>
          </a>
        <?php endif; ?>

      </div>
  	</div>

    <div class="info-lists">

      <?php if (count($tab1) > 0): ?>
    	<div class="info-list is-active" data-name="info-list-1">
    		<?php for ($i = 0; $i < count($tab1); $i ++) { ?>

          <?php
            $title   = $tab1[$i]['title'];
            $excerpt = $tab1[$i]['description_excerpt'];
            $full    = $tab1[$i]['description_full'];
            $tab_num = '1';
          ?>

          <?php if ($i == 0): ?>
            <div class="info-list-column info-list-left">
          <?php endif; ?>

          <?php if ($i == ($tab1_break)): ?>
            </div>
            <div class="info-list-column info-list-right">
          <?php endif; ?>

          <?php include('info-table-info-container.php'); ?>

          <?php if ($i == (count($tab1) - 1)): ?>
            </div>
          <?php endif; ?>

    		<?php } ?>
      </div>
    <?php endif; ?>

      <?php if (count($tab2) > 0): ?>
    	<div class="info-list" data-name="info-list-2">
    		<?php for ($i = 0; $i < count($tab2); $i ++) { ?>

          <?php
            $title   = $tab2[$i]['title'];
            $excerpt = $tab2[$i]['description_excerpt'];
            $full    = $tab2[$i]['description_full'];
            $tab_num = '2';
          ?>


          <?php if ($i == 0): ?>
            <div class="info-list-column info-list-left">
          <?php endif; ?>

          <?php if ($i == ($tab2_break)): ?>
            </div>
            <div class="info-list-column info-list-right">
          <?php endif; ?>

          <?php include('info-table-info-container.php'); ?>

          <?php if ($i == (count($tab2) - 1)): ?>
            </div>
          <?php endif; ?>

    		<?php } ?>
      </div>
    <?php endif; ?>

      <?php if (count($tab3) > 0): ?>
    	<div class="info-list" data-name="info-list-3">
    		<?php for ($i = 0; $i < count($tab3); $i ++) { ?>

          <?php
            $title   = $tab3[$i]['title'];
            $excerpt = $tab3[$i]['description_excerpt'];
            $full    = $tab3[$i]['description_full'];
            $tab_num = '3';
          ?>

          <?php if ($i == 0): ?>
            <div class="info-list-column info-list-left">
          <?php endif; ?>

          <?php if ($i == ($tab3_break)): ?>
            </div>
            <div class="info-list-column info-list-right">
          <?php endif; ?>

          <?php include('info-table-info-container.php'); ?>

          <?php if ($i == (count($tab3) - 1)): ?>
            </div>
          <?php endif; ?>

    		<?php } ?>
      </div>
    <?php endif; ?>



    </div>
  </div>
</div>

<?php } ?>

<script type="text/javascript">
  (function( $ ) {

    var button                = $('.system-features-toggle'),
        open_text             = button.data('open-text'),
        close_text            = button.data('close-text'),
        target                = $('.info-table-new-panel'),
        nav                   = $('.info-tabs'),
        lists                 = $('.info-lists');

    // Click handler for show/hide of whole panel
    button.on('click', function(e) {
      e.preventDefault();
      if (!target.hasClass('is-active')) {
        target.addClass('is-active');
        button.html(close_text);
      } else {
        target.removeClass('is-active');
        button.html(open_text);
      }
    });

    // Click handler for show/hide of lists
    nav.find('a').each(function() {
      $(this).on('click', function(e) {
        e.preventDefault();

        if (!$(this).hasClass('is-active')) {
          ClearActiveStates();
          var list_target = $(this).data('name');
          $(this).addClass('is-active');
          lists.find('.info-list[data-name="' + list_target + '"]').addClass('is-active');
        }
      });
    });

    // Click handler for show/hide of descriptions
    // Someone will want a Less - toggle here
    lists.find('.show-full-excerpt').each(function() {
      $(this).on('click', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('is-active')) {
          //$(this).addClass('is-active');
          $('.description-full[data-name="' + $(this).data('name') + '"]').addClass('is-active');
          // $(this).html($(this).data('close-text'));
          $(this).hide();
        }
        /*
        else {
          $(this).removeClass('is-active');
          $('.description-full[data-name="' + $(this).data('name') + '"]').removeClass('is-active');
          $(this).html($(this).data('open-text'));
        }
        */
      });
    });

    // Clear nav and tab active states
    function ClearActiveStates() {
      nav.find('a').each(function() {
        $(this).removeClass('is-active');
      });
      lists.find('.info-list').each(function() {
        $(this).removeClass('is-active');
      });
    };
  })(jQuery);
</script>
