<?php

	$child_pages = get_pages('child_of='.$post->ID.'&sort_order=ASC&sort_column=menu_order&parent='.$post->ID);
	$cp_count    = count($child_pages);

	$contextual  = get_post_meta( $post->ID, '_cmb2_contextual_nav', true );
	$columns     = get_post_meta( $post->ID, '_cmb2_child_columns', true );
	$type        = get_post_meta( $post->ID, '_cmb2_child_list_type', true );

	$counter       = 0;
	$child_count   = 0;

	$column_class= "col-sm-12";

	if ($columns == 2) {
		$column_class = "col-sm-6";
	}
	if ($columns == 3) {
		$column_class = "col-sm-4";
	}

?>


<div class="child-list pad-b-70">
	<?php the_content(); ?>

	<?php

		if ($child_pages) {

			// Show or hide the contextual nav
			if ($contextual != "No") {
				echo '<ul class="nav child-list-nav align-center subhead subhead-nav">';
					foreach ($child_pages as $key => $child) {
						$child_title = $child->post_title;
						$child_slug  = $child->post_name;

						echo '<li><span data-href="#' . $child_slug . '" class="h4 inline">' . $child_title . '</span></li>';
					}
				echo '</ul>';
			}

			// START - child list div
			echo '<div class="child-list ' . $type . '">';

			foreach ($child_pages as $key => $child) {

				$child_count   = 0;

				$child_title = $child->post_title;
				if (get_post_meta( $child->ID, '_cmb2_page_menu_title', true )) {
					$child_title = get_post_meta( $child->ID, '_cmb2_page_menu_title', true );
				}

				$child_slug  = $child->post_name;
				$grandchildren = get_pages('child_of='.$child->ID.'&sort_order=ASC&sort_column=menu_order&parent='.$child->ID);

				$child_hidden = get_post_meta( $child->ID, '_cmb2_hide_from_child_list', true );

				if ($columns == 1) {
					echo '<div class="row parent-container-row">';
				} else {
					if ($counter == 0) {
						echo '<div class="row parent-container-row-multi-columns">';
					}
				}

				if ($child_hidden != "yes") {

				echo '<div id="' . $child_slug . '" class="parent '. $column_class .'">';
				echo '<h3 class="parent-title"><strong>' . $child_title . '</strong></h3><hr/>';

				if ($grandchildren) {

					if ($type == "product") {
						echo '<div class="grandchild-holder">';
					} else {
						echo '<ul>';
					}

					// Grandchild item loop - START
					foreach ($grandchildren as $key => $grandchild) {

						$grandchild_hidden = get_post_meta( $grandchild->ID, '_cmb2_hide_from_child_list', true );

						if ($grandchild_hidden != "yes") {


							if ($type == "product") {
								if ($child_count == 0) {
									echo '<div class="row">';
								}
							}

							$grandchild_title       = $grandchild->post_title;
							if (get_post_meta( $grandchild->ID, '_cmb2_page_menu_title', true )) {
								$grandchild_title = get_post_meta( $grandchild->ID, '_cmb2_page_menu_title', true );
							}
							$url                    = get_permalink($grandchild->ID);
							$grandchild_cost        = get_post_meta( $grandchild->ID, '_cmb2_product_cost', true );
							$grandchild_image       = wp_get_attachment_image_src( get_post_meta( $grandchild->ID, '_cmb2_product_image_id', true ), 'product_thumb' );
							$grandchild_image_large = get_post_meta( $grandchild->ID, '_cmb2_product_image', true );

							$gc = "";

							if ($type == "product") {
								$gc .= '<div class="child col-sm-4 product-item">';
							} else {
								$gc .= '<li class="child col-sm-12 menu-item">';
							}


							$gc .= '<a href="' . $url . '">';

							if ($type == "product") {
								$gc .= '<div class="child-image">';

								if ($grandchild_image) {
									$gc .= '<picture>';
									$gc .= '<source srcset="' . $grandchild_image[0] . ' 1x, ' . $grandchild_image_large . ' 2x"/>';
									$gc .= '<img src="' . $grandchild_image[0] . '" alt="' . $grandchild_title . ' Image"/>';
									$gc .= '</picture>';
								} else {
									$gc .= '<img src="http://placehold.it/330x250/cccccc/ffffff/" alt="DogWatch placeholder image"/>';
								}

								$gc .= '</div>';
								$gc .= '<div class="child-title">' . $grandchild_title . '</div>';
								$gc .= '<div class="child-cost">$' . $grandchild_cost . '</div>';
							} else {
								$gc .= $grandchild_title;
							}

							$gc .= '</a>';

							// Close individual product item
							if ($type == "product") {
								$gc .= '</div>';
							} else {
								$gc .= '</li>';
							}

							echo $gc;

							// Close product row conditionally
							if ($type == "product") {
								if ($child_count == 2) {
									echo '</div>';
									$child_count = 0;
								} else {
									$child_count++;
									// Get that last child div close tag
									if ($key == count($grandchildren) - 1) {
										echo '</div>';
									}
								}
							}
						}
					}
					// Grandchild item loop - END



					// Close child list container
					if ($type == "product") {
						echo '</div>';
					} else {
						echo '</ul>';
					}


				} // End IF statement for if there are grandchildren

				// Close ID tag div
				echo '</div>';

				// If there is only one column just close the row

				if ($columns == 1) {
					echo '</div>';
				} else {

					if ($counter == $columns - 1) {
						echo '</div>';
						$counter = 0;
					} else {
						$counter++;
					}
				}





			}

		} // End of child hidden statement
			// END - child list div
			echo '</div>';
		}
	?>

</div>
