<?php
  $text_section = get_post_meta( $id, '_cmb2_text_section', true );
?>

<?php if (in_array('text_panel', $options)) { ?>

<?php for ($i = 0; $i < count($text_section); $i ++) { ?>

  <?php
    $panel_color = $text_section[$i]['panel_color'];
    $left_col    = $text_section[$i]['left_column_content'];
    $right_col   = $text_section[$i]['right_column_content'];
    $remove_tp   = $text_section[$i]['remove_top_padding'];
    $remove_bp   = $text_section[$i]['remove_bottom_padding'];
    $top_row     = $text_section[$i]['top_row_content'];

    $text_panel_class = "";

    if ($top_row != null && $top_row != "") {
      $text_panel_class = "with-top-row";
    }

  ?>

<div class="panel <?php echo $panel_color ?> text-panel <?php if ($remove_tp != 'yes') { echo 'pad-t-50'; } else { echo 'no-top-pad'; } ?> <?php if ($remove_bp != 'yes') { echo 'pad-b-50'; } else { echo 'no-bottom-pad'; } ?> <?php echo $text_panel_class; ?>">
  <div class="container">
    <?php if ($top_row != ""): ?>
      <div class="row">
        <div class="top-row-col col-sm-12">
          <?php echo apply_filters( 'the_content', $top_row); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($left_col != ""): ?>
    <div class="row">
      <div class="col col-sm-6">


        <? if ($left_col != "" && $left_col != null) { ?>
          <?php echo apply_filters( 'the_content', $left_col); ?>
        <?php } ?>


      </div>
      <div class="col col-sm-6">


        <? if ($right_col != "" && $right_col != null) { ?>
          <?php echo apply_filters( 'the_content', $right_col); ?>
        <?php } ?>


      </div>
    </div>
    <?php endif; ?>

  </div>
</div>

<?php } ?>

<?php } ?>
