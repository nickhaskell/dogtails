<?php
	$headline = get_post_meta( $id, '_cmb2_headline', true );
	$subheadline = get_post_meta( $id, '_cmb2_sub_headline', true );
	$forked_items = get_post_meta( $id, '_cmb2_forked_pages', true );
	$column_class = "col-sm-4";
	$image_class  = "three-col";

	if (count($forked_items) == 2) {
		$column_class = "col-sm-6";
		$image_class = "two-col";
	}
?>

<?php if (count($forked_items) > 0) { ?>
<div class="panel panel-white training-products-fork-panel pad-b-70">

	<?php if ($headline || $subheadline) { ?>
	<div class="product-content-header container align-center">
		<div class="row">
		<div class="col-sm-12">
			<div class="headline"><?php echo '<h2>' . $headline . '</h2>'; ?></div>
			<h4 class="subhead"><?php echo $subheadline; ?></h4>
		</div>
		</div>
	</div>
	<?php } ?>

	<div class="container pad-t-50 column-container <?php echo $image_class; ?>">
  	<div class="row related-items training-products-fork">


  		<ul>
  			<?php for ($i = 0; $i < count($forked_items); $i ++) { ?>

  			<?php {
  				$fp_url    = $forked_items[$i]['link'];
  				$fp_url_target = $forked_items[$i]['link_target'];
  				$fp_desc   = $forked_items[$i]['description'];
          $fp_title  = $forked_items[$i]['headline'];

  				$fp_img_id = $forked_items[$i]['image_id'];
					if (count($forked_items) == 2) {
						$fp_img_1x = wp_get_attachment_image_src( $fp_img_id, 'product_model_thumb_2_col' );
					} else {
						$fp_img_1x = wp_get_attachment_image_src( $fp_img_id, 'product_model_thumb' );
					}
  				$fp_img_2x = wp_get_attachment_image_src( $fp_img_id, 'full_size' );

					$image_alt   = get_post_meta($fp_img_id)['_wp_attachment_image_alt'][0];
					$image_title = get_the_title($fp_img_id);
  			} ?>

  			<li class="fork-item <?php echo $column_class; ?>">
          <?php if ($fp_img_id) { ?>
  					<div class="fork-item-image <?php echo $image_class; ?>">


	  					<?php if ($fp_url): ?>
	              <a href="<?php echo $fp_url; ?>" target="<?php echo $fp_url_target; ?>">
	  					<?php endif; ?>
  							<picture>
  								<source
										srcset="<?php echo $fp_img_1x[0]; ?> 1x, <?php echo $fp_img_2x[0]; ?> 2x"/>
  								<img
										src="<?php echo $fp_img_1x[0]; ?>"
										alt="<?php echo $image_alt; ?>"
										title="<?php echo $image_title; ?>"
										width="<?php echo $fp_img_1x[1]; ?>"
										height="<?php echo $fp_img_1x[2]; ?>"
										/>
  							</picture>
              <?php if ($fp_url): ?>
                </a>
              <?php endif; ?>


  					</div>
            <?php } ?>

  					<div class="training-product-item-headline align-center">
							<?php if ($fp_url): ?>
								<a href="<?php echo $fp_url; ?>" class="btn large" target="<?php echo $fp_url_target; ?>"><?php echo $fp_title; ?></a>
							<?php endif; ?>
            </div>
  					<div class="fork-item-description align-center">
  						<?php echo apply_filters( 'the_content', $fp_desc); ?>
  					</div>
  			</li>

  			<?php } ?>
  		</ul>
  	</div>
  </div>
</div>
<?php } ?>
