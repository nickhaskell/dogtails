<?php  ?>

<div class="panel dealer-list-panel pad-b-70">
  <div class="container">
    <ul class="dealer-list">
      <?php
      query_posts(array (
        'post_type' => 'dealer',
        'posts_per_page' => -1,
        'order' => 'ASC' ));
    if (have_posts()) : while (have_posts()) : the_post();

    endwhile; endif; wp_reset_query();
       ?>


       <?php
       $args = array(
           //'numberposts' => -1,
           'posts_per_page' => -1,
           'order' => 'ASC',
           'orderby' => 'title',
           'post_type' => 'dealer',
       );

       ?>

       <?php
         $my_query = null;
         $my_query = new WP_Query($args);
         if( $my_query->have_posts() ) {
           while ($my_query->have_posts()) : $my_query->the_post(); ?>

             <li class="dealer-item">
               <?php get_template_part( 'inc/partials/dealer-location-child-list-item' ); ?>
             </li>

             <?php
           endwhile;
         }
         wp_reset_query();

       ?>

    </ul>
  </div>
</div>
