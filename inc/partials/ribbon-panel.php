<?php
  $ribbon_panel_image_id = get_post_meta( $post->ID, '_cmb2_ribbon_panel_image_id', true );
  $ribbon_panel_image_2x = wp_get_attachment_image_src( $ribbon_panel_image_id, 'full_size' );
  $ribbon_panel_image_1x = wp_get_attachment_image_src( $ribbon_panel_image_id, 'ribbon@1x' );
  $image_data       = get_post_meta($ribbon_panel_image_id);
  $ribbon_panel_color    = get_post_meta( $post->ID, '_cmb2_ribbon_panel_color', true );
  $ribbon_panel_text     = get_post_meta( $post->ID, '_cmb2_ribbon_panel_text', true );

  $ribbon_panel_descriptor_class = "dark";

  $ribbon_link = get_option('general_setting_cdbb_link');

  if ($ribbon_panel_color != "panel-green-dark") {
    $ribbon_panel_descriptor_class == " ";
  }
?>

<?php if (in_array('ribbon_panel', $options)) { ?>

<div class="panel <?php echo $ribbon_panel_color; ?> ribbon-panel <?php echo $ribbon_panel_descriptor_class; ?>">
  <div class="container pad-tb-30">
    <div class="row">
      <div class="col-sm-2 ribbon-panel-image align-center">
        <?php if ($ribbon_link != "") { ?>
          <a href="<?php echo $ribbon_link; ?>" target="_blank" class="display-inline-block">
            <picture>
              <source
                srcset="
                  <?php echo $ribbon_panel_image_1x[0]; ?> 1x,
                  <?php echo $ribbon_panel_image_2x[0]; ?> 2x">
              <img
                src="<?php echo $ribbon_panel_image_1x[0]; ?>"
                alt="<?php echo $image_data['_wp_attachment_image_alt'][0];?>"
                title="<?php echo get_the_title($ribbon_panel_image_id); ?>"
                width="<?php echo $ribbon_panel_image_1x[1];?>"
                height="<?php echo $ribbon_panel_image_1x[2];?>">
            </picture>
          </a>
        <?php } else { ?>
          <picture>
            <source
              srcset="
                <?php echo $ribbon_panel_image_1x[0]; ?> 1x,
                <?php echo $ribbon_panel_image_2x[0]; ?> 2x">
                <img
                  src="<?php echo $ribbon_panel_image_1x[0]; ?>"
                  alt="<?php echo $image_data['_wp_attachment_image_alt'][0];?>"
                  title="<?php echo get_the_title($ribbon_panel_image_id); ?>"
                  width="<?php echo $ribbon_panel_image_1x[1];?>"
                  height="<?php echo $ribbon_panel_image_1x[2];?>">
          </picture>
        <?php } ?>
      </div>
      <div class="col-sm-10 ribbon-panel-text">
        <?php echo apply_filters( 'the_content', $ribbon_panel_text); ?>
      </div>
    </div>
  </div>
</div>

<?php } ?>
