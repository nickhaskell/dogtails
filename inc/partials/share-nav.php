<?php
$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$title = get_the_title();

if( get_post_thumbnail_id() ) {
    $img_id = get_post_thumbnail_id();
    $img_data = wp_get_attachment_image_src( $img_id, 'full_size' );
    $featured_image_url = $img_data[0];
}

$twitter_url   = 'http://twitter.com/share?url=' . $current_url;
$twitter_url  .= '&text=' . $title;
$twitter_url  .= '&via=DogWatchFence';

$fb_url        = 'https://www.facebook.com/dialog/share?app_id=325168201319256';
$fb_url       .= '&display=popup';
$fb_url       .= '&href=' . $current_url;
$fb_url       .= '&redirect_uri=' . $current_url;

$mail_url      = 'mailto:';
$mail_url     .= '?subject=Check out this link from DogTails by DogWatch';
$mail_url     .= '&body=' . $title . ": " . $current_url;

if(get_post_thumbnail_id()) {
    $pinterest_url = 'http://pinterest.com/pin/create/button/';
    $pinterest_url .= '?url=' . $current_url;
    $pinterest_url .= '&media=' . $featured_image_url;
    $pinterest_url .= '&description=' . $title;
}

?>
<div class="social-share-nav">
    <div class="social-share-nav__items">
        <strong>Share:</strong>
        <a href="<?php echo $fb_url; ?>" target="_blank" class="icon-facebook icon-circle" title="facebook"></a>
        <a href="<?php echo $twitter_url; ?>" target="_blank" class="icon-twitter icon-circle" title="twitter"></a>
        <a href="<?php echo $mail_url; ?>" target="_blank"  class="icon-email icon-circle" title="email">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12.713L.015 3h23.97L12 12.713zm0 2.574L0 5.562V21h24V5.562l-12 9.725z"/></svg>
        </a>
        <?php if(get_post_thumbnail_id()) : ?>
        <a href="<?php echo $pinterest_url; ?>" target="_blank" class="icon-pinterest-circled icon-circle" title="pinterest"></a>
        <?php endif; ?>
    </div>
</div>