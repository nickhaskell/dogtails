<?php
	$forked_items = get_post_meta( $id, '_cmb2_forked_pages', true );
?>

<?php if (count($forked_items) > 0) { ?>
<div class="panel panel-white fork-panel pad-t-40 pad-b-70">
  <div class="container">
  	<div class="row related-items fork">
  		<ul>
  			<?php for ($i = 0; $i < count($forked_items); $i ++) { ?>

  			<?php {
  				$fp_url    = $forked_items[$i]['link'];
  				$fp_desc   = $forked_items[$i]['description'];
          $fp_title  = $forked_items[$i]['headline'];

  				$fp_img_id = $forked_items[$i]['image_id'];
  				$fp_img_1x = wp_get_attachment_image_src( $fp_img_id, 'product_model_thumb' );
  				$fp_img_2x = wp_get_attachment_image_src( $fp_img_id, 'full_size' );
  			} ?>

  			<li class="fork-item col-sm-4">
          <?php if ($fp_img_id) { ?>
  					<div class="fork-item-image">


  					<?php if ($fp_url): ?>
              <a href="<?php echo $fp_url; ?>">
  					<?php endif; ?>


  							<picture>
  								<source srcset="<?php echo $fp_img_1x[0]; ?> 1x, <?php echo $fp_img_2x[0]; ?> 2x"/>
  								<img src="<?php echo $fp_img_1x[0]; ?>" alt="<?php echo $fp_title; ?>"/>
  							</picture>



              <?php if ($fp_url): ?>
                </a>
              <?php endif; ?>


  					</div>
            <?php } ?>

  					<h3 class="fork-item-headline">
              <strong><?php echo $fp_title; ?></strong>
            </h3>
  					<div class="fork-item-description">
  						<?php echo apply_filters( 'the_content', $fp_desc); ?>
  					</div>
  			</li>

  			<?php } ?>
  		</ul>
  	</div>
  </div>
</div>
<?php } ?>
