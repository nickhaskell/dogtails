<?php
  $col_count             = get_post_meta( $post->ID, '_cmb2_main_content_columns', true );
  $two_col_left_content  = get_post_meta( $post->ID, '_cmb2_two_col_left_content', true );
  $two_col_right_content = get_post_meta( $post->ID, '_cmb2_two_col_right_content', true );
  $one_col_content       = get_post_meta( $post->ID, '_cmb2_one_col_content', true );
?>

<?php if ($two_col_left_content != "" || $two_col_right_content != "" || $one_col_content != ""): ?>
<div class="panel panel-white">
  <div class="container">
    <div class="row">

      <?php if ($col_count == "one"): ?>

        <div class="col-sm-6 center-col pad-b-30">
          <?php echo apply_filters( 'the_content', $one_col_content); ?>
        </div>

      <?php else: ?>

        <div class="col-sm-6 col-l">
          <?php echo apply_filters( 'the_content', $two_col_left_content); ?>

            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('dealer-area-widgets-left') ) : ?>
                <?php the_widget('dealer-area-widgets-left'); ?>
            <?php endif; ?>

        </div>
        <div class="col-sm-6 col-r">
          <?php echo apply_filters( 'the_content', $two_col_right_content); ?>

          <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('dealer-area-widgets-right') ) : ?>
              <?php the_widget('dealer-area-widgets-right'); ?>
          <?php endif; ?>
        </div>

      <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>
