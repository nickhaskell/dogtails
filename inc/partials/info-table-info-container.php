<div class="info-container">
  <h4 class="title"><?php echo $title; ?></h4>
  <div class="description">
    <span class="description-excerpt">
      <?php echo $excerpt; ?>
      <?php if ($full): ?>
        <a href="#" class="show-full-excerpt" data-name="tab<?php echo $tab_num;?>-description-full-<?php echo $i;?>" data-open-text="More +" data-close-text="Less -">More +</a>
      <?php endif; ?>
      <span class="description-full" data-name="tab<?php echo $tab_num;?>-description-full-<?php echo $i;?>">
        <?php echo $full; ?>
      </span>
    </span>
  </div>
</div>
