<?php
  $col_count             = get_post_meta( $post->ID, '_cmb2_main_content_columns', true );
  $two_col_left_content  = get_post_meta( $post->ID, '_cmb2_two_col_left_content', true );
  $two_col_right_content = get_post_meta( $post->ID, '_cmb2_two_col_right_content', true );
  $one_col_content       = get_post_meta( $post->ID, '_cmb2_one_col_content', true );
?>

<?php if ($two_col_left_content != "" || $two_col_right_content != "" || $one_col_content != ""): ?>
<div class="panel panel-white main-content-panel">
  <div class="container">
    <div class="row">

      <?php if ($col_count == "one"): ?>

        <div class="col-sm-6 center-col pad-b-30">
          <?php echo apply_filters( 'the_content', $one_col_content); ?>
        </div>

      <?php elseif ($col_count == "one-fw"): ?>

        <div class="col-sm-12 pad-b-70">
          <?php echo apply_filters( 'the_content', $one_col_content); ?>
        </div>

      <?php elseif ($col_count == "three-q"): ?>

        <div class="col-sm-9 pad-b-70 three-q">
          <?php echo apply_filters( 'the_content', $one_col_content); ?>
        </div>

      <?php else: ?>

        <div class="col-sm-6 col-l">
          <?php echo apply_filters( 'the_content', $two_col_left_content); ?>
        </div>
        <div class="col-sm-6 col-r">
          <?php echo apply_filters( 'the_content', $two_col_right_content); ?>
        </div>

      <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>
