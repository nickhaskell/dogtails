<?php
global $user_login;
?>

<div class="panel panel-white">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 center-col pad-b-30">
        <div class="login-form-wrapper group">
<?php
if(isset($_GET['login']) && $_GET['login'] == 'failed')
{
    ?>
      <div class="aa_error">
        <p>FAILED: Try again!</p>
      </div>
    <?php
}
    if (is_user_logged_in()) {

        echo '<p class="align-center"><a id="wp-submit" class="btn" href="', wp_logout_url(), '" title="Logout">Logout</a></p>';

    } else {
         wp_login_form($args);

              $args = array(
                        'echo'           => true,
                        'redirect'       => site_url( $_SERVER['REQUEST_URI'] ),
                        'form_id'        => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Log In' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => true,
                        'value_username' => NULL,
                        'value_remember' => true
                        );
    }

?>
</div>

</div>
</div>
</div>
</div>
