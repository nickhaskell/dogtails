<?php
  $pt_size = get_post_meta( $post->ID, '_cmb2_photo_text_panel_size', true );
	$pt_align = get_post_meta( $post->ID, '_cmb2_photo_text_panel_alignment', true );

  $pt_text = get_post_meta( $post->ID, '_cmb2_photo_text_panel_text', true );

  $pts_img_id = get_post_meta( $post->ID, '_cmb2_photo_text_panel_image_small_id', true );
	$pts_img_1x = wp_get_attachment_image_src( $pts_img_id, 'pwt_small' );
	$pts_img_2x = wp_get_attachment_image_src( $pts_img_id, 'full_size' );

	$ptl_img_id = get_post_meta( $post->ID, '_cmb2_photo_text_panel_image_large_id', true );
	$ptl_img_1x = wp_get_attachment_image_src( $ptl_img_id, 'pwt_large' );
	$ptl_img_2x = wp_get_attachment_image_src( $ptl_img_id, 'full_size' );

  $pt_img_1x = "";
  $pt_img_2x = "";

  if ($pt_size == "panel_small") {
    $image_data = get_post_meta($pts_img_id);
    $image_title = get_the_title($pts_img_id);
    $pt_img_1x = $pts_img_1x;
    $pt_img_2x = $pts_img_2x;
  } else {
    $image_data = get_post_meta($ptl_img_id);
    $image_title = get_the_title($ptl_img_id);
    $pt_img_1x = $ptl_img_1x;
    $pt_img_2x = $ptl_img_2x;
  }
?>

<?php if (in_array('photo_text_panel', $options)) { ?>
<div class="panel panel-white photo-text-panel dark <?php echo $pt_size; ?> <?php echo $pt_align; ?>">

  <div class="photo-text-panel-text-container">
    <div class="container">
      <div class="photo-text-panel-text ">
        <div class="photo-text-panel-image-content">
          <?php echo apply_filters( 'the_content', $pt_text); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="photo-text-panel-image">
		<picture>
			<source
				srcset="<?php echo $pt_img_1x[0]; ?> 1x,
                <?php echo $pt_img_2x[0]; ?> 2x">
			<img
        src="<?php echo $pt_img_1x[0]; ?>"
        alt="<?php echo $image_data['_wp_attachment_image_alt'][0];?>"
        title="<?php echo $image_title; ?>"
        width="<?php echo $pt_img_1x[1]; ?>"
        height="<?php echo $pt_img_1x[2]; ?>"/>
		</picture>
	</div>

</div>
<?php } ?>
