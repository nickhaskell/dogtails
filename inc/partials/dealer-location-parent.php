<?php
  // This is the partial for the highest level of dealer location, eg. United States

  $taxonomy_name = 'dealer_location';

  $child_slug      = $post->post_name;
  $child_title     = $post->post_title;
  $child_id        = $post->ID;

  $name       = get_post_meta( $child_id, '_cmb2_dealer_name', true );
  $location   = get_post_meta( $child_id, '_cmb2_dealer_description', true );
  $owner      = get_post_meta( $child_id, '_cmb2_dealer_owner', true );
  $phone      = get_post_meta( $child_id, '_cmb2_dealer_phone', true );
  $secondary  = get_post_meta( $child_id, '_cmb2_dealer_secondary_phone', true );
  $toll_free  = get_post_meta( $child_id, '_cmb2_dealer_toll_free', true );
  $email      = get_post_meta( $child_id, '_cmb2_dealer_email', true );
  $link       = get_post_meta( $child_id, '_cmb2_dealer_link', true );
?>

<div class="dealer-locations">

<?php
  echo '<ul class="dealer-archive-list-parent">';

  $parent_args = array(
    'numberposts' => -1,
    'orderby' => 'name',
    'order' => 'ASC',
    'post_type' => 'dealer',
    'tax_query' => array(
      array(
        'taxonomy' => 'dealer_location',
        'field' => 'id',
        'terms' => $children->term_id, // Where term_id of Term 1 is "1".
        'include_children' => false
      ))
    );

  $children = get_posts( $parent_args );

  foreach ( $children as $child )  {
    $term = get_term_by( 'id', $child, $taxonomy_name );
    echo '<li><h3><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></h3><hr/>';

    $args = array(
          'taxonomy' => $taxonomy_name,
          'term' => $term->name,
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'post_status' => 'publish',
        );

  query_posts( $args );

  echo '<ul class="dealer-archive-list">';

    while ( have_posts() ) : the_post();
        echo '<li class="dealer-archive-item">';
        get_template_part( 'inc/partials/dealer-location' );
        echo '</li>';
    endwhile;

  echo '</ul>';

  }
  echo '</li></ul>';
?>
