<?php
	$show_dealer_nav  = get_post_meta( $post->ID, '_cmb2_dealer_area_nav', true );

	class Walker_Nav_Menu_Dropdown_Two extends Walker_Nav_Menu{

	    // don't output children opening tag (`<ul>`)
	    public function start_lvl(&$output, $depth) {}

	    // don't output children closing tag
	    public function end_lvl(&$output, $depth) {}

	    public function start_el(&$output, $item, $depth, $args){

	      // add spacing to the title based on the current depth
	      $item->title = str_repeat("- ", $depth) . $item->title;

	      // call the prototype and replace the <li> tag
	      // from the generated markup...
	      parent::start_el($output, $item, $depth, $args);

	      $output = str_replace('<li', '<option value="' . $item->url . '"', $output);
	    }

	    // replace closing </li> with the closing option tag
	    public function end_el($output, $item, $depth){
	      $output .= "</option>\n";
	    }
	}

?>

<?php if ( is_user_logged_in() ) { ?>

<?php if ($show_dealer_nav && $show_dealer_nav == 'yes') { ?>
<div class="panel panel-white dealer-nav pad-b-30">
  <div class="container">
    <div class="row form ">
      <div class="col-sm-4 nav">
        <?php wp_nav_menu(
          array(
            'theme_location' => 'dealer',
              'walker'         => new Walker_Nav_Menu_Dropdown_Two(),
              'items_wrap'     => '<select class="styled-select ajax-select-form" data-placeholder="Dealer Area Menu" data-links="true" data-size="8">%3$s</select>'
        )); ?>
      </div>
    </div>
  </div>
</div>
<?php } elseif (class_exists('bbPress') && is_bbpress()) { ?>
	<div class="panel panel-white dealer-nav pad-b-30">
	  <div class="container">
	    <div class="row form ">
	      <div class="col-sm-4 nav">
	        <?php wp_nav_menu(
	          array(
	            'theme_location' => 'dealer',
	              'walker'         => new Walker_Nav_Menu_Dropdown_Two(),
	              'items_wrap'     => '<select class="styled-select ajax-select-form" data-placeholder="Dealer Area Menu" data-links="true" data-size="8">%3$s</select>'
	        )); ?>
	      </div>
	    </div>
	  </div>
	</div>
<?php } elseif (is_cart() || is_checkout() || is_page( array( 706 ) )) { ?>
	<div class="panel panel-white dealer-nav pad-b-30">
	  <div class="container">
	    <div class="row form ">
	      <div class="col-sm-4 nav">
	        <?php wp_nav_menu(
	          array(
	            'theme_location' => 'dealer',
	              'walker'         => new Walker_Nav_Menu_Dropdown_Two(),
	              'items_wrap'     => '<select class="styled-select ajax-select-form" data-placeholder="Dealer Area Menu" data-links="true" data-size="8">%3$s</select>'
	        )); ?>
	      </div>
	    </div>
	  </div>
	</div>
<?php } ?>

<?php } ?>
