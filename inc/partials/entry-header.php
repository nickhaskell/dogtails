<?php
$hide_page_header   = get_post_meta( $id, '_cmb2_hide_page_header', true );
?>

<?php if ($hide_page_header == "on"): ?>
<style media="screen">
.entry-header {
  display:none;
}
.sub-page {
  padding-top: 0;
}
</style>
<?php endif; ?>

<div class="entry-header container align-center  <?php if (isset($subhead) && !$subhead) { echo 'no-subhead'; } ?>">
  <?php if ( is_user_logged_in() && get_current_user_role() == "Administrator") { echo "<a class='edit-link btn small grey' href='" . get_edit_post_link( $id ) . "'>Edit</a>"; } ?>
  <h1 class="entry-title headline">
    <?php
      if ($headline_alt) {
        echo $headline_alt;
      } else {
        the_title();
      }
    ?>
  </h1>

  <?php if ($headline_button_link && $headline_button_text): ?>
    <div class="headline-button">
      <a href="<?php echo $headline_button_link; ?>" <?php if ($headline_button_target) { echo "target='_blank'"; } ?>class="btn grey"><?php echo $headline_button_text; ?></a>
    </div>
  <?php endif ?>

  <?php if ($headline_optional) { ?>
    <h2 class="headline-optional">
      <?php echo $headline_optional; ?>
    </h4>
  <?php } ?>

  <?php if ($headline_sub) { ?>
    <h4 class="headline-sub subhead">
      <?php echo $headline_sub; ?>
    </h4>
  <?php } ?>

</div>
