<?php
  $headline = getMeta('slider_panel_headline');
  $slides = get_post_meta( $id, '_cmb2_slider_panel_new_slides', true );
  $slider_class = "single";
  if (count($slides) > 1) {
    $slide_class = "multi";
  }
?>

<?php if (in_array('slider_panel_new', $options)) { ?>
  <!--noptimize-->
  <style media="screen">
  	<?php echo file_get_contents( get_template_directory_uri() . "/dist/styles/components/slider-panel-new.css"); ?>
  </style>
  <!--/noptimize-->
<div class="slider-panel-new <?php echo $slide_class; ?>">
  <div class="slides">
      <div class="slide">
        <div class="slide-column left-column">
          <?php foreach ($slides as $key => $slide): ?>
            <?php
              $image_id_1x       = $slide['image_1x_id'];
              $image_id_2x       = $slide['image_2x_id'];
              $image_1x          = wp_get_attachment_image_src( $image_id_1x, 'full' );
              $image_2x          = wp_get_attachment_image_src( $image_id_2x, 'full' );
              $image_alt         = get_post_meta($image_id_1x)['_wp_attachment_image_alt'][0];
              $image_title       = get_the_title($image_id_1x);
              $srcset            = $image_1x[0] . ' 1x';

              if ($image_id_2x) {
                $srcset .= ', ' . $image_2x[0] . ' 2x';
              }
            ?>
          <picture class="slide-<?php echo $key; ?> <?php if ($key == 0) { echo 'is-active'; } ?>" style="<?php echo 'max-width: ' . $image_1x[1] . 'px;'; ?>">
            <source srcset="<?php echo $srcset; ?>">
            <img
              src="<?php echo $image_1x[0]; ?>"
              alt="<?php echo $image_alt; ?>"
              title="<?php echo $image_title; ?>"
              width="<?php echo $image_1x[1]; ?>"
              height="<?php echo $image_1x[2]; ?>">
          </picture>
          <?php endforeach; ?>
        </div>
        <div class="slide-column right-column">
          <?php if ($headline): ?>
            <h5 class="title slides-headline"><?php echo $headline; ?></h5>
          <?php endif; ?>
          <?php if (count($slides) > 1): ?>
          <div class="navigation-holder">
            <ul class="navigation">
              <?php foreach ($slides as $key => $slide): ?>
                <?php
                $slide_class = $slide['nav_item_class'];
                if ($slide_class == "") {
                  $slide_class = "default";
                }
                ?>
                <li class="slide-<?php echo $key; ?> <?php if ($key == 0) { echo 'is-active'; } ?>"><a href="#" class="<?php echo $slide_class; ?>" data-slide="<?php echo $key; ?>"></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endif; ?>
          <h2 class="headline color-grey">
            <?php foreach ($slides as $key => $slide): ?>
            <span class="slide-<?php echo $key;?> <?php if ($key == 0) { echo 'is-active'; } ?>"><?php echo $slide['title']; ?></span>
            <?php endforeach; ?>
          </h2>
          <h4 class="description">
            <?php foreach ($slides as $key => $slide): ?>
            <span class="slide-<?php echo $key; ?> <?php if ($key == 0) { echo 'is-active'; } ?>"><?php echo $slide['description']; ?></span>
            <?php endforeach; ?>
          </h4>
        </div>
      </div>
  </div>

</div>

<?php if (count($slides) > 1): ?>
  <script type="text/javascript">
    (function( $ ) {

      var slider = {

        parent: $('.slider-panel-new'),
        nav: $('.slider-panel-new .navigation'),
        images: $('.slider-panel-new .left-column'),
        descriptions: $('.slider-panel-new .description'),
        headlines: $('.slider-panel-new .headline'),
        slides_count: $('.slider-panel-new .navigation li').length,
        active_slide: 0,

        configs: {
          rotate_time: 6000,
          transition_time: 1000,
        },

        timeout: '',

        init: function() {
          console.log('initialized slider');
          this.addHandlers();
          this.startRotation();
        },

        addHandlers: function() {
          console.log('adding handlers');
          var obj = this;
          obj.nav.find('a').each(function() {
            $(this).on('click', function(e) {
              e.preventDefault();
              obj.resetRotation();
              obj.clearActiveSlides();
              obj.setActiveSlide($(this).data('slide'));
              obj.startRotation();
            });
          });
        },

        startRotation: function() {
          console.log('starting slide rotation');
          var obj = this;

          obj.timeout = setTimeout(function() {
            obj.rotateSlide();
          }, obj.configs.rotate_time);
        },

        resetRotation: function() {
          console.log('reseting slide rotation');
          var obj = this;
          // Clear the timer on the current timeout
          window.clearTimeout(obj.timeout);
        },

        rotateSlide: function() {
          console.log('rotating slide');
          var obj = this;
          var new_active_slide = "";
          console.log(this.active_slide);
          console.log(this.slides_count);

          // If active slide reaches the limit, set it back to 0
          if (obj.active_slide < (obj.slides_count - 1)) {
            new_active_slide = obj.active_slide + 1;
          } else {
            new_active_slide = 0;
          }
          obj.clearActiveSlides();
          obj.setActiveSlide(new_active_slide);
          obj.startRotation();
        },

        // Add transition-out class
        fadeOutActiveSlides: function() {
          var obj = this;
            obj.clearActiveSlides();
            obj.setActiveSlide();
            obj.startRotation();
        },

        setActiveSlide: function(slide_num) {
          console.log('setting active slide');
          var obj = this;
          obj.active_slide = slide_num;
          this.parent.find('.slide-' + obj.active_slide).addClass('is-active');
        },

        clearActiveSlides: function() {
          console.log('clearing active slide');
          var obj = this;
          this.parent.find('.is-active').removeClass('is-active transition-out');
        },

      };
      slider.init();

    })(jQuery);
  </script>
<?php endif; ?>
<?php } ?>
