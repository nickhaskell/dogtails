<?php
$headline       = woo_settings_get_option('woo_settings_hero_text');
$image_id       = woo_settings_get_option('woo_settings_hero_img_id');

if ($image_id) {
    $image_1x       = wp_get_attachment_image_src( $image_id, 'hero_product_header_new@1x' );
    $image_2x       = wp_get_attachment_image_src( $image_id, 'hero_product_header_new@2x' );
    $image_alt      = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
    $image_title    = get_the_title($image_id);

    $w                 = $image_1x[1];
    $h                 = $image_1x[2];
    $ratio             = ($h / $w) * 100;
    $intrinsic_padding .= "padding-top: " . $ratio . "%;";

    if (exif_imagetype($image_1x[0]) == 3) {
        $im = imagecreatefrompng($image_1x[0]);
    }
    if (exif_imagetype($image_1x[0]) == 2) {
        $im = imagecreatefromjpeg($image_1x[0]);
    }

    $rgb = imagecolorat($im, 0, 0);
    $colors = imagecolorsforindex($im, $rgb);
    $image_background_color = "background-color: rgba(".$colors['red'].",".$colors['green'].",".$colors['blue'].",0.35);";
}

$skroll_to = "transform: translate3d(0px, 33.333333%, 0px)";
$skroll_from = "transform: translate3d(0px, 0%, 0px)";
$skroll_start = "transform: translate3d(0px, 0%, 0px)";
?>

<div class="hero dark">
    <ul class="single-image-slider product-new-slider store-slider">
        <li>
            <div class="slide-container">
                <div class="picture__holder skrollable-hero" data-start="<?php echo $skroll_from;?>" data-top-bottom="<?php echo $skroll_to;?>" style="<?php echo $skroll_start;?>">
                    <picture
                        class="intrinsic"
                        style="<?php echo $intrinsic_padding; ?> <?php echo $image_background_color; ?>">
                        <source
                            srcset="<?php echo $image_1x[0]; ?> 1x, <?php echo $image_2x[0]; ?> 2x">
                        <img
                            src="<?php echo $image_1x[0]; ?>"
                            alt="<?php echo $image_title; ?>"
                            title="<?php echo $image_alt; ?>"
                            width="<?php echo $image_1x[1]; ?>"
                            height="<?php echo $image_1x[2]; ?>"
                            class="intrinsic-item"/>
                    </picture>
                </div>
                <div class="slide-copy-container">
                    <div class="slide-copy">
                        <h5 class="hero">
                            <?php woocommerce_page_title(); ?>
                        </h5>
                        <h1 class="hero">
                            <a href="<?php echo wc_get_page_permalink( 'shop' ); ?>">
                                <?php echo $headline; ?>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
