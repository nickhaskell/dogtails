<?php
  $quote       = get_post_meta( $id, '_cmb2_customer_story_quote', true );
  $image_id    = get_post_meta( $id, '_cmb2_customer_story_image_id', true );
  if ($image_id) {

    $image_1x    = wp_get_attachment_image_src( $image_id, 'customer_story@1x' );
    $image_2x    = wp_get_attachment_image_src( $image_id, 'customer_story@2x' );
    $image_m_1x    = wp_get_attachment_image_src( $image_id, 'customer_story_m@1x' );
    $image_m_2x    = wp_get_attachment_image_src( $image_id, 'customer_story_m@2x' );

    $image_alt   = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
    $image_title = get_the_title($image_id);

    if (exif_imagetype($image_1x[0]) == 3) {
        $im = imagecreatefrompng($image_1x[0]);
    }
    if (exif_imagetype($image_1x[0]) == 2) {
        $im = imagecreatefromjpeg($image_1x[0]);
    }

    $rgb = imagecolorat($im, 0, 0);
    $colors = imagecolorsforindex($im, $rgb);
    $image_background_color = "background-color: rgba(".$colors['red'].",".$colors['green'].",".$colors['blue'].",0.35);";
}
?>

<?php if (in_array('customer_story', $options)) { ?>
<div class="customer-story dark">
  <div class="content-container">
    <h5 class="title">Customer Story</h5>
    <h4 class="quote"><?php echo $quote; ?></h4>
  </div>
  <div class="image-container">
    <?php if ($image_id): ?>
    <picture style="<?php echo $image_background_color; ?>">
      <source
        srcset="<?php echo $image_1x[0]; ?> 1x,
                <?php echo $image_2x[0]; ?> 2x"
        media="(min-width:1200px)"/>
      <source
        srcset="<?php echo $image_m_1x[0]; ?> 1x,
                <?php echo $image_m_2x[0]; ?> 2x"
        media="(min-width:960px)"/>
      <source
        srcset="<?php echo $image_1x[0]; ?> 1x,
                <?php echo $image_2x[0]; ?> 2x"
        media="(max-width:959px)"/>
      <img
        src="<?php echo $image_1x[0]; ?>"
        alt="<?php echo $image_alt; ?>"
        title="<?php echo $image_title; ?>"
        width="<?php echo $image_1x[1]; ?>"
        height="<?php echo $image_1x[2]; ?>">
    </picture>
    <?php endif; ?>
  </div>
</div>
<?php } ?>
