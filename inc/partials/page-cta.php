<?php
  $headline = getMeta('page_cta_headline');
  $button_text = getMeta('page_cta_button_text');
  $button_url = getMeta('page_cta_button_url');
?>
<?php if (in_array('page_cta', $options)) : ?>
<div class="blog-cta page-cta panel panel-green-dark dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 blog-cta__content">
        <?php if ($headline): ?>
          <h2 class="blog-cta__content__headline color-white">
            <strong><?php echo $headline; ?></strong>
          </h2>
        <?php endif; ?>
        <?php if ($button_text): ?>
          <a href="<?php echo $button_url; ?>" class="blog-cta__content__button btn another-green"><?php echo $button_text; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
