<?php
  $cp_title = get_post_meta( $id, '_cmb2_comparison_panel_title', true );
  $cp_left  = get_post_meta( $id, '_cmb2_comparison_panel_left_column_content', true );
  $cp_right  = get_post_meta( $id, '_cmb2_comparison_panel_right_column_content', true );

  $cp_f1_link = get_post_meta( $id, '_cmb2_comparison_panel_file_1_file', true );
  $cp_f1_text = get_post_meta( $id, '_cmb2_comparison_panel_file_1_link_text', true );
  $cp_f1_description = get_post_meta( $id, '_cmb2_comparison_panel_file_1_description', true );
  $cp_f2_link = get_post_meta( $id, '_cmb2_comparison_panel_file_2_file', true );
  $cp_f2_text = get_post_meta( $id, '_cmb2_comparison_panel_file_2_link_text', true );
  $cp_f2_description = get_post_meta( $id, '_cmb2_comparison_panel_file_2_description', true );
  $cp_f3_link = get_post_meta( $id, '_cmb2_comparison_panel_file_3_file', true );
  $cp_f3_text = get_post_meta( $id, '_cmb2_comparison_panel_file_3_link_text', true );
  $cp_f3_description = get_post_meta( $id, '_cmb2_comparison_panel_file_3_description', true );
?>


<?php if (in_array('comparison_panel', $options)) { ?>
<div class="panel panel-tan comparison-panel">
  <div class="container pad-t-80 pad-b-70">
    <div class="row">
      <div class="col-sm-12 comparison-panel-title align-center">
        <h3><strong><?php echo $cp_title; ?></strong></h3>
      </div>
    </div>
    <div class="row comparison-panel-content">
      <div class="col-sm-12 comparison-panel-left-column align-center unmargin-last">
        <?php echo apply_filters( 'the_content', $cp_left); ?>
      </div>
    </div>
    <div class="row comparison-panel-files">
      <div class="col-sm-4 comparison-panel-file align-center">
        <a href="<?php echo $cp_f1_link; ?>" class="btn white"><?php echo $cp_f1_text; ?></a>
        <p>
          <?php echo $cp_f1_description; ?>
        </p>
      </div>
      <div class="col-sm-4 comparison-panel-file align-center">
        <a href="<?php echo $cp_f2_link; ?>" class="btn white"><?php echo $cp_f2_text; ?></a>
        <p>
          <?php echo $cp_f2_description; ?>
        </p>
      </div>
      <div class="col-sm-4 comparison-panel-file align-center">
        <a href="<?php echo $cp_f3_link; ?>" class="btn white"><?php echo $cp_f3_text; ?></a>
        <p>
          <?php echo $cp_f3_description; ?>
        </p>
      </div>
    </div>
  </div>
</div>
<?php } ?>
