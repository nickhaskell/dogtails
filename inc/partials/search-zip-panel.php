<form method="get" class="zipsearch combined-input" action="//www.dogwatch.com/">
<?php /* <form method="get" class="zipsearch combined-input" action="<?php echo esc_url( home_url( '/' ) ); ?>"> */ ?>
    <input class="zipcode combined-input-input" type="tel" name="s" value = "" pattern="\d{5,5}(-\d{4,4})?" maxlength="5" placeholder="Zip Code">
    <input type="hidden" name="post_type" value="dealer" />
    <button type="submit" class="submit combined-input-button" name="submit"><span class="icon-search-2"></span></button>
</form>
