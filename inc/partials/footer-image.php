<?php
	$footer_img_id = get_post_meta( $post->ID, '_cmb2_footer_image_id', true );
	$footer_img_1x = wp_get_attachment_image_src( $footer_img_id, 'hero_product' );
	$footer_img_2x = wp_get_attachment_image_src( $footer_img_id, 'full_size' );
?>

<?php if (in_array('footer_image', $options)) { ?>

	<div class="footer-image dark">
		<picture>
			<source
				srcset="<?php echo $footer_img_1x[0]; ?> 1x, <?php echo $footer_img_2x[0]; ?> 2x">
			<img src="<?php echo $footer_img_1x[0]; ?>">
		</picture>
	</div>

<?php } ?>
