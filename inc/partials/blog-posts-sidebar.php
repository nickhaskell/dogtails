<div class="blog-posts-sidebar">
  <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('dw-blog-widgets-right') ) : ?>
    <?php the_widget('dw-blog-widgets-right'); ?>
  <?php endif; ?>
</div>
