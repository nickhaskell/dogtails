<?php
	//$show_find_a_dealer  = get_post_meta( $post->ID, '_cmb2_find_a_dealer_form', true );

	class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{

	    // don't output children opening tag (`<ul>`)
	    public function start_lvl(&$output, $depth) {}

	    // don't output children closing tag
	    public function end_lvl(&$output, $depth) {}

	    public function start_el(&$output, $item, $depth, $args){

	      // add spacing to the title based on the current depth
	      $item->title = str_repeat("&nbsp;", $depth * 4) . $item->title;

	      // call the prototype and replace the <li> tag
	      // from the generated markup...
	      parent::start_el($output, $item, $depth, $args);

	      $output = str_replace('<li', '<option value="' . $item->url . '"', $output);
	    }

	    // replace closing </li> with the closing option tag
	    public function end_el($output, $item, $depth){
	      $output .= "</option>\n";
	    }
	}

?>

<?php if (in_array('find_a_dealer_form_panel', $options)) { ?>
<div class="panel panel-green-dark find-a-dealer-form-panel dark">
  <div class="container find-a-dealer-form pad-tb-50">
		<div class="row form-header">
			<div class="col-sm-12">
				<h4 class="align-center">Search for your local dealer by zip code, state or country:</h4>
			</div>
		</div>
  	<div class="row form-zip-search">
  		<div class="col-sm-12 align-center pad-b-50">

				<?php include 'search-zip-panel.php'; ?>

  		</div>
  	</div>
  	<div class="row form">
  		<div class="col-sm-6 col-l">
  			<?php wp_nav_menu(
  				array(
  					'theme_location' => 'domestic',
  						'walker'         => new Walker_Nav_Menu_Dropdown(),
  						'items_wrap'     => '<select class="styled-select ajax-select-form" data-links="true" data-size="8" data-placeholder-option="true">%3$s</select>',
  			)); ?>
  		</div>
  		<div class="col-sm-6 col-r">
  			<?php wp_nav_menu(
  				array(
  					'theme_location' => 'international',
  						'walker'         => new Walker_Nav_Menu_Dropdown(),
  						'items_wrap'     => '<select class="styled-select ajax-select-form" data-links="true" data-size="8" data-placeholder-option="true">%3$s</select>',
  			)); ?>
  		</div>
  	</div>
  </div>
</div>
<?php } ?>
