<?php
  $headline       = get_post_meta( $id, '_cmb2_product_hero_new_headline', true );
  $headline_color = get_post_meta( $id, '_cmb2_product_hero_new_headline_color', true );
  $image_id       = get_post_meta( $id, '_cmb2_product_hero_new_image_id', true );

  if ($image_id) {

    $image_1x       = wp_get_attachment_image_src( $image_id, 'hero_product_new@1x' );
    $image_2x       = wp_get_attachment_image_src( $image_id, 'hero_product_new@2x' );
    $image_alt      = get_post_meta($image_id)['_wp_attachment_image_alt'][0];
    $image_title    = get_the_title($image_id);

    $w                 = $image_1x[1];
    $h                 = $image_1x[2];
    $ratio             = ($h / $w) * 100;
    $intrinsic_padding .= "padding-top: " . $ratio . "%;";

    if (exif_imagetype($image_1x[0]) == 3) {
        $im = imagecreatefrompng($image_1x[0]);
    }
    if (exif_imagetype($image_1x[0]) == 2) {
        $im = imagecreatefromjpeg($image_1x[0]);
    }

    $rgb = imagecolorat($im, 0, 0);
    $colors = imagecolorsforindex($im, $rgb);
    $image_background_color = "background-color: rgba(".$colors['red'].",".$colors['green'].",".$colors['blue'].",0.35);";
  }
?>

<?php if (in_array('product_hero_new', $options)) { ?>
<div class="product-hero-new <?php echo $headline_color; ?>">
  <div class="image-container">
    <?php if ($image_id): ?>
    <picture
      style="<?php echo $image_background_color; ?>">
      <source
        srcset="<?php echo $image_1x[0]; ?> 1x,
                <?php echo $image_2x[0]; ?> 2x">
      <img
        src="<?php echo $image_1x[0]; ?>"
        alt="<?php echo $image_alt; ?>"
        title="<?php echo $image_title; ?>"
        width="<?php echo $image_1x[1]; ?>"
        height="<?php echo $image_1x[2]; ?>"
        class="intrinsic-item"/>
    </picture>
    <?php endif; ?>
  </div>
  <div class="content-container">
    <h6 class="romance"><?php echo $headline; ?></h6>
  </div>
</div>
<?php } ?>
