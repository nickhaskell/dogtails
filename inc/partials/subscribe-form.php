<div style="display:none;">
    <div class="subscribe-form v--25">
        <div class="subscribe-form__logo">
            <?php get_template_part("/inc/svg/logo"); ?>
        </div>
        <div class="subscribe-form__intro v--15">
            <h2>Like what you see? Subscribe to DogTails!</h2>
            <p>Get our monthly email newsletter, featuring the latest pet safety guides, dog training tips and more, designed for pet parents like you! Don’t worry, we’ll never share your information!</p>
        </div>
        <div class="subscribe-form__form v--25">
            <div class="subscribe-form__form__body form">
                <?php echo gravity_form(1, false, false, false, null, true,);?>
            </div>
            <div class="subscribe-form__form__footer">
                <p>By submitting this form, you consent to receive emails from DogWatch, Inc. You can unsubscribe at any time by using the SafeUnsubscribe® link, found at the bottom of every email.</p>
            </div>
        </div>
    </div>
</div>