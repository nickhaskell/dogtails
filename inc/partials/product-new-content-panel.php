<?php
	$product_overview    = get_post_meta( $id, '_cmb2_product_overview', true );
	$product_includes    = get_post_meta( $id, '_cmb2_product_includes', true);

	$product_video_url   = get_post_meta( $id, '_cmb2_product_video_url', true);

  //$product_image       = wp_get_attachment_image_src( get_post_meta( $id, '_cmb2_product_image_id', true ), 'product_thumb' );
	//$product_image_large = get_post_meta( $id, '_cmb2_product_image', true );

  $product_img_id      = get_post_meta( $id, '_cmb2_product_image_id', true );
  $product_img_2x_id   = get_post_meta( $id, '_cmb2_product_image_2x_id', true );
  $product_img_1x      = wp_get_attachment_image_src( $product_img_id, 'full_size' );
  $product_img_2x      = wp_get_attachment_image_src( $product_img_2x_id, 'full_size' );
	$image_alt           = get_post_meta($product_img_id)['_wp_attachment_image_alt'][0];
	$image_title         = get_the_title($product_img_id);


  $product_logo        = get_post_meta( $id, '_cmb2_product_logo', true );

	$product_headline    = get_post_meta( $id, '_cmb2_product_headline', true );
	$product_subheadline = get_post_meta( $id, '_cmb2_product_sub_headline', true );

	$product_model       = get_post_meta( $id, '_cmb2_product_model_number', true );

	$include_product_info = get_post_meta( $id, '_cmb2_include_product_info', true );
	$include_product_models = get_post_meta( $id, '_cmb2_include_product_models', true );

	$product_model_1_name = get_post_meta( $id, '_cmb2_product_model_1_name', true );
	$product_model_1_content = get_post_meta( $id, '_cmb2_product_model_1_content', true );
	$product_model_1_img_id = get_post_meta( $id, '_cmb2_product_model_1_image_id', true );
	$product_model_1_img_1x = wp_get_attachment_image_src( $product_model_1_img_id, 'product_model_thumb' );
	$product_model_1_img_2x = wp_get_attachment_image_src( $product_model_1_img_id, 'full_size' );
	$p1_alt                 = get_post_meta($product_model_1_img_id)['_wp_attachment_image_alt'][0];
	$p1_title               = get_the_title($product_model_1_img_id);


	$product_model_2_name = get_post_meta( $id, '_cmb2_product_model_2_name', true );
	$product_model_2_content = get_post_meta( $id, '_cmb2_product_model_2_content', true );
	$product_model_2_img_id = get_post_meta( $id, '_cmb2_product_model_2_image_id', true );
	$product_model_2_img_1x = wp_get_attachment_image_src( $product_model_2_img_id, 'product_model_thumb' );
	$product_model_2_img_2x = wp_get_attachment_image_src( $product_model_2_img_id, 'full_size' );
	$p2_alt                 = get_post_meta($product_model_2_img_id)['_wp_attachment_image_alt'][0];
	$p2_title               = get_the_title($product_model_2_img_id);

	$product_model_3_name = get_post_meta( $id, '_cmb2_product_model_3_name', true );
	$product_model_3_content = get_post_meta( $id, '_cmb2_product_model_3_content', true );
	$product_model_3_img_id = get_post_meta( $id, '_cmb2_product_model_3_image_id', true );
	$product_model_3_img_1x = wp_get_attachment_image_src( $product_model_3_img_id, 'product_model_thumb' );
	$product_model_3_img_2x = wp_get_attachment_image_src( $product_model_3_img_id, 'full_size' );
	$p3_alt                 = get_post_meta($product_model_3_img_id)['_wp_attachment_image_alt'][0];
	$p3_title               = get_the_title($product_model_3_img_id);
?>

<div class="panel product-content-panel product-content-panel-new panel-white">
	<div class="container">

		<?php if ($product_headline || $product_subheadline) { ?>
		<div class="product-content-header align-center">
			<div class="row">
			<div class="col-sm-12">
				<div class="headline"><?php echo '<h2>' . $product_headline . '</h2>'; ?></div>
				<h4 class="subhead"><?php echo $product_subheadline; ?></h4>
				<?php if ($product_video_url): ?>
					<a href="#" class="btn video-display-button" data-video-url="<?php echo $product_video_url; ?>" data-video-target=".product-image" data-open-text="Watch Product Video" data-close-text="X Close">Watch Product Video</a>
				<?php endif; ?>
			</div>
			</div>
		</div>
		<?php } ?>

		<?php if ($product_img_id) { ?>

		<div class="product-image align-center">
			<div class="row">
				<div class="col-sm-12">
					<picture class="display-inline-block" style="<?php echo 'max-width: ' . $product_img_1x[1] . 'px;'; ?>">
						<source srcset="<?php echo $product_img_1x[0] . ' 1x, ' . $product_img_2x[0] . ' 2x'; ?>"/>
						<img
							src="<?php echo $product_img_1x[0]; ?>"
							alt="<?php echo $image_alt; ?>"
							title="<?php echo $image_title; ?>"
							width="<?php echo $product_img_1x[1]; ?>"
							height="<?php echo $product_img_1x[2]; ?>"/>
					</picture>
				</div>
			</div>
		</div>

		<?php } else {	?>

			<div class="product-padder">
				<div class="row">
					<div class="col-sm-12 pad-t-30">
					</div>
				</div>
			</div>

		<?php } ?>

		<?php if ($include_product_info == 'yes'): ?>
		<div class="row product-main-info">

			<?php if ($product_logo): ?>
			<div class="container product-logo-container">
				<div class="row">
					<div class="col-sm-12">
						<img src="<?php echo $product_logo; ?>" alt="<?php echo $title; ?> Logo" class="product-logo" />
					</div>
				</div>
			</div>
			<?php endif ?>

			<div class="col-sm-6">
				<h3>
					<strong>
						<?php if ($product_model): ?>
							<?php echo $product_model; ?>
						<?php else: ?>
							<?php echo $title; ?>
						<?php endif ?>
					</strong>
				</h3>
			</div>
		</div>

		<hr class="margin-b-30"/>

	  <?php // Start product info ?>
		<div class="row product-sub-info">
			<div class="col-sm-6">
				<?php if ($product_overview) { ?>
				<h4>Overview</h4>
				<?php echo apply_filters( 'the_content', $product_overview); ?>
				<?php } ?>
			</div>
			<div class="col-sm-6">
				<?php if ($product_includes) { ?>
				<h4>Includes</h4>
					<ul class="paragraph-list">
					<?php for ($i = 0; $i < count($product_includes); $i ++) { ?>
						<li><?php echo $product_includes[$i]; ?></li>
					<?php } ?>
					</ul>
				<?php } ?>
			</div>
		</div>
	  <?php // End product info ?>

		<?php endif; ?>


		<?php if ($include_product_models == 'yes'): ?>
		<div class="product-models">

			<div class="row product-models-header">
				<div class="col-sm-12">
					<h3>
						<strong>
							Models
						</strong>
					</h3>
				</div>
			</div>

			<hr class="margin-b-30" />

			<div class="row product-models-content">
				<div class="col-sm-4 product-model">
					<?php if ($product_model_1_img_id) { ?>

						<picture style="<?php echo 'max-width: ' . $product_model_1_img_1x[1] . 'px;'; ?>">
							<source srcset="<?php echo $product_model_1_img_1x[0] . ' 1x, ' . $product_model_1_img_2x[0] . ' 2x'; ?>"/>
							<img
								src="<?php echo $product_model_1_img_1x[0]; ?>"
								alt="<?php echo $p1_alt; ?>"
								title="<?php echo $p1_title; ?>"
								width="<?php echo $product_model_1_img_1x[1]; ?>"
								height="<?php echo $product_model_1_img_1x[2]; ?>"/>
						</picture>

					<?php } else { ?>

						<img src="http://placehold.it/270x193/cccccc/ffffff/"/>

					<?php } ?>

					<p>
						<strong><?php echo $product_model_1_name; ?></strong>
					</p>
					<?php echo apply_filters( 'the_content', $product_model_1_content); ?>

				</div>
				<div class="col-sm-4 product-model">
					<?php if ($product_model_2_img_id) { ?>

						<picture style="<?php echo 'max-width: ' . $product_model_2_img_1x[1] . 'px;'; ?>">
							<source srcset="<?php echo $product_model_2_img_1x[0] . ' 1x, ' . $product_model_2_img_2x[0] . ' 2x'; ?>"/>
							<img
								src="<?php echo $product_model_2_img_1x[0]; ?>"
								alt="<?php echo $p2_alt; ?>"
								title="<?php echo $p2_title; ?>"
								width="<?php echo $product_model_2_img_1x[1]; ?>"
								height="<?php echo $product_model_2_img_1x[2]; ?>"/>
						</picture>

					<?php } else { ?>

						<img src="http://placehold.it/270x193/cccccc/ffffff/"/>

					<?php } ?>

					<p>
						<strong><?php echo $product_model_2_name; ?></strong>
					</p>
					<?php echo apply_filters( 'the_content', $product_model_2_content); ?>

				</div>
				<div class="col-sm-4 product-model">
					<?php if ($product_model_3_img_id) { ?>

						<picture style="<?php echo 'max-width: ' . $product_model_3_img_1x[1] . 'px;'; ?>">
							<source srcset="<?php echo $product_model_3_img_1x[0] . ' 1x, ' . $product_model_3_img_2x[0] . ' 2x'; ?>"/>
							<img
								src="<?php echo $product_model_3_img_1x[0]; ?>"
								alt="<?php echo $p3_alt; ?>"
								title="<?php echo $p3_title; ?>"
								width="<?php echo $product_model_3_img_1x[1]; ?>"
								height="<?php echo $product_model_3_img_1x[2]; ?>"/>
						</picture>

					<?php } else { ?>

						<img src="http://placehold.it/270x193/cccccc/ffffff/"/>

					<?php } ?>

					<p>
						<strong><?php echo $product_model_3_name; ?></strong>
					</p>
					<?php echo apply_filters( 'the_content', $product_model_3_content); ?>

				</div>
			</div>

		</div>

	<?php endif; ?>

	</div>
</div>

<?php if ($product_video_url): ?>
	<script type="text/javascript">
		(function( $ ) {

			var button                = $('.video-display-button'),
					url                   = '<?php echo $product_video_url; ?>',
			 		target                = $(button.data('video-target')),
					open_text             = button.data('open-text'),
					close_text            = button.data('close-text'),
					picture               = target.find('picture'),
					video_url             = '//www.youtube.com/embed/' + url,
					video_url_autoplay    = video_url + '?autoplay=1&rel=0',
					video_template        = '<iframe width="560" height="315" src="' + video_url + '" frameborder="0" allowfullscreen></iframe>',
					video_parent_template = '<div class="iframe-16-9-container"><div class="iframe-embed-video">' + video_template + '</div></div>';

			// Append video template to target
			target.append(video_parent_template);
			target.addClass('has-video hiding-video');

			// Get the iframe element after we append it to the target
			var iframe                = target.find('iframe');

			// Add button click handler and interactions
			button.on('click', function(e) {
				e.preventDefault();
				if (target.hasClass('hiding-video')) {
					target.removeClass('hiding-video');
					target.addClass('showing-video');
					button.html(close_text);
					iframe.attr('src', video_url_autoplay);
				} else {
					target.addClass('hiding-video');
					target.removeClass('showing-video');
					button.html(open_text);
					iframe.attr('src', video_url);
				}
			});

		})(jQuery);
	</script>
<?php endif; ?>
