<?php
  $link  = get_post_meta( $id, '_cmb2_faq_link', true );
  $faq_l = get_post_meta( $id, '_cmb2_left_column_faq', true );
  $faq_r = get_post_meta( $id, '_cmb2_right_column_faq', true );
?>

<?php if (in_array('faq_panel', $options)) { ?>

<div class="panel panel-tan showhides-panel faq-panel pad-tb-50">
  <div class="container">
    <div class="row heading">
      <div class="col-sm-12 margin-b-10">
        <h3><strong>Frequently Asked Questions</strong></h3>
        <hr/>
      </div>
    </div>
    <div class="row faqs">
      <div class="col col-sm-6">


        <? if (count($faq_l) > 0 && $faq_l != null) { ?>
        <ul class="show-hide show-hide-left">
        	<?php for ($i = 0; $i < count($faq_l); $i ++) { ?>

        		<?php
        			$faq_l_q = $faq_l[$i]['question'];
        			$faq_l_a = $faq_l[$i]['answer'];
        		?>
        		<li class="show-hide-container">
        			<div class="show-hide-heading">
        				<span class="subhead">
        					<?php echo $faq_l_q; ?>
        				</span>
        			</div>
        			<div class="show-hide-content" style="display: none;">
        				<div class="show-hide-actual-content">
        					<?php echo apply_filters( 'the_content', $faq_l_a); ?>
        				</div>
        			</div>
        		</li>

        	<?php } ?>
        </ul>
        <?php } ?>


      </div>
      <div class="col col-sm-6">


        <? if (count($faq_r) > 0 && $faq_r != null) { ?>
        <ul class="show-hide show-hide-right">
        	<?php for ($i = 0; $i < count($faq_r); $i ++) { ?>

        		<?php
        			$faq_r_q = $faq_r[$i]['question'];
        			$faq_r_a = $faq_r[$i]['answer'];
        		?>
        		<li class="show-hide-container">
        			<div class="show-hide-heading">
        				<span class="subhead">
        					<?php echo $faq_r_q; ?>
        				</span>
        			</div>
        			<div class="show-hide-content" style="display: none;">
        				<div class="show-hide-actual-content">
        					<?php echo apply_filters( 'the_content', $faq_r_a); ?>
        				</div>
        			</div>
        		</li>

        	<?php } ?>
        </ul>
        <?php } ?>


      </div>
    </div>
    <?php if ($link): ?>
      <div class="row footer">
        <div class="col-sm-12">
          <a class="pull-right green" href="<?php echo $link ?>">View All <i class="icon-right-small"></i></a>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>

<?php } ?>
