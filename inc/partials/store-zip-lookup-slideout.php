<?php
    $user_dealer_zip = "null";
    $user_dealer_name = "DogWatch";
    $user_dealer_url = "null";

    if (isset($_COOKIE["user_dealer_zip"])) {
        $user_dealer_zip = $_COOKIE["user_dealer_zip"];
    }
    if (isset($_COOKIE["user_dealer_name"])) {
        $user_dealer_name = $_COOKIE["user_dealer_name"];
    }
    if (isset($_COOKIE["user_dealer_url"])) {
        $user_dealer_url = $_COOKIE["user_dealer_url"];
    }
    //setCookie("user_dealer_zip", this.zip_code, 1);
    //setCookie("user_dealer_name", this.dealer.name, 1);
    //setCookie("user_dealer_url", this.dealer.url, 1);

    /*
    if we DONT HAVE a zip code, show the form
    if we HAVE a zip code and the dealer name IS NOT dogwatch, forward them to the dealer url
    if we HAVE a zip code and the dealer name IS dogwatch, don't show the form
    */
?>



<?php //if ($user_dealer_zip == "null" && $user_dealer_name != "null") : ?>

<div class="store-zip-search">
    <div class="store-zip-search__panel dark">
    <div class="store-zip-search__content">
        <div class="store-zip-search__logo">
          <a href="<?php echo esc_url( home_url() ); ?>/" class="logo-svg" title="DogWatch">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488 60"><style>.st8{fill:#fff;} .st9{fill:#fff;}</style><path class="st8" d="M438.9 10.1l1.1-.2c3.2-.3 6.1-.3 8.7-2.1.1-.1.4-.2.6-.3 4.8-1.4 10.5-1.9 14.9-4.5.8-.5 1.6-1.6 2.7-2.2 2.1-1.1 3.5-.9 5.9-.7l2.5.1c4.4 1.3 7 3.6 11.6 3.9.4.8.6 1.1 0 2.1-2.5 4.1-3.6 5.2-8.7 5-.6 1-.5 2-.7 3.2-1.8.2-2.9.3-4.3-.8h-2.6c-2.1 1.9-5.1 4.6-6.6 7.1-.8 1.3-.7 3-1.6 4.3-1.1 1.5-3.6 3.1-4.1 5-.6 2.4 1.1 6 1.6 8.4.5 2.1.6 4.4 1.8 6.3 1.7 2.5 4.8-1.3 5.2 3.5-1.4.1-2.3-.1-3.5.5h-3.1c-2.2-1.1-5.2-10.2-6.5-13-2.3 2.9-3.5 7-5.2 10.5-.5 1-1.1 2.8-.7 3.9.4 1.3 3 3.2 4.1 4.3-1.1.9-2.4 1.4-4.3.6-8.8-3.5 2.8-20.1-.3-24.5l-3 .1c-4.1-.3-8-1.6-11.5-3.4-.6 3.5-1.6 7.3-2.9 10.5-.6 1.5-1.8 2.6-1.8 4.3 0 .9-.2 1.3-.3 2.1v1.8c0 .9 1.6 6.5 2.8 6.7 1 .2 2 .1 3 .4.1 0 .2 0 .4.1.5.2.6.4.7.9.1.3.7.8.9 1.1-.9.9-2.5 1.2-4.2 1.3h-.3c-4-.1-4.2-5.3-6.1-8.2-.5-.8-2.4-2.8-2.5-3.7-.3-2.6.9-4.7.4-7.6-.3-1.5-2.2-3.1-3-4.3-2.9.5-5.2 3.6-7.6 5.5-2.9 2.2-6.4 4.9-7.9 7.8-.8 1.6-1.1 3.8-.9 5.5.2 2.2 4.3.7 3.4 4.3-1.5.2-5 .9-5.9-.5-2.1-3.2.1-11.2 1.1-14.6.1-.3 2-1.1 2.4-1.4 1-.7 1.8-2.6 2.3-3.7 2.3-5.8 1.9-12.4 4.9-17.8l-2.4-.2c-4.2-.3-19.5-.9-22.1-3.9.7-.1 1.2-.2 2.1-.3l5.5-.1h4.9c5.3.5 11.3 2 16.1-.4 2-1 4.4-2.7 6.7-3 5.6-1 10.9.3 16.3.3"/><path class="st9" d="M322.5 2.2c3.4 2.4 1.4 7.7 1.8 11.3 1.4.7 2-1 3.2-1.2 3.2-1.4 8-1.9 11.4-.5 5 1.9 7 7.7 7.5 12.8l.5 26.1c-.5 1-1.6 1.9-2.5 2.2-5 .1-11.8 2.7-14.8-2.4-.2-6.3.8-13.1-.9-18.8-.5-1-1.6-1.9-2.5-1.2-3.4 1.8-.9 8.5-1.6 12.8v8.2c-1.6 2.7-5.2 1.2-7.7 1.9-2.9-1-8.4 1.9-8.8-2.7l-.5-45.6c0-1.2.7-2.4 1.6-2.9 4.3-.5 9-.2 13.3 0M267.6 2.6c1.6 2.6-1.1 7.7 2.5 8.7 1.4.2 3.2-.5 3.9 1.2.5 3.6.2 7.5.2 11.3-1.1 2.7-5-.2-5.9 2.4 0 3.6-.4 7.7.5 11.1 1.1 1.2 3-.2 4.3 1.2l.9 1.2c-1.1 4.3 2.1 10.9-2.7 13-6.4 1-14.5 2.2-19.1-3.4-1.1-7.8-.5-16.2-.9-23.9-.9-1.2-2.5-1.4-2.7-3.1 0-4.4-1.1-9.2 1.8-11.6 4.8-1.9 7.5-5.6 11.6-8.7 1.7.3 4.2-.6 5.6.6M304.4 13.9c0 4.6.5 9.7-.2 14-3.9 1.9-11.6-1.9-12 5.1.7 5.8 7 2.7 10.7 3.6l1.4 1.7v13.3c-3.2 3.4-9.5 2.1-13.9 1.7-6.6-1.2-12.9-7.7-14.5-14.5-2.3-9.7 1.1-18.8 8.4-24.6 5-3.1 11.6-2.4 17.7-2.4 1 .2 1.9 1.2 2.4 2.1M21 22c-1.1.2-1.6 1.9-1.8 3.1-.2 2.6-.8 5.9.9 8.7 2.3.7 4.3 0 6.4-.7 1.1-.7 2-1.9 2.3-3.4.5-2.2-.2-4.3-1.4-6-1.9-1.5-4.2-1.7-6.4-1.7m20.2-11.3c5.7 5.8 7.5 14 5.9 22.7-1.4 6-4.5 12.6-10.2 15.5-8.4 6-20.2 4.3-30.7 4.8-1.8.2-4.5 0-5.2-.5-.8-.5-.8-1.1-.9-2.4C0 49.4.1 21.6.8 6.2c1.6-5.1 7-3.2 11.6-3.4 10.4.1 21.3-.6 28.8 7.9M116.8 26.8c-1.4.5-2 1.9-1.6 3.6 1.1 2.2 4.1 1.9 6.4 1.7 1.4-.5 2.7-1.2 2.7-2.9-1.2-2.6-4.8-3.1-7.5-2.4m91.1-24.3c1.1.2 1.1 1.2 1.4 2.2-1.4 10.9-4.8 20.5-7.5 30.7-1.8 5.6-3.9 11.1-6.1 16.7-1.8 2.4-5 1.2-7.5 1.5-4.3-.1-11.1 2.4-12.7-3.1-.9-2.4-.9-5.1-2-7.2l-1.1.5c-1.4 3.4-1.6 8-5 9.9-5.2.2-11.4 1.2-16.1-.7-3.4-5.1-3.9-11.6-5.9-17.1-1.4-2.9-1.8-6.5-3.2-8.7-1.4 7.2.2 15-.7 22.2-.9 4.4-4.1 8.2-8 9.7-9.8 1.7-22 2.4-30.7-2.2-2.5-1.7-1.7-5.1-1.7-7.5.2-1.4.1-3.6 1.7-4.3 6.4 1.7 12.9 5.1 20 2.9.2-.2.7-.5.7-1 0 0 .6-1.8-.7-1.5-7.8 1.5-16.6-.6-22.7-7.4-3.6-5.6-2.5-14.2.9-19.3 3.9-6.5 10.5-8.2 17.3-7 2.5.7 5 1.7 7.3 3.1 2.3-4.3 8-1.7 12-2.7l.2-.2c-.2-2.7-2.5-6.5.5-8.5 4.8-.7 10.4-.2 15.4-.2 3 1.9 2.5 6 3.6 8.9 1.2 5.5 3.2 10.2 4.3 15.5.2.7.9 1 1.6.7 1.2-1.7 2.1-5.1 2-6.5.3-2.3-3.4-11.2-3.6-17.7 1.8-2.2 5.2-1 7.7-1.2 3.4.7 8.2-1.9 10.5 1.7 1.6 7.2 2.7 15 5.2 21.7 0 .5.5.5.7.2 3-7.2 3.6-15.2 5.7-22.9.5-1 1.6-.7 2.5-1.2h14zM68.6 29.2c-.7 1-1.6 2.4-.9 3.9.5 2.2 2.7 2.4 4.5 3.1 2.5 0 5.2-.7 6.1-3.4.2-1.4-.2-2.7-1.1-3.9-2-1.8-6.8-2.3-8.6.3m24.1-8.9c3.2 5.1 4.3 13.5 1.6 19.6-3 8-9.5 13.3-17.5 14-9.1.7-18.4-.5-23.2-9.2-4.8-6-4.3-16.2-1.4-22.9 4.3-9.4 14.3-12.5 24.1-11.3 6.8.4 12.5 4.3 16.4 9.8M223.9 36.4c-.7.5-1.8.6-2.3 1.7-.1 1.2-.9 1.7.9 2.4 1.4.5 3.4.5 4.8-.2.9-.2 1.6-1.2 1.6-2.4-1.9-2.1-3.1-2.1-5-1.5M246 20.7c1.4 9.9-.2 20-.2 29.9-.5 1.2-1.4 1.9-2.5 2.2-3.9.2-8 .5-11.6-.2-.9-.7-.2-3.1-1.8-2.2-4.5 3.6-12.3 3.9-17.5 1.4-3.6-1.9-6.4-5.8-7.1-9.9-.5-5.1 1.4-9.2 4.3-12.8 5.5-3.6 12.5-4.8 19.3-3.1.2-.2 1.1-.3.8-2.1-3.9-2.4-9-1-13.3-.8-1.8.2-4.5 1.5-5.8-.4-.2-3.1-.7-6.6.2-9.4 6.4-3.9 15.5-2.2 23-2.2 5.3.7 10.1 4 12.2 9.6M360.3 10.5c1.1 0 2.1-.1 2.1-1.5 0-1.1-.9-1.3-1.8-1.3h-1.7v2.8h1.4zm-1.5 5.1h-1.3v-9h3.2c2 0 3 .8 3 2.6 0 1.6-.9 2.3-2.2 2.5l2.4 3.9h-1.4l-2.2-3.9h-1.5v3.9zm1.5 2c3.3 0 5.9-2.8 5.9-6.5s-2.6-6.5-5.9-6.5c-3.4 0-6 2.8-6 6.5.1 3.7 2.7 6.5 6 6.5m-7.4-6.5c0-4.5 3.4-7.8 7.4-7.8s7.4 3.3 7.4 7.8-3.4 7.8-7.4 7.8-7.4-3.3-7.4-7.8"/></svg>
          </a>
        </div>
        <div class="h3 store-zip-search__instructions">Please enter your delivery zip code</div>
        <div class="store-zip-search__your-dealer" style="display: none;">
            <p class="h4 store-zip-search__your-dealer__title"></p>
            <a class="store-zip-search__your-dealer__url btn orange" href="#" target="_blank">Go Now</a>
        </div>
        <form action="" class="store-zip-search__form">
            <label for="zip_code group">Zip Code</label>
            <input type="text" name="zip_code" value="" pattern="\d{5,5}(-\d{4,4})?" maxlength="5" placeholder="eg. 12345">
            <button name="submit" class="btn orange">Go to store</button>
        </form>
    </div>  
    </div>
    <div class="store-zip-search__overlay"></div>
</div>

<script type="text/javascript">
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    var zip_search = {
        parent: jQuery('.store-zip-search'),
        overlay: jQuery('.store-zip-search .store-zip-search__overlay'),
        form: jQuery('.store-zip-search form'),
        input: jQuery('.store-zip-search form input'),
        submit: jQuery('.store-zip-search form button'),
        title: jQuery('.store-zip-search__instructions'),
        dealer_info: jQuery('.store-zip-search__your-dealer'),
        dealer_info_title: jQuery('.store-zip-search__your-dealer__title'),
        dealer_info_url: jQuery('.store-zip-search__your-dealer__url'),
        zip_code: "<?php echo $user_dealer_zip; ?>",
        query_base: "<?php echo esc_url( home_url() ); ?>",
        query_url: "",
        has_dealer: false,
        dealer: {
            name: "<?php echo $user_dealer_name; ?>",
            url: "<?php echo $user_dealer_url; ?>",
        },
        messages: {
            forwarding_dealer: "Forwarding you to your local dealer:",
            forwarding_dogwatch: "Forwarding you to our store.",
        },
        init: function() {
            this.assign();
            // If we already have a URL and a name that isn't dogwatch, just forward them
            if (this.dealer.url !== null && this.dealer.name !== "DogWatch") {
                this.startEndRoutineAlreadyHaveDealerInfo();
            } else {
                this.addHandlers();
            }
            this.is_revealed(true);
        },
        assign: function() {
            this.query_url = this.query_base + "?s=";
        },
        addHandlers: function() {
            var obj = this;
            this.form.on('submit', function(event) {

                event.preventDefault();

                obj.is_disabled(true);
                obj.is_loading(true);

                obj.zip_code = obj.input.val();
                obj.query_url += obj.zip_code + "&post_type=dealer";

                if (obj.zip_code !== null && obj.zip_code !== "") {
                    jQuery.get(obj.query_url)
                        .done(function(data) { obj.searchFinished(data); })
                        .fail(function() { console.log('search failed'); });
                } else {
                    // Fire error messaging
                } 
            });
        },
        searchFinished: function(data) {

            this.is_loading(false);
            this.is_finished();

            $data = jQuery(data); // turn html data into jquery object
            var dealer = $data.find('.single-dealer');
            var dealer_title = jQuery.trim(dealer.find('.entry-title').text());

            // If the dealer text does not container "Located" (TODO: make this more precise)
            if (dealer !== null && dealer_title.indexOf("Located") < 1) {

                this.dealer.name = dealer_title; // remove whitespace
                this.dealer.url = dealer.find('.dealer-info__url a').attr('href');
                console.log('Your dealer is: ' + this.dealer.name + ' with the url: ' + this.dealer.url);
                this.has_dealer = true;
                

            } else {
                console.log('We didn\'t find your dealer');
                // Forward the user to dogwatch store
                // this.is_revealed(false);
            }

            this.startEndRoutine();
        },
        // Save Zip and forward to dealer url
        setCookies: function() {
            // If we never set the dealer name to anythign else, set the zip to all zeroes 
            if (this.dealer.name === "DogWatch") {
                setCookie("user_dealer_zip", "000000", 1);
            } else {
                setCookie("user_dealer_zip", this.zip_code, 1);
            }
            
            setCookie("user_dealer_name", this.dealer.name, 1);
            setCookie("user_dealer_url", this.dealer.url, 1);
        },
        startEndRoutine: function() {
            var obj = this;
            console.log("starting end routine");

            // Set our cookies since we are ending the routine
            this.setCookies();

            // Update form text if we are just passing them to dw store
            if (obj.has_dealer === false) {
                obj.title.text(obj.messages.forwarding_dogwatch);
            }

            // If we have a dealer's info for the user set them down the dealer routine
            // Otherwise, send them through the non-dealer routine
            setTimeout(
                function() {
                    obj.is_forwarding(obj.has_dealer);
                }, 1000
            );
        },
        startEndRoutineAlreadyHaveDealerInfo: function() {
            var obj = this;
            console.log("starting end routine with existing dealer info");

            //obj.title.text(obj.messages.forwarding_dogwatch);

            // If we have a dealer's info for the user set them down the dealer routine
            // Otherwise, send them through the non-dealer routine

            setTimeout(
                function() {
                    //obj.is_forwarding(true);
                    obj.form.hide();
                    obj.forwardUser();
                }, 1000
            );
        },
        // Show the form
        is_revealed: function(setting) {
            console.log("is revealed: " + setting);
            var revealed_class = "is-active";

            if (setting===true) {
                this.parent.addClass(revealed_class);  
                this.input.focus();
            } else {
                this.parent.removeClass(revealed_class);  
            }

        },
        // Let the user know we are loading
        // NOTE: split disabled into another function so we can call it elsewhere maybe
        is_loading: function(setting) {
            var loading_class = "is-loading";
            if (setting === true) {
                this.parent.addClass(loading_class);
            } else {
                this.parent.removeClass(loading_class);
            }
        },
        is_disabled: function(setting) {
            if (setting === true) {
                this.submit.prop("disabled", true);
                this.input.prop("disabled", true);
            } else {
                this.submit.prop("disabled", false);
                this.input.prop("disabled", false);
            }
        },
        // Show the checkmark, wait 1s and start forwarding
        is_finished: function() {
            var finished_class = "is-finished";
            this.parent.addClass(finished_class);
            var obj = this;
        },
        // Let the user know you are forwarding them, if true, forward them, if false hide the form
        is_forwarding: function(setting) {
            let obj = this;
            let stagger = 0;
            let stagger_incriment = 100;
            stagger_array = [this.submit, this.input, this.form.find('label')];

            for (let index = 0; index < stagger_array.length; index++) {
                setTimeout(
                    function() { 
                        jQuery(stagger_array[index]).addClass("is-removed");
                    }, stagger
                );
                stagger += stagger_incriment;
                // When loop is finished forward the user
                if (index === stagger_array.length - 1) {
                    // If we are forwarding the user, do that, otherwise, hide the modal and show the DW store
                    if (setting === true) {
                        this.forwardUser();
                    } else {
                        setTimeout(
                            function() { 
                                obj.is_revealed(false);
                            }, 1500
                        );
                    }
                }
            }
        },
        forwardUser: function() {
            console.log("forwarding user to : " + this.dealer.url);
            var obj = this;
            let stagger = 0;
            let stagger_incriment = 100;
            var stagger_array = [
                "show_title",
                "update_info",
                "fade_in"
            ];

            for (let index = 0; index < stagger_array.length; index++) {
                setTimeout(
                    function() { 
                        // Update title messaging
                        if (stagger_array[index] === "show_title") {
                            obj.title.text(obj.messages.forwarding_dealer);
                        }
                        // Update dealer particulars
                        if (stagger_array[index] === "update_info") {
                            obj.dealer_info_title.text(obj.dealer.name);
                            obj.dealer_info_url.attr('href', obj.dealer.url);
                        }
                        // Fade in dealer info box
                        if (stagger_array[index] === "fade_in") {
                            obj.dealer_info.fadeIn();
                        }
                    }, stagger
                );
                stagger += stagger_incriment;
                // When we reach the end of the array, just forward the user
                if (index === stagger_array.length - 1) {
                    obj.goToDealer();
                }
            }
        },
        goToDealer: function() {
            var obj = this;
            console.log("forwarding to: " + obj.dealer.url);

            setTimeout(
                function() {
                    // this forwards the user
                    window.location.href = obj.dealer.url;
                }, 5000
            );
        },
    }
    if (zip_search.zip_code !== "000000") {
        zip_search.init();
    }
    
</script>

<?php // endif; ?>