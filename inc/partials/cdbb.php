<?php
$cdbb_url = get_option('general_setting_cdbb_link');
?>

<?php if ($cdbb_url != ""): ?>
<div class="panel panel-white cdbb-panel low-res">
  <div class="container align-center pad-t-30">
    <a href="<?php echo $cdbb_url;?>" target="_blank" class="bb-seal">Consumer's Digest Best Buy</a>
  </div>
</div>
<?php endif; ?>
