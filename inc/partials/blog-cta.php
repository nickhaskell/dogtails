<?php if ($cta_headline): ?>
<div class="blog-cta panel panel-grey dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 blog-cta__content">
        <?php if ($cta_headline): ?>
          <h2 class="blog-cta__content__headline color-white">
            <strong><?php echo $cta_headline; ?></strong>
          </h2>
        <?php endif; ?>
        <?php if ($cta_button_text): ?>
          <a href="<?php echo $cta_button_url; ?>" class="blog-cta__content__button btn another-green"><?php echo $cta_button_text; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
