<?php
  // This is the partial for Dealer Locations one level below the highest, eg. Masachusetts

  $child_slug      = $post->post_name;
  $child_title     = $post->post_title;
  $child_id        = $post->ID;
?>

<a href="<?php the_permalink(); ?>"><?php echo $child_title; ?></a>
