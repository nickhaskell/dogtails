<?php
  $image_nav_headline = get_post_meta( $id, '_cmb2_image_nav_headline', true );;
  $image_nav_subhead = get_post_meta( $id, '_cmb2_image_nav_subhead', true );;
  $image_nav_images = get_post_meta( $id, '_cmb2_image_nav_images', true );
  $image_nav_links_title = get_post_meta( $id, '_cmb2_image_links_title', true );
?>

<?php if (in_array('image_nav_panel', $options)) { ?>

<div class="panel panel-green image-nav-panel dark">
  <div class="container pad-t-50 pad-b-70">

		<div class="row">
			<div class="col-sm-12 image-nav-header margin-b-30 align-center">
				<div class="headline margin-b-10"><?php echo '<h2>' . $image_nav_headline . '</h2>'; ?></div>
				<h4 class="subhead"><?php echo $image_nav_subhead; ?></h4>
			</div>
		</div>

    <div class="row">

        <div class="col-l col-sm-8 image-nav-images">

          <?php for ($i = 0; $i < count($image_nav_images); $i ++) { ?>

            <?php
              $ini_image_id = $image_nav_images[$i]['image_id'];
              $ini_image_1x = wp_get_attachment_image_src( $ini_image_id, 'image_nav_image' );
              $ini_image_2x = wp_get_attachment_image_src( $ini_image_id, 'full_size' );
              $ini_title = $image_nav_images[$i]['title'];
            ?>

            <div class="image" data-image="image-<?php echo $i; ?>">
              <picture>
                <source srcset="<?php echo $ini_image_1x[0] . ' 1x, ' . $ini_image_2x[0] . ' 2x'; ?>"/>
                <img src="<?php echo $ini_image_1x[0]; ?>" alt="<?php echo $ini_title; ?> Image"/>
              </picture>
            </div>

          <?php } ?>

        </div>
        <div class="col-r col-sm-4 image-nav-links">

          <div class="image-links-container">
            <div class="image-links-title">
              <?php if ($image_nav_links_title): ?>
                <h3><?php echo $image_nav_links_title; ?></h3>
              <?php else: ?>
                <h3>Examples</h3>
              <?php endif; ?>
              <hr/>
            </div>

          	<?php for ($i = 0; $i < count($image_nav_images); $i ++) { ?>

          		<?php
          			$ini_title = $image_nav_images[$i]['title'];
          		?>

          			<div class="image-link" data-image="image-<?php echo $i; ?>">
          				<span class="subhead">
          					<?php echo $ini_title ?>
          				</span>
          			</div>


          	<?php } ?>

          </div>
        </div>


    </div>
  </div>
</div>

<script type="text/javascript">
  (function( $ ) {
    images_container = $('.image-nav-images');
    links_container  = $('.image-nav-links');
    in_container     = $('.image-nav-panel');

    images_container.find('.image').first().addClass('active');
    links_container.find('.image-link').first().addClass('active');

    $('.image-link').on('click', function() {

      link = $(this);
      image_data = link.data('image');
      image_pair = images_container.find('.image[data-image="' + image_data + '"]');

      $('.image-link').each(function() {
        $(this).removeClass('active');
      });

      images_container.find('.image').each(function() {
        $(this).removeClass('active');
      });

      link.toggleClass('active');
      image_pair.toggleClass('active');

      // If this is on a phone-sized viewport, move to the the top of the panel
      if (bp_phone === true) {
        $('html, body').animate({
          scrollTop: images_container.offset().top - 80
        }, SCROLL_SPEED);
      }
    });
  })(jQuery);
</script>
<?php } ?>
