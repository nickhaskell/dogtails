<?php
/**
 * Archive header
 *
 * @package Shikoku_Inu
 */
?>
<div class="entry-header container align-center  <?php if (!$subhead) { echo 'no-subhead'; } ?>">
	<h1 class="entry-title">
		<?php
			// Formatting based on the Underscores theme
			// https://github.com/Automattic/_s/blob/master/archive.php
			if ( is_category() ) :
				echo "Category: ";
				single_cat_title();

			elseif ( is_tag() ) :
				echo "Tag: ";
				single_tag_title();

			elseif ( is_author() ) :
				printf( __( 'Author: %s', 'shikoku-inu' ), '<span class="vcard">' . get_the_author() . '</span>' );

			elseif ( is_day() ) :
				printf( __( 'Day: %s', 'shikoku-inu' ), '<span>' . get_the_date() . '</span>' );

			elseif ( is_month() ) :
				printf( __( 'Month: %s', 'shikoku-inu' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'shikoku-inu' ) ) . '</span>' );

			elseif ( is_year() ) :
				printf( __( 'Year: %s', '_s' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'shikoku-inu' ) ) . '</span>' );
			else :
				_e( 'Archives', 'shikoku-inu' );

			endif;
		?>
	</h1>
</div><!-- .entry-header -->
