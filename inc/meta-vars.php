<?php

	global $post;

	$slug      = $post->post_name;
	$title     = $post->post_title;
	$id        = $post->ID;
	$post_type = get_post_type($id);

	$parent    = get_post($post->post_parent);

	$options   = get_post_meta( $id, '_cmb2_options', true );

	$headline_alt         = get_post_meta( $id, '_cmb2_headline_alt', true );
	$headline_button_link = get_post_meta( $id, '_cmb2_headline_button_link', true );
	$headline_button_text = get_post_meta( $id, '_cmb2_headline_button_text', true );
	$headline_button_target = get_post_meta( $id, '_cmb2_headline_button_new_window', true);
	$headline_sub         = get_post_meta( $id, '_cmb2_headline_sub', true );
	$headline_optional    = get_post_meta( $id, '_cmb2_headline_optional', true );
	$page_scripts         = get_post_meta( $id, '_cmb2_page_scripts', true );

	$template = basename(get_page_template());

	//$second_column      = get_post_meta( $id, '_cmb2_second_column', true );
	//$subhead            = get_post_meta( $id, '_cmb2_headline', true );
	//$columns            = get_post_meta( $id, '_cmb2_columns', true );
	//$header_button      = get_post_meta( $id, '_cmb2_header_button', true );
	//$header_button_text = get_post_meta( $id, '_cmb2_header_button_text', true );

	/*
	if ( is_page('battery-plan-sign-up') )
	{
		if($_SERVER['SERVER_PORT'] != '443')
		{
			header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		}
	}
	else
	{
		if($_SERVER['SERVER_PORT'] == '443')
		{
			header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		}
	}
	*/

?>
