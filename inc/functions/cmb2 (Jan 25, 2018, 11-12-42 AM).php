<?php

// Get shop posts
function cmb2_get_post_options( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'post',
        'numberposts' => 10,
        'orderby'     => 'title',
        'order'       => 'ASC',
		'meta_key'    => '_wp_page_template',
		'meta_value'  => 'page-product.php'
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }
    return $post_options;
}

// Get posts
function cmb2_get_all_pages( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'post',
        'numberposts' => 10,
        'orderby'     => 'title',
        'order'       => 'ASC',
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }
    return $post_options;
}

function cmb2_is_front( $field ) {
	// Don't show this field if not in the cats category
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// check for a template type
	if ($template_file == 'page-home.php') {
		return true;
	} else {
		return false;
	}
}

function cmb2_is_default( $field ) {
	// Don't show this field if not in the cats category
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// check for a template type
	if ($template_file == 'default') {
		return true;
	} else {
		return false;
	}
}

function cmb2_is_front_page( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/* CUSTOM FIELD INCLUDES **************************************************** */

function cmb2_render_address_field_callback( $field_object, $value, $object_id, $object_type, $field_type_object ) {

    // make sure we specify each part of the value we need.
    $value = wp_parse_args( $value, array(
        'row-title' => '',
        'row-description' => '',
    ) );

    ?>
    <div><p><label for="<?php echo $field_type_object->_id( '_row_title' ); ?>">Row Title</label></p>
        <?php echo $field_type_object->input( array(
            'name'  => $field_type_object->_name( '[row-title]' ),
            'id'    => $field_type_object->_id( '_row_title' ),
            'value' => $value['row-title'],
        ) ); ?>
    </div>
    <div><p><label for="<?php echo $field_type_object->_id( '_row_description' ); ?>'">Row Description</label></p>
        <?php echo $field_type_object->input( array(
            'class' => 'test',
            'name'  => $field_type_object->_name( '[row-description]' ),
            'id'    => $field_type_object->_id( '_row_description' ),
            'value' => $value['row-description'],
            'type'  => 'textarea',
        ) ); ?>
    </div>
    <?php

    echo $field_type_object->_desc( true );

}
add_filter( 'cmb2_render_address', 'cmb2_render_address_field_callback', 10, 5 );



/* ************************************************************************** */




 // Custom Metaboxes
add_filter( 'cmb2_meta_boxes', 'cmb2_mbs' );

function cmb2_mbs( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb2_';

  $tinySmall['toolbar1'] = 'bold, italic, bullist, numlist, link, unlink, hr, formatselect';
  $tinySmall['toolbar2'] = '';

  $tinySmall2['toolbar1'] = 'bold, italic, bullist, numlist, link, unlink, alignleft, aligncenter, hr, formatselect';
  $tinySmall2['toolbar2'] = '';

  $photoWithText['toolbar1'] = 'bold, italic, link, unlink,  alignleft, aligncenter, hr, formatselect';
  $photoWithText['toolbar2'] = '';



  // Default template options?

  $meta_boxes['tempate_options'] = array(
		'id'         => 'template_options',
		'title'      => __( 'Template Options', 'cmb2' ),
		'object_types'      => array( 'page', ), // Post type
		'context'    => 'side', //  'normal', 'advanced', or 'side'
		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
		'show_names' => false, // Show field names on the left
    // 'show_on_cb' => 'cmb2_is_default',
    'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-customer-store.php')),
		'fields'     => array(
      array(
          'description' => 'How many main content columns you need',
          'type' => 'title',
          'id' => $prefix . 'main_content_columns_title',
          'row_classes' => 'no-border clear-pad',
      ),
      array(
          'name'    => __( 'Columns', 'cmb2' ),
          'id'      => $prefix . 'main_content_columns',
          'type'    => 'select',
          'options' => array(
            'one' => __( 'One', 'cmb2' ),
            'one-fw' => __( 'One Full Width', 'cmb2' ),
            'three-q' => __( 'Three Quarter Width', 'cmb2' ),
            'two'   => __( 'Two', 'cmb2' ),
        ),
        'default' => 'two',
      ),
      array(
          'description' => 'Selecting an option below will add fields for that specific content to this page.',
          'type' => 'title',
          'id' => $prefix . 'panel_options_title',
          'row_classes' => 'no-border clear-pad',
      ),
        array(
        		'name'    => __( 'Options', 'cmb2' ),
        		'id'      => $prefix . 'options',
        		'type'    => 'multicheck',
        		'options' => array(
        			'hero' => __( 'Hero', 'cmb2' ),
              'show_hide_panel' => __( 'Show/Hide Content', 'cmb2' ),
        			'video_panel' => __( 'Video Content', 'cmb2' ),
        			'featured_panel' => __( 'Featured Panel', 'cmb2' ),
        			'text_panel' => __( 'Text Panel', 'cmb2' ),
              'product_hero_new' => __( 'Product Hero (New)', 'cmb2' ),
              'trustbox_panel' => __( 'Trustbox Panel', 'cmb2' ),
              'slider_panel_new' => __( 'Slider (New)', 'cmb2' ),
              'page_cta' => __( 'Page CTA', 'cmb2' ),
        			'info_table' => __( 'Info Table', 'cmb2' ),
              'photo_text_panel' => __( 'Photo with Text Panel', 'cmb2'),
              'other_video_panel' => __( 'Video Panel', 'cmb2'),
              'faq_panel' => __('FAQ Panel', 'cmb2'),
              'comparison_panel' => __( 'Comparison Panel', 'cmb2'),
              'find_a_dealer_form_panel' => __('Find a Dealer Form Panel', 'cmb2'),
        			'footer_image' => __( 'Footer Image', 'cmb2' ),
        			'customer_story' => __( 'Customer Story', 'cmb2' ),
        		),
        	),
  			),
  		);

      // Home Template Options

      $meta_boxes['home_tempate_options'] = array(
        'id'         => 'home_template_options',
        'title'      => __( 'Template Options', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'context'    => 'side', //  'normal', 'advanced', or 'side'
        'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        'show_names' => false, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-home.php')),
        'fields'     => array(
          array(
              'description' => 'Selecting an option below will add fields for that specific content to this page.',
              'type' => 'title',
              'id' => $prefix . 'test_title',
              'row_classes' => 'no-border clear-pad',
          ),
            array(
                'name'    => __( 'Options', 'cmb2' ),
                'id'      => $prefix . 'options',
                'type'    => 'multicheck',
                'options' => array(
                  'hero' => __( 'Hero', 'cmb2' ),
                  'home_features_panel' => __( 'Features Panel', 'cmb2' ),
                  'dealer_difference' => __( 'Dealer Difference Panel', 'cmb2' ),
                  'smartfence_ribbon' => __( 'Smartfence Ribbon', 'cmb2' ),
                  'ribbon_panel' => __( 'Ribbon Panel', 'cmb2' ),
                  'product_hero_new' => __( 'Product Hero (New)', 'cmb2' ),
                  'trustbox_panel' => __( 'Trustbox Panel', 'cmb2' ),
                  'home_multicolumn_panel' => __( 'Multi Column Panel', 'cmb2' ),
                  'featured_panel' => __( 'Featured Panel', 'cmb2' ),
                  'other_video_panel' => __( 'Video Panel', 'cmb2'),
                  'customer_story' => __( 'Customer Story', 'cmb2'),
                  'comparison_panel' => __( 'Comparison Panel', 'cmb2'),
                  'footer_image' => __( 'Footer Image', 'cmb2' ),
                ),
              ),
            ),
          );

      // Child List Template Options

      $meta_boxes['child_list_template_options'] = array(
        'id'         => 'child_list_template_options',
        'title'      => __( 'Template Options', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'context'    => 'side', //  'normal', 'advanced', or 'side'
        'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        'show_names' => false, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-child-list.php')),
        'fields'     => array(
          array(
              'description' => 'How many main content columns you need',
              'type' => 'title',
              'id' => $prefix . 'main_content_columns_title',
              'row_classes' => 'no-border clear-pad',
          ),
          array(
              'name'    => __( 'Columns', 'cmb2' ),
              'id'      => $prefix . 'main_content_columns',
              'type'    => 'select',
              'options' => array(
                'one' => __( 'One', 'cmb2' ),
                'two'   => __( 'Two', 'cmb2' ),
            ),
            'default' => 'two',
          ),
          array(
              'description' => 'Selecting an option below will add fields for that specific content to this page.',
              'type' => 'title',
              'id' => $prefix . 'test_title',
              'row_classes' => 'no-border clear-pad',
          ),
            array(
                'name'    => __( 'Options', 'cmb2' ),
                'id'      => $prefix . 'options',
                'type'    => 'multicheck',
                'options' => array(
                  'hero' => __( 'Hero', 'cmb2' ),
                  'featured_panel' => __( 'Featured Panel', 'cmb2' ),
                  'photo_text_panel' => __( 'Photo with Text Panel', 'cmb2'),
                  'footer_image' => __( 'Footer Image', 'cmb2' ),
                ),
              ),
            ),
          );

      // Fork Template Options

      $meta_boxes['fork_template_options'] = array(
        'id'         => 'fork_template_options',
        'title'      => __( 'Template Options', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'context'    => 'side', //  'normal', 'advanced', or 'side'
        'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        'show_names' => false, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-fork.php', 'page-training-products.php')),
        'fields'     => array(
          array(
              'description' => 'Selecting an option below will add fields for that specific content to this page.',
              'type' => 'title',
              'id' => $prefix . 'test_title',
              'row_classes' => 'no-border clear-pad',
          ),
            array(
                'name'    => __( 'Options', 'cmb2' ),
                'id'      => $prefix . 'options',
                'type'    => 'multicheck',
                'options' => array(
                  'hero' => __( 'Hero', 'cmb2' ),
                  'featured_panel' => __( 'Featured Panel', 'cmb2' ),
                  'photo_text_panel' => __( 'Photo with Text Panel', 'cmb2'),
                  'trustbox_panel' => __( 'Trustbox Panel', 'cmb2' ),
                  'footer_image' => __( 'Footer Image', 'cmb2' ),
                ),
              ),
            ),
          );

      // Product template options?

          $meta_boxes['product_tempate_options'] = array(
        		'id'         => 'product_template_options',
        		'title'      => __( 'Template Options', 'cmb2' ),
        		'object_types'      => array( 'page', ), // Post type
        		'context'    => 'side', //  'normal', 'advanced', or 'side'
        		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        		'show_names' => false, // Show field names on the left
            'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product.php')),
        		'fields'     => array(
              array(
                  'description' => 'Selecting an option below will add fields for that specific content to this page.',
                  'type' => 'title',
                  'id' => $prefix . 'panel_options_title',
                  'row_classes' => 'no-border clear-pad',
              ),
                array(
                		'name'    => __( 'Options', 'cmb2' ),
                		'id'      => $prefix . 'options',
                		'type'    => 'multicheck',
                		'options' => array(
                			'hero' => __( 'Hero', 'cmb2' ),
                			'ribbon_panel' => __( 'Ribbon Panel', 'cmb2' ),
                			'featured_panel' => __( 'Featured Panel', 'cmb2' ),
                			'text_panel' => __( 'Text Panel', 'cmb2' ),
                			'info_table' => __( 'Info Table', 'cmb2' ),
                			'cta_panel' => __( 'CTA Panel', 'cmb2' ),
                      'image_nav_panel' => __( 'Image Nav Panel', 'cmb2' ),
                      'photo_text_panel' => __( 'Photo with Text Panel', 'cmb2'),
                      'other_video_panel' => __( 'Video Panel', 'cmb2'),
                      'faq_panel' => __('FAQ Panel', 'cmb2'),
                			'footer_image' => __( 'Footer Image', 'cmb2' ),
                		),
                	),
          			),
          		);

        // Dealer area template options?

        $meta_boxes['dealerarea_tempate_options'] = array(
      		'id'         => 'dealerarea_template_options',
      		'title'      => __( 'Template Options', 'cmb2' ),
      		'object_types'      => array( 'page', ), // Post type
      		'context'    => 'side', //  'normal', 'advanced', or 'side'
      		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
      		'show_names' => false, // Show field names on the left
          // 'show_on_cb' => 'cmb2_is_default',
          'show_on'    => array( 'key' => 'page-template', 'value' => array('page-dealerarea.php')),
      		'fields'     => array(
            array(
                'description' => 'How many main content columns you need',
                'type' => 'title',
                'id' => $prefix . 'main_content_columns_title',
                'row_classes' => 'no-border clear-pad',
            ),
            array(
                'name'    => __( 'Columns', 'cmb2' ),
                'id'      => $prefix . 'main_content_columns',
                'type'    => 'select',
                'options' => array(
                  'one' => __( 'One', 'cmb2' ),
                  'two'   => __( 'Two', 'cmb2' ),
              ),
              'default' => 'two',
            ),
      			),
      		);

          // Default template options?

          $meta_boxes['store_tempate_options'] = array(
        		'id'         => 'store_template_options',
        		'title'      => __( 'Template Options', 'cmb2' ),
        		'object_types'      => array( 'page', ), // Post type
        		'context'    => 'side', //  'normal', 'advanced', or 'side'
        		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        		'show_names' => false, // Show field names on the left
            // 'show_on_cb' => 'cmb2_is_default',
            'show_on'    => array( 'key' => 'page-template', 'value' => array('page-store.php')),
        		'fields'     => array(
              array(
                  'description' => 'How many main content columns you need',
                  'type' => 'title',
                  'id' => $prefix . 'main_content_columns_title',
                  'row_classes' => 'no-border clear-pad',
              ),
              array(
                  'name'    => __( 'Columns', 'cmb2' ),
                  'id'      => $prefix . 'main_content_columns',
                  'type'    => 'select',
                  'options' => array(
                    'one' => __( 'One', 'cmb2' ),
                    'one-fw' => __( 'One Full Width', 'cmb2' ),
                    'two'   => __( 'Two', 'cmb2' ),
                ),
                'default' => 'two',
              ),
              array(
                  'description' => 'Checking the box below will hide the sidebar on the store page template',
                  'type' => 'title',
                  'id' => $prefix . 'panel_options_title_sidebar',
                  'row_classes' => 'no-border clear-pad',
              ),
              array(
                'name' => 'Hide sidebar?',
                'id'   => $prefix . 'hide_store_sidebar',
                'type' => 'checkbox'
              ),
              array(
                  'description' => 'Selecting an option below will add fields for that specific content to this page.',
                  'type' => 'title',
                  'id' => $prefix . 'panel_options_title',
                  'row_classes' => 'no-border clear-pad',
              ),
              array(
              		'name'    => __( 'Options', 'cmb2' ),
              		'id'      => $prefix . 'options',
              		'type'    => 'multicheck',
              		'options' => array(
              			'footer_image' => __( 'Footer Image', 'cmb2' ),
              		),
              	),
        			),
        		);

// Configs

  $meta_boxes['display_options'] = array(
    'id'         => 'display_options',
    'title'      => __( 'Display Options', 'cmb2' ),
    'object_types'      => array( 'page','post' ), // Post type
    'context'    => 'side', //  'normal', 'advanced', or 'side'
    'priority'   => 'core', //  'high', 'core', 'default' or 'low'
    'show_names' => false, // Show field names on the left
    'show_names' => true, // Show field names on the left
    'closed'       => true,
    'fields'     => array(
      array(
        'name' => __( 'Alternate Headline', 'cmb2' ),
        'id'   => $prefix . 'headline_alt',
        'type' => 'text',
        'row_classes' => 'no-border',
        ),
      array(
        'name' => __( 'Optional Headline', 'cmb2' ),
        'id'   => $prefix . 'headline_optional',
        'type' => 'text',
        'escape_cb' => false,
        'sanitization_cb' => false,
        'row_classes' => 'no-border',
        ),
        array(
					'name' => __( 'Sub Headline', 'cmb2' ),
					'id'   => $prefix . 'headline_sub',
					'type' => 'textarea_small',
          'escape_cb' => false,
          'sanitization_cb' => false,
          'row_classes' => 'no-border',
				),
        array(
					'name' => __( 'Headline Button Link', 'cmb2' ),
					'description' => 'Include a button link under the page headline',
					'id'   => $prefix . 'headline_button_link',
					'type' => 'text',
          'row_classes' => 'no-border',
				),
				array(
					'name' => __( 'Header Button Link Text', 'cmb2' ),
					'description' => 'Text on the header button',
					'id'   => $prefix . 'headline_button_text',
					'type' => 'text',
          'row_classes' => 'no-border',
				),
        array(
          'name' => 'Open button link in new window?',
          'id'   => $prefix . 'headline_button_new_window',
          'type' => 'checkbox'
        ),
        array(
					'name' => __( 'Child List Title', 'cmb2' ),
					'desc' => __( 'Replaces page title on child list template pages', 'cmb2' ),
					'id'   => $prefix . 'page_menu_title',
					'type' => 'text',
          'row_classes' => 'no-border',
				),
        array(
					'name'    => __( 'Hide from Child List Templates', 'cmb2' ),
					'id'      => $prefix . 'hide_from_child_list',
					'type'    => 'radio_inline',
					'options' => array(
						'yes' => __( 'Yes', 'cmb2' ),
						'no'  => __( 'No', 'cmb2' ),
					),
					'default' => 'no',
          'row_classes' => 'no-border',
				),
        array(
					'name'    => __( 'Include Dealer Area Nav', 'cmb2' ),
					'id'      => $prefix . 'dealer_area_nav',
					'type'    => 'radio_inline',
					'options' => array(
						'yes' => __( 'Yes', 'cmb2' ),
						'no'  => __( 'No', 'cmb2' ),
					),
					'default' => 'no',
          'row_classes' => 'no-border',
				),
        array(
          'name' => 'Hide page header?',
          'id'   => $prefix . 'hide_page_header',
          'type' => 'checkbox'
        ),
      ),
    );


// Page specific javascripts

$meta_boxes['Page Specific Scripts'] = array(
  'id'         => 'page_specific_scripts',
  'title'      => __( 'Page Specific Scripts', 'cmb2' ),
  'object_types'      => array( 'page','post' ), // Post type
  'context'    => 'side', //  'normal', 'advanced', or 'side'
  'priority'   => 'low', //  'high', 'core', 'default' or 'low'
  'show_names' => false, // Show field names on the left
  'closed'       => true,
  'fields'     => array(
    array(
        'description' => 'Place page-specific scripts such as conversion codes in the box below. Please include opening and closing &lt;script&gt; tags before and after each piece of code.',
        'type' => 'title',
        'id' => $prefix . 'page_scripts_title',
        'row_classes' => 'no-border clear-pad',
    ),
      array(
        'name' => __( 'Scripts', 'cmb2' ),
        'id'   => $prefix . 'page_scripts',
        'type' => 'textarea',
        'escape_cb' => false,
        'sanitization_cb' => false,
        'row_classes' => 'no-border',
      )
    ),
  );


    $meta_boxes['main_content_area'] = array(
      'id'         => 'main_content_area',
      'title'      => __( 'Main Content Area', 'cmb2' ),
      'object_types'      => array( 'page', ), // Post type
      'context'    => 'normal', //  'normal', 'advanced', or 'side'
      'priority'   => 'high', //  'high', 'core', 'default' or 'low'
      'show_names' => false, // Show field names on the left
      'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-dealerarea.php', 'page-store.php','page-customer-store.php')),
      'fields'     => array(
          array(
            'name' => __( 'Left Content', 'cmb2' ),
            'id'   => $prefix . 'two_col_left_content',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
                'media_buttons' => true,
                'textarea_rows' => 12,
              ),
            'before_row' => '<div id="main_content_two" class="container"><div class="row group">',
            'after_row' => '',
            'row_classes' => 'admin-col col-sm-6',
          ),
          array(
            'name' => __( 'Right Content', 'cmb2' ),
            'id'   => $prefix . 'two_col_right_content',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
                'media_buttons' => true,
                'textarea_rows' => 12,
              ),
            'before_row' => '',
            'after_row' => '</div></div>',
            'row_classes' => 'admin-col col-sm-6',
          ),
          array(
            'name' => __( 'Main Content', 'cmb2' ),
            'id'   => $prefix . 'one_col_content',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
                'media_buttons' => true,
                'textarea_rows' => 12,
              ),
            'before_row' => '<div id="main_content_one">',
            'after_row' => '</div>',
            'row_classes' => '',
          ),
        ),
      );


    // Product Content

    $meta_boxes['product_specific_content'] = array(
  		'id'         => 'product_specific_content',
  		'title'      => __( 'Product Content', 'cmb2' ),
  		'object_types'      => array( 'page', ), // Post type
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
  		'show_names' => true, // Show field names on the left
  		//'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
  		'show_on'    => array( 'key' => 'page-template', 'value' => 'page-product.php'),

  		'fields'     => array(
        array(
          'name' => __( 'Product Headline', 'cmb2' ),
          'id'   => $prefix . 'product_headline',
          'type' => 'text',
          'escape_cb' => false,
          'sanitization_cb' => false,
          'row_classes' => 'no-border',
        ),
        array(
          'name' => __( 'Product Sub Headline', 'cmb2' ),
          'id'   => $prefix . 'product_sub_headline',
          'type' => 'textarea_small',
          'escape_cb' => false,
          'sanitization_cb' => false,
        ),
  				array(
  					'name' => __( 'Product Image', 'cmb2' ),
  					'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
  					'id'   => $prefix . 'product_image',
  					'type' => 'file',
  					'allow' => array( 'url', 'attachment' ),
  				),
          array(
              'name'    => __( 'Include Product Info', 'cmb2' ),
              'id'      => $prefix . 'include_product_info',
              'type'    => 'select',
              'options' => array(
                'yes' => __( 'Yes', 'cmb2' ),
                'no'   => __( 'No', 'cmb2' ),
            ),
            'default' => 'no',
            'row_classes' => 'no-border',
          ),
  				array(
  					'name' => __( 'Product Logo', 'cmb2' ),
  					'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
  					'id'   => $prefix . 'product_logo',
  					'type' => 'file',
  					'allow' => array( 'url', 'attachment' ),
  				),
  				array(
  					'name' => __( 'Product Model', 'cmb2' ),
  					'desc' => __( 'Replaces product title under product image', 'cmb2' ),
  					'id'   => $prefix . 'product_model_number',
  					'type' => 'text',
  				),
          array(
            'name' => __( 'Product Overview', 'cmb2' ),
            'id'   => $prefix . 'product_overview',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
              'media_buttons' => false,
              'textarea_rows' => 3,
              'tinymce' => $tinySmall,
              ),
          ),
  				array(
  					'name' => __( 'Includes', 'cmb2' ),
  					'desc' => __( 'Each field is one list item.', 'cmb2' ),
  					'id'   => $prefix . 'product_includes',
  					'type' => 'text',
  					'repeatable' => true,
  				),
          array(
              'name'    => __( 'Include Product Models', 'cmb2' ),
              'id'      => $prefix . 'include_product_models',
              'type'    => 'select',
              'options' => array(
                'yes' => __( 'Yes', 'cmb2' ),
                'no'   => __( 'No', 'cmb2' ),
            ),
            'default' => 'no',
            'row_classes' => 'no-border',
          ),


          array(
  					'name' => __( 'Product Model 1 Name', 'cmb2' ),
  					'id'   => $prefix . 'product_model_1_name',
  					'type' => 'text',
            'row_classes' => 'no-border',
  				),
          array(
  					'name' => __( 'Product Model 1 Image', 'cmb2' ),
  					'id'   => $prefix . 'product_model_1_image',
  					'type' => 'file',
  					'allow' => array( 'url', 'attachment' ),
            'row_classes' => 'no-border',
  				),
          array(
            'name' => __( 'Product Model 1 Content', 'cmb2' ),
            'id'   => $prefix . 'product_model_1_content',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
                'media_buttons' => false,
                'textarea_rows' => 3,
                'tinymce' => $tinySmall,
              ),
          ),
          array(
  					'name' => __( 'Product Model 2 Name', 'cmb2' ),
  					'id'   => $prefix . 'product_model_2_name',
  					'type' => 'text',
            'row_classes' => 'no-border',
  				),
          array(
  					'name' => __( 'Product Model 2 Image', 'cmb2' ),
  					'id'   => $prefix . 'product_model_2_image',
  					'type' => 'file',
  					'allow' => array( 'url', 'attachment' ),
            'row_classes' => 'no-border',
  				),
          array(
            'name' => __( 'Product Model 2 Content', 'cmb2' ),
            'id'   => $prefix . 'product_model_2_content',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
                'media_buttons' => false,
                'textarea_rows' => 3,
                'tinymce' => $tinySmall,
              ),
          ),
          array(
  					'name' => __( 'Product Model 3 Name', 'cmb2' ),
  					'id'   => $prefix . 'product_model_3_name',
  					'type' => 'text',
            'row_classes' => 'no-border',
  				),
          array(
  					'name' => __( 'Product Model 3 Image', 'cmb2' ),
  					'id'   => $prefix . 'product_model_3_image',
  					'type' => 'file',
  					'allow' => array( 'url', 'attachment' ),
            'row_classes' => 'no-border',
  				),
          array(
            'name' => __( 'Product Model 3 Content', 'cmb2' ),
            'id'   => $prefix . 'product_model_3_content',
              'type'    => 'wysiwyg',
              'escape_cb' => false,
              'sanitization_cb' => false,
              'options' => array(
                'media_buttons' => false,
                'textarea_rows' => 3,
                'tinymce' => $tinySmall,
              ),
          ),
  			),
  		);


    // Fork Content

    $meta_boxes['fork_specific_content'] = array(
  		'id'         => 'fork_specific_content',
  		'title'      => __( 'Fork Content', 'cmb2' ),
  		'object_types'      => array( 'page', ), // Post type
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
  		'show_names' => true, // Show field names on the left
  		//'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
  		'show_on'    => array( 'key' => 'page-template', 'value' => 'page-fork.php'),
  		'fields'     => array(
          array(
  					'id'          => $prefix . 'forked_pages',
  					'type'        => 'group',
  					'options'     => array(
  						'group_title'   => __( 'Fork Item {#}', 'cmb2' ), // {#} gets replaced by row number
  						'add_button'    => __( 'Add Another Fork Item', 'cmb2' ),
  						'remove_button' => __( 'Remove Fork Item', 'cmb2' ),
  						'sortable'      => true, // beta
  					),
  					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
  					'fields'      => array(
	            array(
	                'name'    => __( 'Page Link', 'cmb2' ),
	                'id'      => 'link',
	                'type'    => 'text',
                  'row_classes' => 'no-border',
	            ),
              array(
      					'name' => __( 'Page Image', 'cmb2' ),
      					'id'   => 'image',
      					'type' => 'file',
      					'allow' => array( 'url', 'attachment' ),
                'row_classes' => 'no-border',
      				),
              array(
    						'name' => 'Headline',
    						'id'   => 'headline',
    						'type' => 'text',
                'row_classes' => 'no-border',
    					),
  						array(
  							'name' => 'Description',
  							'id'   => 'description',
                'type'    => 'wysiwyg',
                'escape_cb' => false,
                'sanitization_cb' => false,
                'options' => array(
                  'media_buttons' => false,
                  'textarea_rows' => 3,
                  'tinymce' => $tinySmall,
                ),
  						),
  					),
  				),
  			),
  		);


      // Training Products Content

      $meta_boxes['training_products_specific_content'] = array(
        'id'         => 'training_products_specific_content',
        'title'      => __( 'Training Products Content', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'context'    => 'normal', //  'normal', 'advanced', or 'side'
        'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
        //'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
        'show_on'    => array( 'key' => 'page-template', 'value' => 'page-training-products.php'),
        'fields'     => array(
          array(
              'name'    => 'Headline',
              'id'      => $prefix . 'headline',
              'type'    => 'text',
              'row_classes' => 'no-border',
          ),
          array(
            'name' => 'Sub Headline',
            'id'   => $prefix . 'sub_headline',
            'type'    => 'wysiwyg',
            'escape_cb' => false,
            'sanitization_cb' => false,
            'options' => array(
              'media_buttons' => false,
              'textarea_rows' => 3,
              'tinymce' => $tinySmall,
            ),
          ),
            array(
              'id'          => $prefix . 'forked_pages',
              'type'        => 'group',
              'options'     => array(
                'group_title'   => __( 'Training Product Item {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => __( 'Add Another Training Product Item', 'cmb2' ),
                'remove_button' => __( 'Remove Training Product Item', 'cmb2' ),
                'sortable'      => true, // beta
              ),
              // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
              'fields'      => array(
                array(
                  'name' => __( 'Page Image', 'cmb2' ),
                  'id'   => 'image',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                  'row_classes' => 'no-border',
                ),
                array(
                    'name'    => __( 'Button Link', 'cmb2' ),
                    'id'      => 'link',
                    'type'    => 'text',
                    'row_classes' => 'no-border',
                ),
                array(
                    'name'    => __( 'Link Target', 'cmb2' ),
                    'id'      => 'link_target',
                    'type'    => 'select',
                    'options' => array(
                      '_self' => __( 'Current Window', 'cmb2' ),
                      '_blank'   => __( 'New Window', 'cmb2' ),
                  ),
                  'default' => '_self',
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => 'Button Link Text',
                  'id'   => 'headline',
                  'type' => 'text',
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => 'Description',
                  'id'   => 'description',
                  'type'    => 'wysiwyg',
                  'escape_cb' => false,
                  'sanitization_cb' => false,
                  'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'tinymce' => $tinySmall,
                  ),
                ),
              ),
            ),
          ),
        );

      /*
      $meta_boxes['login_form_options'] = array(
    		'id'         => 'login_form_options',
    		'title'      => __( 'Login Form Options', 'cmb2' ),
    		'object_types'      => array( 'page', ), // Post type
        'show_on'    => array( 'key' => 'page-template', 'value' => 'page-login.php'),
    		'context'    => 'normal', //  'normal', 'advanced', or 'side'
    		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
    		'show_names' => true, // Show field names on the left
        'closed'       => true,
    		'fields'     => array(
            array(
              'name' => 'Redirect on Login URL',
              'id'   => $prefix . 'redirect_on_login_url',
              'type' => 'text',
            ),
    			),
    		);
        */




    $meta_boxes['hero'] = array(
  		'id'         => 'hero',
  		'title'      => __( 'Hero', 'cmb2' ),
  		'object_types' => array( 'page', ), // Post type
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
  		'show_names' => true, // Show field names on the left
      'closed'       => true,
  		'fields'     => array(
        array(
  				'name'    => __( 'Hero type', 'cmb2' ),
  				'id'      => $prefix . 'hero_type',
  				'type'    => 'radio_inline',
  				'options' => array(
  					'home' => __( 'Home', 'cmb2' ),
  					'product'  => __( 'Product', 'cmb2' ),
  					'product-new'  => __( 'Product (new)', 'cmb2' ),
  				),
  				'default' => 'product',
  			),
        array(
  				'id'          => $prefix . 'hero_slider',
  				'type'        => 'group',
  				'description' => __( 'Add slides to your hero.', 'cmb2' ),
  				'options'     => array(
  					'group_title'   => __( 'Slide {#}', 'cmb2' ), // {#} gets replaced by row number
  					'add_button'    => __( 'Add Another Slide', 'cmb2' ),
  					'remove_button' => __( 'Remove Slide', 'cmb2' ),
  					'sortable'      => true, // beta
  				),
  				'fields'      => array(
  					array(
  						'name' => 'Headline',
  						'id'   => 'headline',
  						'description' => 'Add a &lt;br/&gt; to make a line break.',
  						'type' => 'text',
  						'escape_cb' => false,
  						'sanitization_cb' => false,
  					),
  					array(
  						'name' => 'Sub Headline',
  						'description' => 'Add a &lt;br/&gt; to make a line break.',
  						'id'   => 'sub_headline',
  						'type' => 'text',
  						'escape_cb' => false,
  						'sanitization_cb' => false,
  						// 'show_on_cb' => 'cmb2_is_front',
  					),
  					array(
  						'name' => 'Link',
  						'id'   => 'link',
  						'type' => 'text',
  						// 'show_on_cb' => 'cmb2_is_front',
  					),
  					array(
  						'name' => 'Image',
  						'id'   => 'image',
  						'type' => 'file',
  						'allow' => array( 'url', 'attachment' ),
  					),
  				),
  			),
  		),
    );


    $meta_boxes['show_hide_panel'] = array(
  		'id'         => 'show_hide_panel',
  		'title'      => __( 'Show/Hide Content', 'cmb2' ),
  		'object_types'      => array( 'page', ), // Post type
      'show_on'    => array( 'key' => 'page-template', 'value' => array('default')),
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
  		'show_names' => true, // Show field names on the left
      'closed'       => true,
  		'fields'     => array(
          array(
  					'name' => __( 'Left Column Show/Hide Sections', 'cmb2' ),
  					'id'          => $prefix . 'left_column_showhides',
  					'type'        => 'group',
  					'description' => __( 'Add Show/Hide sections to the left column.', 'cmb2' ),
  					// 'show_on_cb' => 'cmb2_is_default',
  					'options'     => array(
  						'group_title'   => __( 'Section {#}', 'cmb2' ), // {#} gets replaced by row number
  						'add_button'    => __( 'Add Another Section', 'cmb2' ),
  						'remove_button' => __( 'Remove Section', 'cmb2' ),
  						'sortable'      => true, // beta
  					),
  					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
  					'fields'      => array(
  						array(
  							'name' => 'Heading',
  							'id'   => 'heading',
  							'type' => 'text',
  						),
  						array(
  							'name' => __( 'Content', 'cmb2' ),
  							'id'   => 'content',
  						    'type'    => 'wysiwyg',
  						    'escape_cb' => false,
  						    'sanitization_cb' => false,
  						    'options' => array(
  								'media_buttons' => false,
  								'textarea_rows' => 3,
  								//'teeny'         => true,
                  'tinymce' => $tinySmall,
  						    ),
  						),
  					),
  				),

  				array(
  					'name' => __( 'Right Column Show/Hide Sections', 'cmb2' ),
  					'id'          => $prefix . 'right_column_showhides',
  					'type'        => 'group',
  					'description' => __( 'Add Show/Hide sections to the right column.', 'cmb2' ),
  					// 'show_on_cb' => 'cmb2_is_default',
  					'options'     => array(
  						'group_title'   => __( 'Section {#}', 'cmb2' ), // {#} gets replaced by row number
  						'add_button'    => __( 'Add Another Section', 'cmb2' ),
  						'remove_button' => __( 'Remove Section', 'cmb2' ),
  						'sortable'      => true, // beta
  					),
  					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
  					'fields'      => array(
  						array(
  							'name' => 'Heading',
  							'id'   => 'heading',
  							'type' => 'text',
  						),
  						array(
  							'name' => __( 'Content', 'cmb2' ),
  							'id'   => 'content',
  						    'type'    => 'wysiwyg',
  						    'escape_cb' => false,
  						    'sanitization_cb' => false,
  						    'options' => array(
  								'media_buttons' => false,
  								'textarea_rows' => 3,
                  'tinymce' => $tinySmall,
  						    ),
  						),
  					),
  				),
  			),
  		);
    $meta_boxes['home_multicolumn_panel'] = array(
  		'id'         => 'home_multicolumn_panel',
  		'title'      => __( 'Multicolumn Panel', 'cmb2' ),
  		'object_types'      => array( 'page', ), // Post type
      'show_on'    => array( 'key' => 'page-template', 'value' => array('page-home.php')),
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
  		'show_names' => true, // Show field names on the left
      'closed'       => true,
  		'fields'     => array(
					array(
						'name' => 'Heading',
						'id'   => $prefix . 'home_multicolumn_panel_heading',
						'type' => 'text',
					),
          array(
            'id' => $prefix . 'home_multicolumn_heading_color',
            'name' => 'Heading color',
            'type' => 'select',
            'default' => 'color-grey',
            'options' => array(
              'color-grey' => 'Grey',
              'color-green'  => 'Green',
            ),
          ),
					array(
						'name' => __( 'Column One', 'cmb2' ),
            'id'   => $prefix . 'home_multicolumn_col_one',
					    'type'    => 'wysiwyg',
					    'escape_cb' => false,
					    'sanitization_cb' => false,
					    'options' => array(
  							'media_buttons' => false,
  							'textarea_rows' => 3,
                'tinymce' => $tinySmall,
				    ),
            'row_classes' => 'no-border',
					),
					array(
						'name' => __( 'Column Two', 'cmb2' ),
            'id'   => $prefix . 'home_multicolumn_col_two',
					    'type'    => 'wysiwyg',
					    'escape_cb' => false,
					    'sanitization_cb' => false,
					    'options' => array(
  							'media_buttons' => false,
  							'textarea_rows' => 3,
                'tinymce' => $tinySmall,
				    ),
            'row_classes' => 'no-border',
					),
					array(
						'name' => __( 'Column Three', 'cmb2' ),
            'id'   => $prefix . 'home_multicolumn_col_three',
					    'type'    => 'wysiwyg',
					    'escape_cb' => false,
					    'sanitization_cb' => false,
					    'options' => array(
  							'media_buttons' => false,
  							'textarea_rows' => 3,
                'tinymce' => $tinySmall,
				    ),
            'row_classes' => 'no-border',
					),
					array(
						'name' => __( 'Column Four', 'cmb2' ),
            'id'   => $prefix . 'home_multicolumn_col_four',
					    'type'    => 'wysiwyg',
					    'escape_cb' => false,
					    'sanitization_cb' => false,
					    'options' => array(
  							'media_buttons' => false,
  							'textarea_rows' => 3,
                'tinymce' => $tinySmall,
				    ),
					),
          array(
  					'name' => __( 'Button Link', 'cmb2' ),
  					'description' => 'Include a button link',
  					'id'   => $prefix . 'home_multicolumn_panel_button_link',
  					'type' => 'text',
            'before_row' => '<div class="container"><div class="row group">',
            'after_row' => '',
            'row_classes' => 'admin-col col-sm-6',
  				),
  				array(
  					'name' => __( 'Button Link Text', 'cmb2' ),
  					'description' => 'Text on the button',
  					'id'   => $prefix . 'home_multicolumn_panel_button_text',
  					'type' => 'text',
            'before_row' => '',
            'after_row' => '</div></div>',
            'row_classes' => 'admin-col col-sm-6',
  				),
				),
  		);


    $meta_boxes['video_panel'] = array(
  		'id'         => 'video_panel',
  		'title'      => __( 'Video Content', 'cmb2' ),
  		'object_types'      => array( 'page', ), // Post type
      'show_on'    => array( 'key' => 'page-template', 'value' => array('default')),
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
  		'show_names' => true, // Show field names on the left
      'closed'       => true,
  		'fields'     => array(
      array(
        'name'    => __( 'Single Video?', 'cmb2' ),
        'id'      => $prefix . 'video_panel_single_video',
        'description' => 'Setting this to Yes will only show the first video in the left column with no title.',
        'type'    => 'radio_inline',
        'options' => array(
          'yes' => __( 'Yes', 'cmb2' ),
          'no'  => __( 'No', 'cmb2' ),
        ),
        'default' => 'no',
      ),
      array(
        'name' => 'Video Placeholder Image',
        'id'   => $prefix . 'video_panel_image',
        'type' => 'file',
        'allow' => array( 'url', 'attachment' ),
      ),
        array(
  					'name' => __( 'Left Column Videos', 'cmb2' ),
  					'id'          => $prefix . 'left_videos',
  					'type'        => 'group',
  					'description' => __( 'Add videos to the left column.', 'cmb2' ),
  					//'show_on_cb' => 'cmb2_is_default',
  					'options'     => array(
  						'group_title'   => __( 'Video {#}', 'cmb2' ), // {#} gets replaced by row number
  						'add_button'    => __( 'Add Another Video', 'cmb2' ),
  						'remove_button' => __( 'Remove Video', 'cmb2' ),
  						'sortable'      => true, // beta
  					),
  					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
  					'fields'      => array(
  						array(
  							'name' => 'Title',
  							'id'   => 'title',
  							'type' => 'text',
  						),
  						array(
  							'name' => 'Embed code',
  							'id'   => 'embed',
  							'type' => 'oembed',
                'description' => 'Paste video URL, should look like this: https://www.youtube.com/watch?v=T79Jzsdg0Mw',
  						),
  					),
  				),
  				array(
  					'name' => __( 'Right Column Videos', 'cmb2' ),
  					'id'          => $prefix . 'right_videos',
  					'type'        => 'group',
  					'description' => __( 'Add videos to the right column.', 'cmb2' ),
  					//'show_on_cb' => 'cmb2_is_default',
  					'options'     => array(
  						'group_title'   => __( 'Video {#}', 'cmb2' ), // {#} gets replaced by row number
  						'add_button'    => __( 'Add Another Video', 'cmb2' ),
  						'remove_button' => __( 'Remove Video', 'cmb2' ),
  						'sortable'      => true, // beta
  					),
  					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
  					'fields'      => array(
  						array(
  							'name' => 'Title',
  							'id'   => 'title',
  							'type' => 'text',
  						),
  						array(
  							'name' => 'Embed code',
  							'id'   => 'embed',
  							'type' => 'oembed',
                'description' => 'Paste video URL, should look like this: https://www.youtube.com/watch?v=T79Jzsdg0Mw',
  						),
  					),
  				),
  			),
  		);

      $meta_boxes['other_video_panel'] = array(
    		'id'         => 'other_video_panel',
    		'title'      => __( 'Video Panel', 'cmb2' ),
    		'object_types'      => array( 'page', ), // Post type
        'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-home.php', 'page-product.php')),
    		'context'    => 'normal', //  'normal', 'advanced', or 'side'
    		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
    		'show_names' => true, // Show field names on the left
        'closed'     => true,
    		'fields'     => array(
          array(
            'name' => 'Video Placeholder Image',
            'id'   => $prefix . 'other_video_image',
            'type' => 'file',
            'allow' => array( 'url', 'attachment' ),
          ),
          array(
    					'name' => __( 'Videos', 'cmb2' ),
    					'id'          => $prefix . 'other_videos',
    					'type'        => 'group',
    					//'description' => __( 'Add videos.', 'cmb2' ),
    					'options'     => array(
    						'group_title'   => __( 'Video {#}', 'cmb2' ), // {#} gets replaced by row number
    						'add_button'    => __( 'Add Another Video', 'cmb2' ),
    						'remove_button' => __( 'Remove Video', 'cmb2' ),
    						'sortable'      => true, // beta
    					),
    					'fields'      => array(
    						array(
    							'name' => 'Title',
    							'id'   => 'title',
    							'type' => 'text',
                  'type' => 'text',
                  'before_row' => '<div class="container"><div class="row group">',
                  'after_row' => '',
                  'row_classes' => 'admin-col col-sm-6',
    						),
    						array(
    							'name' => 'Embed code',
    							'id'   => 'embed',
    							'type' => 'oembed',
                  'description' => 'Paste video URL, should look like this: https://www.youtube.com/watch?v=T79Jzsdg0Mw',
                  'before_row' => '',
                  'after_row' => '</div></div>',
                  'row_classes' => 'admin-col col-sm-6',
    						),
    					),
    				),
    			),
    		);

      $meta_boxes['info_table'] = array(
      		'id'         => 'info_table',
      		'title'      => __( 'Info Table', 'cmb2' ),
      		'object_types'      => array( 'page', ), // Post type
      		'context'    => 'normal', //  'normal', 'advanced', or 'side'
      		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
      		'show_names' => true, // Show field names on the left
          'closed'     => true,
      		//'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
          'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-product.php')),
      		'fields'     => array(
            array(
                'name'    => __( 'Add Bottom Padding', 'cmb2' ),
                'id'      => $prefix . 'info_table_include_bottom_padding',
                'type'    => 'select',
                'options' => array(
                  'yes' => __( 'Yes', 'cmb2' ),
                  'no'   => __( 'No', 'cmb2' ),
              ),
              'default' => 'no',
            ),
            array(
              'name' => 'Tab one title',
              'id'   => $prefix . 'tab_one_title',
              'type' => 'text',
              'row_classes' => 'no-border',
            ),
      				array(
      					'id'          => $prefix . 'info_table_tab_one',
      					'type'        => 'group',
                'name'        => 'Tab 1 Rows',
      					'options'     => array(
      						'group_title'   => __( 'Row {#}', 'cmb2' ), // {#} gets replaced by row number
      						'add_button'    => __( 'Add Another Row', 'cmb2' ),
      						'remove_button' => __( 'Remove Row', 'cmb2' ),
      						'sortable'      => true, // beta
      					),
      					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
      					'fields'      => array(
      						array(
      							'name' => 'Title',
      							'id'   => 'title',
      							'type' => 'textarea_small',
                    'before_row' => '<div class="container"><div class="row group">',
                    'after_row' => '',
                    'row_classes' => 'admin-col col-sm-6',
      						),
      						array(
      							'name' => 'Description',
      							'id'   => 'description',
      							'type' => 'textarea_small',
                    'before_row' => '',
                    'after_row' => '</div></div>',
                    'row_classes' => 'admin-col col-sm-6',
      						),
                ),
              ),
              array(
                'name' => 'Tab two title',
                'id'   => $prefix . 'tab_two_title',
                'type' => 'text',
                'row_classes' => 'no-border',
              ),
      				array(
      					'id'          => $prefix . 'info_table_tab_two',
      					'type'        => 'group',
                'name'        => 'Tab 2 Rows',
      					'options'     => array(
      						'group_title'   => __( 'Row {#}', 'cmb2' ), // {#} gets replaced by row number
      						'add_button'    => __( 'Add Another Row', 'cmb2' ),
      						'remove_button' => __( 'Remove Row', 'cmb2' ),
      						'sortable'      => true, // beta
      					),
      					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
      					'fields'      => array(
                  array(
      							'name' => 'Title',
      							'id'   => 'title',
      							'type' => 'textarea_small',
                    'before_row' => '<div class="container"><div class="row group">',
                    'after_row' => '',
                    'row_classes' => 'admin-col col-sm-6',
      						),
      						array(
      							'name' => 'Description',
      							'id'   => 'description',
      							'type' => 'textarea_small',
                    'before_row' => '',
                    'after_row' => '</div></div>',
                    'row_classes' => 'admin-col col-sm-6',
      						),
      					),
      				),
      			),
    		);

        $meta_boxes['home_features_panel'] = array(
         'id'         => 'home_features_panel',
         'title'      => __( 'Features Panel', 'cmb2' ),
         'object_types'      => array( 'page', ), // Post type
         'context'    => 'normal', //  'normal', 'advanced', or 'side'
         'priority'   => 'default', //  'high', 'core', 'default' or 'low'
         'show_names' => true, // Show field names on the left
         'show_on'    => array( 'key' => 'page-template', 'value' => array('page-home.php')),
         //'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
         'fields'     => array(
           array(
             'id'          => $prefix . 'home_featured_items',
             'type'        => 'group',
             'options'     => array(
               'group_title'   => __( 'Link {#}', 'cmb2' ), // {#} gets replaced by row number
               'add_button'    => __( 'Add Another Link', 'cmb2' ),
               'remove_button' => __( 'Remove Link', 'cmb2' ),
               'sortable'      => true, // beta
             ),
             // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
             'fields'      => array(
               array(
                 'name' => 'Image',
                 'id'   => 'image',
                 'type' => 'file',
                 'allow' => array( 'url', 'attachment' ),
               ),
                 array(
                     'name'    => __( 'Select Page', 'cmb2' ),
                     'id'      => 'link',
                     'type'    => 'select',
                     'options' => cmb2_get_all_pages( array( 'post_type' => 'page', 'numberposts' => 1000, ) ),
                 ),
               array(
                 'name' => 'link text',
                 'id'   => 'link_text',
                 'type' => 'text',
                 'description' => 'Add a &lt;br/&gt; to make a line break.',
                 'escape_cb' => false,
                 'sanitization_cb' => false,
               ),
             ),
           ),
         ),
         );

         $meta_boxes['ribbon_panel'] = array(
           'id'         => 'ribbon_panel',
           'title'      => __( 'Ribbon Panel', 'cmb2' ),
           'object_types' => array( 'page', ), // Post type
           'context'    => 'normal', //  'normal', 'advanced', or 'side'
           'priority'   => 'default', //  'high', 'core', 'default' or 'low'
           'show_names' => true, // Show field names on the left
           'closed'     => true,
           'fields'     => array(
             array(
                 'name'    => __( 'Panel color', 'cmb2' ),
                 'id'      => $prefix . 'ribbon_panel_color',
                 'type'    => 'select',
                 'options' => array(
                   'panel-green-dark' => __( 'Green', 'cmb2' ),
                   'panel-white' => __( 'White', 'cmb2' ),
                   'panel-tan' => __( 'Tan', 'cmb2' ),
                 ),
                 'default' => 'panel-green-dark',
                 'before_row' => '<div class="container"><div class="row group">',
                 'after_row' => '',
                 'row_classes' => 'admin-col col-sm-2',
               ),
               array(
                 'name' => __( 'Image', 'cmb2' ),
                 'desc' => __( 'Left column image. Upload an image or enter a URL.', 'cmb2' ),
                 'id'   => $prefix . 'ribbon_panel_image',
                 'type' => 'file',
                 'allow' => array( 'url', 'attachment' ),
                 'before_row' => '',
                 'after_row' => '</div></div>',
                 'row_classes' => 'admin-col col-sm-10',
               ),
               array(
                 'name' => __( 'Text', 'cmb2' ),
                 'id'      => $prefix . 'ribbon_panel_text',
                 'type'    => 'wysiwyg',
                 'escape_cb' => false,
                 'sanitization_cb' => false,
                 'options' => array(
                   'media_buttons' => false,
                   'textarea_rows' => 3,
                   'tinymce' => $photoWithText,
                 ),
               ),
           ),
         );

      $meta_boxes['featured_panel'] = array(
    		'id'         => 'featured_panel',
    		'title'      => __( 'Featured Panel', 'cmb2' ),
    		'object_types' => array( 'page', ), // Post type
        'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-home.php', 'page-fork.php', 'page-child-list.php')),
    		'context'    => 'normal', //  'normal', 'advanced', or 'side'
    		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
    		'show_names' => true, // Show field names on the left
        'closed'       => true,
    		'fields'     => array(
					array(
						'name' => 'Image',
						'id'   => $prefix . 'featured_image',
						'type' => 'file',
						'allow' => array( 'url', 'attachment' ),
					),
          array(
						'name' => 'Headline',
						'id'   => $prefix . 'featured_headline',
						'description' => 'Add a &lt;br/&gt; to make a line break.',
						'type' => 'text',
						'escape_cb' => false,
						'sanitization_cb' => false,
					),
          array(
  					'name' => 'Featured Column Left',
  					'id'   => $prefix . 'featured_column_left',
  				    'type'    => 'wysiwyg',
  				    'escape_cb' => false,
  				    'sanitization_cb' => false,
  				    'options' => array(
  						'media_buttons' => false,
  						'textarea_rows' => 5,
  						'teeny'         => true,
  				  ),
            'before_row' => '<div class="container"><div class="row group">',
            'after_row' => '',
            'row_classes' => 'admin-col col-sm-6',
  				),
          array(
  					'name' => 'Featured Column Right',
  					'id'   => $prefix . 'featured_column_right',
  				    'type'    => 'wysiwyg',
  				    'escape_cb' => false,
  				    'sanitization_cb' => false,
  				    'options' => array(
  						'media_buttons' => false,
  						'textarea_rows' => 5,
  						'teeny'         => true,
  				  ),
            'before_row' => '',
            'after_row' => '</div></div>',
            'row_classes' => 'admin-col col-sm-6',
  				),
      ),
      );

      $meta_boxes['photo_text_panel'] = array(
        'id'         => 'photo_text_panel',
        'title'      => __( 'Photo with Text Panel', 'cmb2' ),
        'object_types' => array( 'page', ), // Post type
        'context'    => 'normal', //  'normal', 'advanced', or 'side'
        'priority'   => 'default', //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
        'closed'       => true,
        'fields'     => array(
          array(
            'name'    => __( 'Panel size', 'cmb2' ),
            'id'      => $prefix . 'photo_text_panel_size',
            'type'    => 'radio_inline',
            'options' => array(
              'panel_small' => __( 'Small', 'cmb2' ),
              'panel_large'  => __( 'Large', 'cmb2' ),
            ),
            'default' => 'panel_small',
          ),
          array(
            'name'    => __( 'Text alignment', 'cmb2' ),
            'id'      => $prefix . 'photo_text_panel_alignment',
            'type'    => 'radio_inline',
            'options' => array(
              'panel_left' => __( 'Left', 'cmb2' ),
              'panel_right'  => __( 'Right', 'cmb2' ),
            ),
            'default' => 'panel_left',
          ),
          array(
            'name' => __( 'Text', 'cmb2' ),
            'id'      => $prefix . 'photo_text_panel_text',
            'type'    => 'wysiwyg',
            'escape_cb' => false,
            'sanitization_cb' => false,
            'options' => array(
              'media_buttons' => false,
              'textarea_rows' => 3,
              'tinymce' => $photoWithText,
            ),
          ),
            array(
              'name' => __( 'Small Image', 'cmb2' ),
              'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
              'id'   => $prefix . 'photo_text_panel_image_small',
              'type' => 'file',
              'allow' => array( 'url', 'attachment' ),
            ),
            array(
              'name' => __( 'Large Image', 'cmb2' ),
              'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
              'id'   => $prefix . 'photo_text_panel_image_large',
              'type' => 'file',
              'allow' => array( 'url', 'attachment' ),
            ),
        ),
      );

      /*
      $meta_boxes['flex_panels'] = array(
    		'id'         => 'flex_panels',
    		'title'      => __( 'Flex Panels', 'cmb2' ),
    		'object_types'      => array( 'page', ), // Post type
    		'context'    => 'normal', //  'normal', 'advanced', or 'side'
    		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
    		'show_names' => true, // Show field names on the left
    		'fields'     => array(
          array(
    					'name' => __( 'Flex Panel', 'cmb2' ),
    					'id'          => $prefix . 'flex_panel',
    					'type'        => 'group',
              'repeatable'    => false,
    					'options'     => array(
    						'group_title'   => __( 'Panel {#}', 'cmb2' ), // {#} gets replaced by row number
    						'add_button'    => __( 'Add Another Panel', 'cmb2' ),
    						'remove_button' => __( 'Remove Panel', 'cmb2' ),
    						'sortable'      => true, // beta
    					),
    					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
    					'fields'      => array(
                  array(
                		'name'    => __( 'Panel Type', 'cmb2' ),
                		'id'      => 'panel_type',
                		'type'    => 'select',
                		'options' => array(
                			'standard' => __( 'Standard', 'cmb2' ),
                			'custom'   => __( 'Custom', 'cmb2' ),
                	),
                ),


                array(
                    'name' => __( 'S Box', 'cmb2' ),
                    'id'         => 'group_standard',
                    'type'       => 'group',
                    'options'     => array(
          						'group_title'   => __( 'Panel {#}', 'cmb2' ), // {#} gets replaced by row number
          						'add_button'    => __( 'Add Another Panel', 'cmb2' ),
          						'remove_button' => __( 'Remove Panel', 'cmb2' ),
          					),
                    'fields'     => array(
                      array(
                        'name' => 'Cool Title',
                        'id'   => 'cool_title',
                        'type' => 'text_small',
                      ),
                    ),
                  ),
    					),
    				),
    			),
    		);
        */

        $meta_boxes['text_panel'] = array(
      		'id'         => 'text_panel',
      		'title'      => __( 'Text Panel', 'cmb2' ),
      		'object_types'      => array( 'page', ), // Post type
          'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-product.php')),
      		'context'    => 'normal', //  'normal', 'advanced', or 'side'
      		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
      		'show_names' => true, // Show field names on the left
          'closed'       => true,
      		'fields'     => array(
              array(
      					'name'        => __( 'Text Section', 'cmb2' ),
      					'id'          => $prefix . 'text_section',
      					'type'        => 'group',
      					'description' => __( 'Add text sections to the left column.', 'cmb2' ),
      					'options'     => array(
      						'group_title'   => __( 'Section {#}', 'cmb2' ), // {#} gets replaced by row number
      						'add_button'    => __( 'Add Another Section', 'cmb2' ),
      						'remove_button' => __( 'Remove Section', 'cmb2' ),
      					),
      					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
      					'fields'      => array(
                  array(
                  		'name'    => __( 'Panel color', 'cmb2' ),
                  		'id'      => 'panel_color',
                  		'type'    => 'select',
                  		'options' => array(
                  			'panel-white' => __( 'White', 'cmb2' ),
                        'panel-tan' => __( 'Tan', 'cmb2' ),
                  		),
                      'default' => 'panel-white',
                  	),
                    array(
        							'name' => __( 'Top Row Content', 'cmb2' ),
        							'id'   => 'top_row_content',
      						    'type'    => 'wysiwyg',
      						    'escape_cb' => false,
      						    'sanitization_cb' => false,
      						    'options' => array(
        								'media_buttons' => true,
        								'textarea_rows' => 2,
        								//'teeny'         => true,
                        'tinymce' => $tinySmall2,
        						  ),
                    ),
      						array(
      							'name' => __( 'Left Column Content', 'cmb2' ),
      							'id'   => 'left_column_content',
    						    'type'    => 'wysiwyg',
    						    'escape_cb' => false,
    						    'sanitization_cb' => false,
    						    'options' => array(
      								'media_buttons' => true,
      								'textarea_rows' => 3,
      								//'teeny'         => true,
                      'tinymce' => $tinySmall,
      						  ),
                    'before_row' => '<div class="container"><div class="row group">',
                    'after_row' => '',
                    'row_classes' => 'admin-col col-sm-6',
                  ),
      						array(
      							'name' => __( 'Right Column Content', 'cmb2' ),
      							'id'   => 'right_column_content',
    						    'type'    => 'wysiwyg',
    						    'escape_cb' => false,
    						    'sanitization_cb' => false,
    						    'options' => array(
      								'media_buttons' => true,
      								'textarea_rows' => 3,
      								//'teeny'         => true,
                      'tinymce' => $tinySmall,
      						  ),
                    'before_row' => '',
                    'after_row' => '</div></div>',
                    'row_classes' => 'admin-col col-sm-6',
      						),
                  array(
                    'name'    => __( 'Remove top padding?', 'cmb2' ),
                    'id'      => 'remove_top_padding',
                    'type'    => 'radio_inline',
                    'options' => array(
                      'yes' => __( 'Yes', 'cmb2' ),
                      'no'  => __( 'No', 'cmb2' ),
                    ),
                    'default' => 'no',
                    'before_row' => '<div class="container"><div class="row group">',
                    'after_row' => '',
                    'row_classes' => 'admin-col col-sm-6',
                ),
                array(
                  'name'    => __( 'Remove bottom padding?', 'cmb2' ),
                  'id'      => 'remove_bottom_padding',
                  'type'    => 'radio_inline',
                  'options' => array(
                    'yes' => __( 'Yes', 'cmb2' ),
                    'no'  => __( 'No', 'cmb2' ),
                  ),
                  'default' => 'no',
                  'before_row' => '',
                  'after_row' => '</div></div>',
                  'row_classes' => 'admin-col col-sm-6',
              ),
      					),
      				),
      			),
      		);



          $meta_boxes['cta_panel'] = array(
          		'id'         => 'cta_panel',
          		'title'      => __( 'CTA Panel', 'cmb2' ),
          		'object_types'      => array( 'page', ), // Post type
          		'context'    => 'normal', //  'normal', 'advanced', or 'side'
          		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
          		'show_names' => true, // Show field names on the left
              'closed'     => true,
          		//'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
              'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-product.php')),
          		'fields'     => array(
                array(
                  'name' => 'Headline',
                  'id'   => $prefix . 'cta_headline',
                  'type' => 'text'
                ),
                array(
                  'name' => 'Button Text',
                  'id'   => $prefix . 'cta_button_text',
                  'type' => 'text'
                ),
                array(
                  'name' => 'Button URL',
                  'id'   => $prefix . 'cta_button_url',
                  'type' => 'text'
                ),
          		),
        		);


          $meta_boxes['image_nav_panel'] = array(
        		'id'         => 'image_nav_panel',
        		'title'      => __( 'Image Nav Panel', 'cmb2' ),
        		'object_types' => array( 'page', ), // Post type
            'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product.php', 'page-product-new.php')),
        		'context'    => 'normal', //  'normal', 'advanced', or 'side'
        		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
        		'show_names' => true, // Show field names on the left
            'closed'       => true,
        		'fields'     => array(

              array(
                'name' => 'Headline',
                'id'   => $prefix . 'image_nav_headline',
                'type' => 'text',
                'escape_cb' => false,
                'sanitization_cb' => false,
                'row_classes' => 'no-border',
              ),
              array(
    						'name' => 'Sub Headline',
                'id'   => $prefix . 'image_nav_subhead',
    					    'type'    => 'wysiwyg',
    					    'escape_cb' => false,
    					    'sanitization_cb' => false,
    					    'options' => array(
      							'media_buttons' => false,
      							'textarea_rows' => 3,
                    'tinymce' => $tinySmall,
    				    ),
                'row_classes' => 'no-border'
    					),
              array(
                'name' => 'Image Links Title',
                'id'   => $prefix . 'image_links_title',
                'description' => 'Heading that appears above the nav links',
                'type' => 'text',
                'escape_cb' => false,
                'sanitization_cb' => false,
              ),


              array(
        				'id'          => $prefix . 'image_nav_images',
        				'type'        => 'group',
        				'description' => __( 'Add images.', 'cmb2' ),
        				'options'     => array(
        					'group_title'   => __( 'Image {#}', 'cmb2' ), // {#} gets replaced by row number
        					'add_button'    => __( 'Add Another Image', 'cmb2' ),
        					'remove_button' => __( 'Remove Image', 'cmb2' ),
        					'sortable'      => true, // beta
        				),
        				'fields'      => array(
        					array(
        						'name' => 'Title',
        						'id'   => 'title',
        						'type' => 'text',
                    'before_row' => '<div class="container"><div class="row group">',
                    'after_row' => '',
                    'row_classes' => 'admin-col col-sm-6',
        					),
        					array(
        						'name' => 'Image',
        						'id'   => 'image',
        						'type' => 'file',
        						'allow' => array( 'url', 'attachment' ),
                    'before_row' => '',
                    'after_row' => '</div></div>',
                    'row_classes' => 'admin-col col-sm-6',
        					),
        				),
        			),
        		),
          );


        $meta_boxes['faq_panel'] = array(
      		'id'         => 'faq_panel',
      		'title'      => __( 'FAQ Panel', 'cmb2' ),
      		'object_types'      => array( 'page', ), // Post type
          'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-product.php', 'page-product-new.php')),
      		'context'    => 'normal', //  'normal', 'advanced', or 'side'
      		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
      		'show_names' => true, // Show field names on the left
          'closed'       => true,
      		'fields'     => array(
              array(
                'name' => 'Full FAQ Link',
                'id'   => $prefix . 'faq_link',
                'description' => 'Link to your full FAQ page',
                'type' => 'text',
              ),
              array(
      					'name'        => __( 'Left Column FAQ Sections', 'cmb2' ),
      					'id'          => $prefix . 'left_column_faq',
      					'type'        => 'group',
      					'description' => __( 'Add FAQ sections to the left column.', 'cmb2' ),
      					'options'     => array(
      						'group_title'   => __( 'Section {#}', 'cmb2' ), // {#} gets replaced by row number
      						'add_button'    => __( 'Add Another Section', 'cmb2' ),
      						'remove_button' => __( 'Remove Section', 'cmb2' ),
      						'sortable'      => true, // beta
      					),
      					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
      					'fields'      => array(
      						array(
                    'name' => __( 'Question', 'cmb2' ),
      							'id'   => 'question',
      							'type' => 'text',
      						),
      						array(
      							'name' => __( 'Answer', 'cmb2' ),
      							'id'   => 'answer',
    						    'type'    => 'wysiwyg',
    						    'escape_cb' => false,
    						    'sanitization_cb' => false,
    						    'options' => array(
      								'media_buttons' => false,
      								'textarea_rows' => 3,
      								//'teeny'         => true,
                      'tinymce' => $tinySmall,
      						  ),
      						),
      					),
      				),

      				array(
      					'name'        => __( 'Right Column FAQ Sections', 'cmb2' ),
      					'id'          => $prefix . 'right_column_faq',
      					'type'        => 'group',
      					'description' => __( 'Add FAQ sections to the right column.', 'cmb2' ),
      					'options'     => array(
      						'group_title'   => __( 'Section {#}', 'cmb2' ), // {#} gets replaced by row number
      						'add_button'    => __( 'Add Another Section', 'cmb2' ),
      						'remove_button' => __( 'Remove Section', 'cmb2' ),
      						'sortable'      => true, // beta
      					),
      					// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
      					'fields'      => array(
      						array(
                    'name' => __( 'Question', 'cmb2' ),
      							'id'   => 'question',
      							'type' => 'text',
      						),
      						array(
      							'name' => __( 'Answer', 'cmb2' ),
      							'id'   => 'answer',
    						    'type'    => 'wysiwyg',
    						    'escape_cb' => false,
    						    'sanitization_cb' => false,
    						    'options' => array(
      								'media_buttons' => false,
      								'textarea_rows' => 3,
      								//'teeny'         => true,
                      'tinymce' => $tinySmall,
      						  ),
      						),
      					),
      				),
      			),
      		);

          // Comparison Panel

          $meta_boxes['comparison_panel'] = array(
        		'id'         => 'comparison_panel',
        		'title'      => __( 'Comparison Panel', 'cmb2' ),
        		'object_types'      => array( 'page', ), // Post type
        		'context'    => 'normal', //  'normal', 'advanced', or 'side'
        		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
        		'show_names' => true, // Show field names on the left
        		//'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
            'closed' => true,
            'show_on'    => array( 'key' => 'page-template', 'value' => array('default', 'page-home.php')),
        		'fields'     => array(
              array(
                  'name'    => __( 'Panel Title', 'cmb2' ),
                  'id'      => $prefix . 'comparison_panel_title',
                  'type'    => 'text',
                  'row_classes' => 'no-border',
              ),
              array(
                'name' => 'Left Column Content',
                'id'      => $prefix . 'comparison_panel_left_column_content',
                'type'    => 'wysiwyg',
                'escape_cb' => false,
                'sanitization_cb' => false,
                'options' => array(
                  'media_buttons' => false,
                  'textarea_rows' => 3,
                  'tinymce' => $tinySmall,
                ),
                'before_row' => '<div class="container"><div class="row group">',
                'after_row' => '',
                'row_classes' => 'admin-col col-sm-6',
              ),
              array(
                'name' => 'Right Column Content',
                'id'      => $prefix . 'comparison_panel_right_column_content',
                'type'    => 'wysiwyg',
                'escape_cb' => false,
                'sanitization_cb' => false,
                'options' => array(
                  'media_buttons' => false,
                  'textarea_rows' => 3,
                  'tinymce' => $tinySmall,
                ),
                'before_row' => '',
                'after_row' => '</div></div>',
                'row_classes' => 'admin-col col-sm-6',
              ),

              array(
      					'name' => 'File 1 Link',
      					'id'   => $prefix . 'comparison_panel_file_1_file',
      					'type' => 'text',
                'before_row' => '<div class="container"><div class="row group">',
                'after_row' => '',
                'row_classes' => 'admin-col col-sm-6',
      				),
              array(
    						'name' => 'File 1 Button Text',
                'id'   => $prefix . 'comparison_panel_file_1_link_text',
    						'type' => 'text',
                'before_row' => '',
                'after_row' => '</div></div>',
                'row_classes' => 'admin-col col-sm-6',
    					),
              array(
      					'name' => 'File 1 Description',
      					'id'   => $prefix . 'comparison_panel_file_1_description',
      					'type' => 'textarea_small',
      				),
              array(
      					'name' => 'File 2 Link',
      					'id'   => $prefix . 'comparison_panel_file_2_file',
      					'type' => 'text',
                'before_row' => '<div class="container"><div class="row group">',
                'after_row' => '',
                'row_classes' => 'admin-col col-sm-6',
      				),
              array(
    						'name' => 'File 2 Button Text',
                'id'   => $prefix . 'comparison_panel_file_2_link_text',
    						'type' => 'text',
                'before_row' => '',
                'after_row' => '</div></div>',
                'row_classes' => 'admin-col col-sm-6',
    					),
              array(
      					'name' => 'File 2 Description',
      					'id'   => $prefix . 'comparison_panel_file_2_description',
      					'type' => 'textarea_small',
      				),
              array(
      					'name' => 'File 3 Link',
      					'id'   => $prefix . 'comparison_panel_file_3_file',
      					'type' => 'text',
                'before_row' => '<div class="container"><div class="row group">',
                'after_row' => '',
                'row_classes' => 'admin-col col-sm-6',
      				),
              array(
    						'name' => 'File 3 Button Text',
                'id'   => $prefix . 'comparison_panel_file_3_link_text',
    						'type' => 'text',
                'before_row' => '',
                'after_row' => '</div></div>',
                'row_classes' => 'admin-col col-sm-6',
    					),
              array(
      					'name' => 'File 3 Description',
      					'id'   => $prefix . 'comparison_panel_file_3_description',
      					'type' => 'textarea_small',
      				),

        			),
        		);

            $meta_boxes['find_a_dealer_form_panel'] = array(
          		'id'         => 'find_a_dealer_form_panel',
          		'title'      => __( 'Find a Dealer Form Panel', 'cmb2' ),
          		'object_types'      => array( 'page', ), // Post type
          		'context'    => 'normal', //  'normal', 'advanced', or 'side'
          		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
          		'show_names' => false, // Show field names on the left
              // 'show_on_cb' => 'cmb2_is_default',
              'show_on'    => array( 'key' => 'page-template', 'value' => array('default')),
          		'fields'     => array(
                array(
                    'description' => 'This panel doesn\'nt have any options. If you don\'t want it included, uncheck it in the Template Options section.',
                    'type' => 'title',
                    'id' => $prefix . 'panel_options_title',
                    'row_classes' => 'no-border clear-pad',
                ),
        			),
        		);


    $meta_boxes['footer_image'] = array(
  		'id'         => 'footer_image',
  		'title'      => __( 'Footer Image', 'cmb2' ),
  		'object_types'      => array( 'page', ), // Post type
  		'context'    => 'normal', //  'normal', 'advanced', or 'side'
  		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
  		'show_names' => false, // Show field names on the left
      'closed'       => true,
  		'fields'     => array(
          array(
            'name' => __( 'Footer Image', 'cmb2' ),
            'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
            'id'   => $prefix . 'footer_image',
            'type' => 'file',
            'allow' => array( 'url', 'attachment' ),
          ),
  			),
  		);


    // Child List Template Options

     $meta_boxes['child_list_template'] = array(
   		'id'         => 'child_list_template',
   		'title'      => __( 'Child List Template Options', 'cmb2' ),
   		'object_types'      => array( 'page', ), // Post type
   		'context'    => 'normal', //  'normal', 'advanced', or 'side'
   		'priority'   => 'high', //  'high', 'core', 'default' or 'low'
   		'show_names' => true, // Show field names on the left
      'show_on'    => array( 'key' => 'page-template', 'value' => array('page-child-list.php')),
     		'fields'     => array(
           array(
   					'name'    => __( 'Include contextual nav', 'cmb2' ),
   					'id'      => $prefix . 'contextual_nav',
   					'type'    => 'radio_inline',
   					'options' => array(
   						'Yes' => __( 'Yes', 'cmb2' ),
   						'No'  => __( 'No', 'cmb2' ),
   					),
   					'default' => 'Yes',
   				),
           array(
   					'name'    => __( 'Child columns', 'cmb2' ),
   					'id'      => $prefix . 'child_columns',
   					'type'    => 'radio_inline',
   					'options' => array(
   						'1' => __( '1', 'cmb2' ),
   						'2'  => __( '2', 'cmb2' ),
   						'3'  => __( '3', 'cmb2' ),
   					),
   					'default' => '1',
   				),
   				array(
   					'name'    => __( 'Child list type', 'cmb2' ),
   					'id'      => $prefix . 'child_list_type',
   					'type'    => 'radio_inline',
   					'options' => array(
   						'menu' => __( 'Menu list', 'cmb2' ),
   						'product'  => __( 'Product list', 'cmb2' ),
   					),
   					'default' => 'menu',
   				),
   			),
   		);

 // Dealer Custom Post Type Meta

  $meta_boxes['dealer_specific_content'] = array(
		'id'         => 'dealer_specific_content',
		'title'      => __( 'Dealer Specific Content', 'cmb2' ),
		'object_types'      => array( 'dealer', ), // Post type
		'context'    => 'normal', //  'normal', 'advanced', or 'side'
		'priority'   => 'default', //  'high', 'core', 'default' or 'low'
		'show_names' => true, // Show field names on the left
		'fields'     => array(
      array(
        'name' => 'Sub headline',
        'id'   => $prefix . 'dealer_subheadline',
        'type' => 'textarea_small',
        'description' => 'The text that appears under the headline of the page'
      ),
        array(
          'name' => 'Service Areas',
          'id'   => $prefix . 'dealer_description',
          'type' => 'textarea_small',
          'before_row' => '<div class="container-fluid">',
          'description' => 'Add a &lt;br/&gt; to make a line break.',
          'escape_cb' => false,
          'sanitization_cb' => false,
          'description' => 'A list of the dealer\'s territories, only visible on search results. Add a &lt;br/&gt; to make a line break'
        ),
        array(
          'name' => 'Service Cities and Towns Header',
          'id'   => $prefix . 'dealer_serive_areas_header',
          'type' => 'text',
          'placeholder' => 'STATE cities we serve:',
          'description' => 'The headline above the service areas, defaults to "Areas we serve"'
        ),
        array(
          'name' => 'Service Cities and Towns',
          'id'   => $prefix . 'dealer_cities_and_towns',
          'type' => 'textarea_small',
          'description' => 'Add a &lt;br/&gt; to make a line break.',
          'escape_cb' => false,
          'sanitization_cb' => false,
          'description' => 'A list of the dealer\'s serviced cities and towns. Add a &lt;br/&gt; to make a line break'
        ),
        array(
          'name' => 'Service Areas Image',
          'id'   => $prefix . 'dealer_service_areas_image',
          'type' => 'file',
          'allow' => array( 'url', 'attachment' ),
          'description' => 'An image of the dealer\'s territory, should be 600px by 600px for HD support'
        ),
        array(
          'name' => 'Dealer Image',
          'id'   => $prefix . 'dealer_about_us_image',
          'type' => 'file',
          'allow' => array( 'url', 'attachment' ),
          'description' => 'An image of the dealer, should be 600px by 600px for HD support'
        ),
        array(
          'name' => 'Owner Name',
          'id'   => $prefix . 'dealer_owner',
          'type' => 'text',
          'description' => 'The owner(s) of the dealership\'s name(s)'
        ),
        array(
          'name' => 'Primary Phone Number',
          'id'   => $prefix . 'dealer_phone',
          'type' => 'text',
          'row_classes' => 'admin-col col-sm-6',
          'before_row' => '<div class="row">',
        ),
        array(
          'name' => 'Secondary Phone Number',
          'id'   => $prefix . 'dealer_secondary_phone',
          'type' => 'text',
          'row_classes' => 'admin-col col-sm-6',
          'after_row' => '</div>',
        ),
        array(
          'name' => 'Toll Free Phone Number',
          'id'   => $prefix . 'dealer_toll_free',
          'type' => 'text',
        ),
        array(
          'name' => 'Email Address',
          'id'   => $prefix . 'dealer_email',
          'type' => 'text',
        ),
        array(
          'name' => 'Website Link',
          'id'   => $prefix . 'dealer_link',
          'type' => 'text',
          'Link to the dealer\'s website, eg: http://www.dogwatchofpugetsound.com/'
        ),
        array(
					'name' => __( 'Dealer Area Zip Codes', 'cmb2' ),
					'id'   => $prefix . 'dealer_zip_codes',
					'type' => 'textarea_small',
          'after_row' => '</div>',
          'description' => 'These are the zip codes that a dealer will come up for in a zip code search, they should be formatted as a comma-separated list without spaces, eg: 30000,30001,30002,',
				),
        array(
            'name'    => 'About Us',
            'id'   => $prefix . 'dealer_about_us',
            'type'    => 'wysiwyg',
            'options' => array(),
        ),
        array(
        'name'        => 'Testimonials',
        'id'          => $prefix . 'dealer_testimonials',
        'type'        => 'group',
        'options'     => array(
          'group_title'   => 'Testimonial {#}',
          'add_button'    => 'Add Another Testimonial',
          'remove_button' => 'Remove Testimonial',
          'sortable'      => true,
        ),
        'fields'      => array(
          array(
            'name' => 'Testimonial Giver\'s Name',
            'id'   => 'name',
            'type' => 'text',
          ),
          array(
            'name' => 'Testimonial Giver\'s Location',
            'id'   => 'location',
            'type' => 'text',
          ),
          array(
            'name' => 'Testimonial',
            'id'   => 'testimonial',
            'type' => 'textarea',
          ),
        ),
      ),
        array(
				'name'        => 'Trust/Review Symbols and Links',
				'id'          => $prefix . 'dealer_trust_icons',
				'type'        => 'group',
				'options'     => array(
					'group_title'   => 'Symbol {#}',
					'add_button'    => 'Add Another Symbol',
					'remove_button' => 'Remove Symbol',
					'sortable'      => true,
				),
				'fields'      => array(
					array(
						'name' => 'Symbol Name',
						'id'   => 'name',
						'type' => 'text',
					),
					array(
						'name' => 'Symbol Image',
						'id'   => 'image',
						'type' => 'file',
						'allow' => array( 'url', 'attachment' ),
					),
					array(
						'name' => 'Symbol URL',
						'id'   => 'url',
						'type' => 'text',
					),
					array(
						'name' => 'Symbol HTML',
						'desc' => 'If there is a symbol image, it will take presidence over this',
						'id'   => 'html',
						'type' => 'textarea',
						'escape_cb' => false,
						'sanitization_cb' => false,
					),
				),
			),
      array(
        'name' => __( 'Review Widget Code', 'cmb2' ),
        'id'   => $prefix . 'review_widget_code',
        'type' => 'textarea',
        'escape_cb' => false,
        'sanitization_cb' => false,
        'row_classes' => 'no-border',
      ),
			),
		);


    // ----------------------------------------------------------------------------
    // New product template - START
    // ----------------------------------------------------------------------------

    // Product template new panel options

        $meta_boxes['product_tempate_new_options'] = array(
          'id'         => 'product_template_new_options',
          'title'      => __( 'Template Options', 'cmb2' ),
          'object_types'      => array( 'page', ), // Post type
          'context'    => 'side', //  'normal', 'advanced', or 'side'
          'priority'   => 'high', //  'high', 'core', 'default' or 'low'
          'show_names' => false, // Show field names on the left
          'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product-new.php')),
          'fields'     => array(
            array(
                'description' => 'Selecting an option below will add fields for that specific content to this page.',
                'type' => 'title',
                'id' => $prefix . 'panel_options_title',
                'row_classes' => 'no-border clear-pad',
            ),
            array(
                'name'    => __( 'Options', 'cmb2' ),
                'id'      => $prefix . 'options',
                'type'    => 'multicheck',
                'options' => array(
                  'hero' => __( 'Hero', 'cmb2' ),
                  'ribbon_panel' => __( 'Ribbon Panel', 'cmb2' ),
                  'featured_panel' => __( 'Featured Panel', 'cmb2' ),
                  'text_panel' => __( 'Text Panel', 'cmb2' ),
                  'info_table_new' => __( 'Info Table', 'cmb2' ),
                  'product_hero_new' => __( 'Product Hero (New)', 'cmb2' ),
                  'trustbox_panel' => __( 'Trustbox Panel', 'cmb2' ),
                  'find_a_dealer' => __( 'Find a Dealer', 'cmb2' ),
                  'slider_panel_new' => __( 'Slider Panel (New)', 'cmb2' ),
                  'customer_story' => __( 'Customer Story', 'cmb2' ),
                  'cta_panel' => __( 'CTA Panel', 'cmb2' ),
                  'image_nav_panel' => __( 'Image Nav Panel', 'cmb2' ),
                  'photo_text_panel' => __( 'Photo with Text Panel', 'cmb2'),
                  'other_video_panel' => __( 'Video Panel', 'cmb2'),
                  'faq_panel' => __('FAQ Panel', 'cmb2'),
                  'footer_image' => __( 'Footer Image', 'cmb2' ),
                ),
              ),
            ),
          );

      // Product template new content options

          $meta_boxes['new_product_specific_content'] = array(
            'id'         => 'new_product_specific_content',
            'title'      => __( 'Product Content', 'cmb2' ),
            'object_types'      => array( 'page', ), // Post type
            'context'    => 'normal', //  'normal', 'advanced', or 'side'
            'priority'   => 'high', //  'high', 'core', 'default' or 'low'
            'show_names' => true, // Show field names on the left
            'show_on'    => array( 'key' => 'page-template', 'value' => 'page-product-new.php'),

            'fields'     => array(
              array(
                'name' => __( 'Product Headline', 'cmb2' ),
                'id'   => $prefix . 'product_headline',
                'type' => 'text',
                'escape_cb' => false,
                'sanitization_cb' => false,
                'row_classes' => 'no-border',
              ),
              array(
                'name' => __( 'Product Sub Headline', 'cmb2' ),
                'id'   => $prefix . 'product_sub_headline',
                'type' => 'textarea_small',
                'escape_cb' => false,
                'sanitization_cb' => false,
              ),
              array(
                'name' => __( 'Product Video ID', 'cmb2' ),
                'id'   => $prefix . 'product_video_url',
                'type' => 'text',
                'description' => 'Get the video ID from the YouTube URL and paste it here, eg: 4RXl_7PZPNg',
                'escape_cb' => false,
                'sanitization_cb' => false,
              ),
                array(
                  'name' => __( 'Product Image', 'cmb2' ),
                  'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
                  'id'   => $prefix . 'product_image',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                ),
                array(
                  'name' => 'Product Image HD',
                  'id'   => $prefix . 'product_image_2x',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                ),
                array(
                    'name'    => __( 'Include Product Info', 'cmb2' ),
                    'id'      => $prefix . 'include_product_info',
                    'type'    => 'select',
                    'options' => array(
                      'yes' => __( 'Yes', 'cmb2' ),
                      'no'   => __( 'No', 'cmb2' ),
                  ),
                  'default' => 'no',
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Logo', 'cmb2' ),
                  'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
                  'id'   => $prefix . 'product_logo',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                ),
                array(
                  'name' => __( 'Product Model', 'cmb2' ),
                  'desc' => __( 'Replaces product title under product image', 'cmb2' ),
                  'id'   => $prefix . 'product_model_number',
                  'type' => 'text',
                ),
                array(
                  'name' => __( 'Product Overview', 'cmb2' ),
                  'id'   => $prefix . 'product_overview',
                    'type'    => 'wysiwyg',
                    'escape_cb' => false,
                    'sanitization_cb' => false,
                    'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'tinymce' => $tinySmall,
                    ),
                ),
                array(
                  'name' => __( 'Includes', 'cmb2' ),
                  'desc' => __( 'Each field is one list item.', 'cmb2' ),
                  'id'   => $prefix . 'product_includes',
                  'type' => 'text',
                  'repeatable' => true,
                ),
                array(
                    'name'    => __( 'Include Product Models', 'cmb2' ),
                    'id'      => $prefix . 'include_product_models',
                    'type'    => 'select',
                    'options' => array(
                      'yes' => __( 'Yes', 'cmb2' ),
                      'no'   => __( 'No', 'cmb2' ),
                  ),
                  'default' => 'no',
                  'row_classes' => 'no-border',
                ),


                array(
                  'name' => __( 'Product Model 1 Name', 'cmb2' ),
                  'id'   => $prefix . 'product_model_1_name',
                  'type' => 'text',
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Model 1 Image', 'cmb2' ),
                  'id'   => $prefix . 'product_model_1_image',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Model 1 Content', 'cmb2' ),
                  'id'   => $prefix . 'product_model_1_content',
                    'type'    => 'wysiwyg',
                    'escape_cb' => false,
                    'sanitization_cb' => false,
                    'options' => array(
                      'media_buttons' => false,
                      'textarea_rows' => 3,
                      'tinymce' => $tinySmall,
                    ),
                ),
                array(
                  'name' => __( 'Product Model 2 Name', 'cmb2' ),
                  'id'   => $prefix . 'product_model_2_name',
                  'type' => 'text',
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Model 2 Image', 'cmb2' ),
                  'id'   => $prefix . 'product_model_2_image',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Model 2 Content', 'cmb2' ),
                  'id'   => $prefix . 'product_model_2_content',
                    'type'    => 'wysiwyg',
                    'escape_cb' => false,
                    'sanitization_cb' => false,
                    'options' => array(
                      'media_buttons' => false,
                      'textarea_rows' => 3,
                      'tinymce' => $tinySmall,
                    ),
                ),
                array(
                  'name' => __( 'Product Model 3 Name', 'cmb2' ),
                  'id'   => $prefix . 'product_model_3_name',
                  'type' => 'text',
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Model 3 Image', 'cmb2' ),
                  'id'   => $prefix . 'product_model_3_image',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                  'row_classes' => 'no-border',
                ),
                array(
                  'name' => __( 'Product Model 3 Content', 'cmb2' ),
                  'id'   => $prefix . 'product_model_3_content',
                    'type'    => 'wysiwyg',
                    'escape_cb' => false,
                    'sanitization_cb' => false,
                    'options' => array(
                      'media_buttons' => false,
                      'textarea_rows' => 3,
                      'tinymce' => $tinySmall,
                    ),
                ),
              ),
            );


    // New info table
    $meta_boxes['info_table_new'] = array(
        'id'         => 'info_table_new',
        'title'      => __( 'Info Table', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'context'    => 'normal', //  'normal', 'advanced', or 'side'
        'priority'   => 'high', //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
        'closed'     => true,
        //'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product-new.php')),
        'fields'     => array(
          array(
            'name' => 'Tab one title',
            'id'   => $prefix . 'tab_one_title',
            'type' => 'text',
            'row_classes' => 'no-border',
          ),
            array(
              'id'          => $prefix . 'info_table_tab_one',
              'type'        => 'group',
              'name'        => 'Tab 1 Rows',
              'options'     => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => __( 'Add Another Row', 'cmb2' ),
                'remove_button' => __( 'Remove Row', 'cmb2' ),
                'sortable'      => true, // beta
              ),
              // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
              'fields'      => array(
                array(
                  'name' => 'Title',
                  'id'   => 'title',
                  'type' => 'text',
                ),
                array(
                  'name' => 'Description Excerpt',
                  'id'   => 'description_excerpt',
                  'type' => 'textarea_small',
                ),
                array(
                  'name' => 'Description Full',
                  'id'   => 'description_full',
                  'type'    => 'wysiwyg',
                  'escape_cb' => false,
                  'sanitization_cb' => false,
                  'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'tinymce' => $tinySmall,
                  ),
                ),
              ),
            ),
            array(
              'name' => 'Tab two title',
              'id'   => $prefix . 'tab_two_title',
              'type' => 'text',
              'row_classes' => 'no-border',
            ),
            array(
              'id'          => $prefix . 'info_table_tab_two',
              'type'        => 'group',
              'name'        => 'Tab 2 Rows',
              'options'     => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => __( 'Add Another Row', 'cmb2' ),
                'remove_button' => __( 'Remove Row', 'cmb2' ),
                'sortable'      => true, // beta
              ),
              // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
              'fields'      => array(
                array(
                  'name' => 'Title',
                  'id'   => 'title',
                  'type' => 'text',
                ),
                array(
                  'name' => 'Description Excerpt',
                  'id'   => 'description_excerpt',
                  'type' => 'textarea_small',
                ),
                array(
                  'name' => 'Description Full',
                  'id'   => 'description_full',
                  'type'    => 'wysiwyg',
                  'escape_cb' => false,
                  'sanitization_cb' => false,
                  'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'tinymce' => $tinySmall,
                  ),
                ),
              ),
            ),
            array(
              'name' => 'Tab three title',
              'id'   => $prefix . 'tab_three_title',
              'type' => 'text',
              'row_classes' => 'no-border',
            ),
            array(
              'id'          => $prefix . 'info_table_tab_three',
              'type'        => 'group',
              'name'        => 'Tab 3 Rows',
              'options'     => array(
                'group_title'   => __( 'Row {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => __( 'Add Another Row', 'cmb2' ),
                'remove_button' => __( 'Remove Row', 'cmb2' ),
                'sortable'      => true, // beta
              ),
              // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
              'fields'      => array(
                array(
                  'name' => 'Title',
                  'id'   => 'title',
                  'type' => 'text',
                ),
                array(
                  'name' => 'Description Excerpt',
                  'id'   => 'description_excerpt',
                  'type' => 'textarea_small',
                ),
                array(
                  'name' => 'Description Full',
                  'id'   => 'description_full',
                  'type'    => 'wysiwyg',
                  'escape_cb' => false,
                  'sanitization_cb' => false,
                  'options' => array(
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'tinymce' => $tinySmall,
                  ),
                ),
              ),
            ),
          ),
      );

      // ----------------------------------------------------------------------
      // Product hero New, AKA 'Romance'
      // ----------------------------------------------------------------------

      $meta_boxes['product_hero_new'] = array(
        'id'         => 'product_hero_new',
        'title'      => __( 'Product Hero (new)', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product-new.php', 'page-home.php', 'default')),
        'context'    => 'normal', //  'normal', 'advanced', or 'side'
        'priority'   => 'default', //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
        'closed'       => true,
        'fields'     => array(
          array(
            'name' => 'Headline',
            'id'   => $prefix . 'product_hero_new_headline',
            'type' => 'text',
          ),
          array(
              'name'    => __( 'Headline color', 'cmb2' ),
              'id'      => $prefix . 'product_hero_new_headline_color',
              'type'    => 'select',
              'options' => array(
                'color-grey dark' => __( 'Grey', 'cmb2' ),
                'color-white'   => __( 'White', 'cmb2' ),
            ),
            'default' => 'color-white'
          ),
            array(
              'name' => __( 'Image', 'cmb2' ),
              'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
              'id'   => $prefix . 'product_hero_new_image',
              'type' => 'file',
              'allow' => array( 'url', 'attachment' ),
            ),
          ),
        );

      // ----------------------------------------------------------------------
      // Customer story
      // ----------------------------------------------------------------------

      $meta_boxes['customer_story'] = array(
        'id'         => 'customer_story',
        'title'      => __( 'Customer Story', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product-new.php', 'page-home.php', 'default')),
        'context'    => 'normal', //  'normal', 'advanced', or 'side'
        'priority'   => 'default', //  'high', 'core', 'default' or 'low'
        'show_names' => false, // Show field names on the left
        'closed'       => true,
        'fields'     => array(
          array(
            'name' => 'Quote',
            'id'   => $prefix . 'customer_story_quote',
            'type' => 'textarea',
          ),
            array(
              'name' => __( 'Image', 'cmb2' ),
              'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
              'id'   => $prefix . 'customer_story_image',
              'type' => 'file',
              'allow' => array( 'url', 'attachment' ),
            ),
          ),
        );

      // ----------------------------------------------------------------------
      // Dealer Difference Panel
      // ----------------------------------------------------------------------

      $meta_boxes['dealer_difference'] = array(
        'id'         => 'dealer_difference',
        'title'      => __( 'Dealer Difference', 'cmb2' ),
        'object_types'      => array( 'page', ), // Post type
        'show_on'    => array( 'key' => 'page-template', 'value' => array('page-home.php')),
        'context'    => 'normal', //  'normal', 'advanced', or 'side'
        'priority'   => 'default', //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
        'closed'       => true,
        'fields'     => array(
          array(
            'name' => 'Headline',
            'id'   => $prefix . 'dealer_difference_headline',
            'type' => 'text',
            'default' => 'Our dealers are the difference',
          ),
          array(
            'name' => 'Description',
            'id'   => $prefix . 'dealer_difference_description',
            'type' => 'textarea',
            'default' => 'DogWatch Dealers know dogs and they know our hidden fence systems inside and out. They are local business owners with a commitment to providing superior customer service. Search for your local dealer today to purchase an outdoor hidden fence:',
          ),
            array(
              'name' => __( 'Image', 'cmb2' ),
              'desc' => __( 'Upload an image or enter a URL. 1440 x 1274', 'cmb2' ),
              'id'   => $prefix . 'dealer_difference_image',
              'type' => 'file',
              'allow' => array( 'url', 'attachment' ),
            ),
          ),
        );

        // --------------------------------------------------------------------
        // Find a Dealer Panel
        // --------------------------------------------------------------------

        $meta_boxes['find_a_dealer'] = array(
          'id'         => 'find_a_dealer',
          'title'      => __( 'Find a Dealer', 'cmb2' ),
          'object_types'      => array( 'page', ), // Post type
          //'show_on'    => array( 'key' => 'page-template', 'value' => array(),
          'context'    => 'normal', //  'normal', 'advanced', or 'side'
          'priority'   => 'default', //  'high', 'core', 'default' or 'low'
          'show_names' => true, // Show field names on the left
          'closed'       => true,
          'fields'     => array(
            array(
              'name' => 'Headline',
              'id'   => $prefix . 'find_a_dealer_headline',
              'type' => 'text',
              'default' => 'How do I buy a SmartFence?',
            ),
            array(
              'name' => 'Description',
              'id'   => $prefix . 'find_a_dealer_description',
              'type' => 'textarea',
              'default' => 'Our Outdoor Hidden Dog Fences are sold by our network of experienced dealers. Search for your local dealer today to enquire about the SmartFence:',
            ),
          ),
        );

// ----------------------------------------------------------------------------
// Page CTA
// ----------------------------------------------------------------------------

$meta_boxes['page_cta'] = array(
  'id'         => 'page_cta',
  'title'      => __( 'Page CTA', 'cmb2' ),
  'object_types'      => array( 'page', ), // Post type
  'context'    => 'normal', //  'normal', 'advanced', or 'side'
  'priority'   => 'low', //  'high', 'core', 'default' or 'low'
  'show_names' => true, // Show field names on the left
  'closed'       => false,
  'fields'     => array(
      array(
        'name' => 'Headline',
        'id'   => $prefix . 'page_cta_headline',
        'type' => 'text',
      ),
      array(
        'name' => 'Button URL',
        'id'   => $prefix . 'page_cta_button_url',
        'type' => 'text',
        'default' => '#'
      ),
      array(
        'name' => 'Button Text',
        'id'   => $prefix . 'page_cta_button_text',
        'type' => 'text',
        'default' => 'Learn More'
      ),
    ),
  );

        $meta_boxes['slider_panel_new'] = array(
          'id'         => 'slider_panel_new',
          'title'      => __( 'Slider Panel (new)', 'cmb2' ),
          'object_types' => array( 'page', ), // Post type
          'show_on'    => array( 'key' => 'page-template', 'value' => array('page-product-new.php', 'default')),
          'context'    => 'normal', //  'normal', 'advanced', or 'side'
          'priority'   => 'default', //  'high', 'core', 'default' or 'low'
          'show_names' => true, // Show field names on the left
          'closed'       => true,
          'fields'     => array(
            array(
              'name' => 'Headline',
              'id'   => $prefix . 'slider_panel_headline',
              'type' => 'text',
            ),
            array(
              'id'          => $prefix . 'slider_panel_new_slides',
              'type'        => 'group',
              'description' => __( 'Add slides.', 'cmb2' ),
              'options'     => array(
                'group_title'   => __( 'Slide {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => __( 'Add Another Slide', 'cmb2' ),
                'remove_button' => __( 'Remove Slide', 'cmb2' ),
                'sortable'      => true, // beta
              ),
              'fields'      => array(
                array(
                  'name' => 'Title',
                  'id'   => 'title',
                  'type' => 'text',
                ),
                array(
                  'name' => 'Description',
                  'id'   => 'description',
                  'type' => 'textarea',
                ),
                array(
                  'name' => 'Image',
                  'id'   => 'image_1x',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                ),
                array(
                  'name' => 'HD Image',
                  'id'   => 'image_2x',
                  'type' => 'file',
                  'allow' => array( 'url', 'attachment' ),
                ),
                array(
                  'name' => 'Nav item class',
                  'id'   => 'nav_item_class',
                  'type' => 'text',
                  'description' => 'Only used when more than one slide is present'
                ),
              ),
            ),
          ),
        );


        $meta_boxes['trustbox_panel'] = array(
          'id'         => 'trustbox_panel',
          'title'      => __( 'Trustbox Panel', 'cmb2' ),
          'object_types' => array( 'page', ), // Post type
          //'show_on'    => array( 'key' => 'page-template', 'value' => array('page-home.php')),
          'context'    => 'normal', //  'normal', 'advanced', or 'side'
          'priority'   => 'default', //  'high', 'core', 'default' or 'low'
          'show_names' => true, // Show field names on the left
          'closed'       => true,
          'fields'     => array(
            array(
              'name' => 'Headline',
              'id'   => $prefix . 'tb_headline',
              'type' => 'text',
            ),
            array(
              'name' => 'Code',
              'id'   => $prefix . 'tb_code',
              'type' => 'textarea',
                      'escape_cb' => false,
        'sanitization_cb' => false,
            ),
                      array(
              'name'    => __( 'Remove top padding?', 'cmb2' ),
              'id'      => $prefix . 'tb_remove_top_padding',
              'type'    => 'select',
              'options' => array(
                'yes' => __( 'Yes', 'cmb2' ),
                'no'   => __( 'No', 'cmb2' ),
            ),
            'default' => 'no'
          ),
          ),
        );


        $meta_boxes['smartfence_ribbon'] = array(
          'id'         => 'smartfence_ribbon',
          'title'      => __( 'Smartfence Ribbon', 'cmb2' ),
          'object_types' => array( 'page', ), // Post type
          'show_on'    => array( 'key' => 'page-template', 'value' => array('page-home.php')),
          'context'    => 'normal', //  'normal', 'advanced', or 'side'
          'priority'   => 'default', //  'high', 'core', 'default' or 'low'
          'show_names' => true, // Show field names on the left
          'closed'       => true,
          'fields'     => array(
            array(
              'name' => 'Headline',
              'id'   => $prefix . 'smartfence_ribbon_headline',
              'type' => 'text',
            ),
            array(
              'name' => 'Subheadline',
              'id'   => $prefix . 'smartfence_ribbon_subheadline',
              'type' => 'text',
            ),
            array(
              'name' => 'Button Text',
              'id'   => $prefix . 'smartfence_ribbon_button_text',
              'type' => 'text',
            ),
            array(
              'name' => 'Button URL',
              'id'   => $prefix . 'smartfence_ribbon_button_url',
              'type' => 'text',
            ),
          ),
        );



    // ----------------------------------------------------------------------------
    // New product template - END
    // ----------------------------------------------------------------------------



	return $meta_boxes;
}

// Load scripts / css into the admin portion of the site
function mytheme_admin_load_scripts($hook) {
    //if( $hook != 'post.php' )
    //    return;

    wp_enqueue_script( 'custom-js', get_template_directory_uri()."/admin/js/template-admin.js" );
    wp_enqueue_style( 'custom-css', get_template_directory_uri()."/admin/css/template-admin.css" );
}
add_action('admin_enqueue_scripts', 'mytheme_admin_load_scripts');


?>
