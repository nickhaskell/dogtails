<?php

  add_action( 'init', 'mytheme_custom_post_dealer' );
  function mytheme_custom_post_dealer() {

      $labels = array(
          'name' => _x( 'Dealers', 'dealer' ),
          'singular_name' => _x( 'Dealer', 'dealer' ),
          'add_new' => _x( 'Add New', 'dealer' ),
          'all_items' => _x( 'Dealers', 'dealer' ),
          'add_new_item' => _x( 'Add New Dealer', 'dealer' ),
          'edit_item' => _x( 'Edit Dealer', 'dealer' ),
          'new_item' => _x( 'New Dealer', 'dealer' ),
          'view_item' => _x( 'View Dealer', 'dealer' ),
          'search_items' => _x( 'Search Dealers', 'dealer' ),
          'not_found' => _x( 'No dealers found', 'dealer' ),
          'not_found_in_trash' => _x( 'No dealers found in Trash', 'dealer' ),
          'parent_item_colon' => _x( 'Parent Dealer:', 'dealer' ),
          'menu_name' => _x( 'Dealers', 'dealer' ),
      );

      $args = array(
          'labels' => $labels,
          'hierarchical' => true,
          'public' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'menu_position' => 20,
          'query_var' => true,
          'has_archive' => true,
          'can_export' => true,
          'capability_type' => 'page',
          'menu_icon' => 'dashicons-store',
          'supports' => array('title', 'page-attributes')
      );

      register_post_type( 'dealer', $args );
  }


  // Custom Taxonomies for Dealers custom post type

  function my_taxonomies_dealer() {
    $labels = array(
      'name'              => _x( 'Dealer Locations', 'taxonomy general name' ),
      'singular_name'     => _x( 'Dealer Location', 'taxonomy singular name' ),
      'search_items'      => __( 'Search Dealer Locations' ),
      'all_items'         => __( 'All Dealer Locations' ),
      'parent_item'       => __( 'Parent Dealer Location' ),
      'parent_item_colon' => __( 'Parent Dealer Location:' ),
      'edit_item'         => __( 'Edit Dealer Location' ),
      'update_item'       => __( 'Update Dealer Location' ),
      'add_new_item'      => __( 'Add New Dealer Location' ),
      'new_item_name'     => __( 'New Dealer Location' ),
      'menu_name'         => __( 'Dealer Locations' ),
    );
    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'show_admin_column' => true,
      'rewrite' => array(
        'slug' => 'dealer-location',
        'hierarchical' => true,
      ),
    );
    register_taxonomy( 'dealer_location', 'dealer', $args );
  }
  add_action( 'init', 'my_taxonomies_dealer', 0 );


  // Filter custom post type by custom taxomony
  function restrict_books_by_genre() {
  		global $typenow;
  		$post_type = 'dealer'; // change HERE
  		$taxonomy = 'dealer_location'; // change HERE
  		if ($typenow == $post_type) {
  			$selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
  			$info_taxonomy = get_taxonomy($taxonomy);
  			wp_dropdown_categories(array(
  				'show_option_all' => __("Show All {$info_taxonomy->label}"),
  				'taxonomy' => $taxonomy,
  				'name' => $taxonomy,
  				'orderby' => 'name',
  				'selected' => $selected,
  				'show_count' => true,
  				'hide_empty' => true,
  			));
  		};
  	}

  	add_action('restrict_manage_posts', 'restrict_books_by_genre');

  	function convert_id_to_term_in_query($query) {
  		global $pagenow;
  		$post_type = 'dealer'; // change HERE
  		$taxonomy = 'dealer_location'; // change HERE
  		$q_vars = &$query->query_vars;
  		if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
  			$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
  			$q_vars[$taxonomy] = $term->slug;
  		}
  	}

  	add_filter('parse_query', 'convert_id_to_term_in_query');

?>
