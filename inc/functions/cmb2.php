<?php

// Get shop posts
function cmb2_get_post_options( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'post',
        'numberposts' => 10,
        'orderby'     => 'title',
        'order'       => 'ASC',
		'meta_key'    => '_wp_page_template',
		'meta_value'  => 'page-product.php'
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }
    return $post_options;
}

// Get posts
function cmb2_get_all_pages( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'post',
        'numberposts' => 10,
        'orderby'     => 'title',
        'order'       => 'ASC',
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }
    return $post_options;
}

function cmb2_is_front( $field ) {
	// Don't show this field if not in the cats category
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// check for a template type
	if ($template_file == 'page-home.php') {
		return true;
	} else {
		return false;
	}
}

function cmb2_is_default( $field ) {
	// Don't show this field if not in the cats category
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// check for a template type
	if ($template_file == 'default') {
		return true;
	} else {
		return false;
	}
}

function cmb2_is_front_page( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/* CUSTOM FIELD INCLUDES **************************************************** */

function cmb2_render_address_field_callback( $field_object, $value, $object_id, $object_type, $field_type_object ) {

    // make sure we specify each part of the value we need.
    $value = wp_parse_args( $value, array(
        'row-title' => '',
        'row-description' => '',
    ) );

    ?>
    <div><p><label for="<?php echo $field_type_object->_id( '_row_title' ); ?>">Row Title</label></p>
        <?php echo $field_type_object->input( array(
            'name'  => $field_type_object->_name( '[row-title]' ),
            'id'    => $field_type_object->_id( '_row_title' ),
            'value' => $value['row-title'],
        ) ); ?>
    </div>
    <div><p><label for="<?php echo $field_type_object->_id( '_row_description' ); ?>'">Row Description</label></p>
        <?php echo $field_type_object->input( array(
            'class' => 'test',
            'name'  => $field_type_object->_name( '[row-description]' ),
            'id'    => $field_type_object->_id( '_row_description' ),
            'value' => $value['row-description'],
            'type'  => 'textarea',
        ) ); ?>
    </div>
    <?php

    echo $field_type_object->_desc( true );

}
add_filter( 'cmb2_render_address', 'cmb2_render_address_field_callback', 10, 5 );



/* ************************************************************************** */




 // Custom Metaboxes
add_filter( 'cmb2_meta_boxes', 'cmb2_mbs' );

function cmb2_mbs( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb2_';

  $tinySmall['toolbar1'] = 'bold, italic, bullist, numlist, link, unlink, hr, formatselect';
  $tinySmall['toolbar2'] = '';

  $tinySmall2['toolbar1'] = 'bold, italic, bullist, numlist, link, unlink, alignleft, aligncenter, hr, formatselect';
  $tinySmall2['toolbar2'] = '';

  $photoWithText['toolbar1'] = 'bold, italic, link, unlink,  alignleft, aligncenter, hr, formatselect';
  $photoWithText['toolbar2'] = '';




// Configs

  $meta_boxes['display_options'] = array(
    'id'         => 'display_options',
    'title'      => __( 'Display Options', 'cmb2' ),
    'object_types'      => array( 'page','post' ), // Post type
    'context'    => 'side', //  'normal', 'advanced', or 'side'
    'priority'   => 'core', //  'high', 'core', 'default' or 'low'
    'show_names' => false, // Show field names on the left
    'show_names' => true, // Show field names on the left
    'closed'       => true,
    'fields'     => array(
      array(
        'name' => __( 'Alternate Headline', 'cmb2' ),
        'id'   => $prefix . 'headline_alt',
        'type' => 'text',
        'row_classes' => 'no-border',
        ),
      array(
        'name' => __( 'Optional Headline', 'cmb2' ),
        'id'   => $prefix . 'headline_optional',
        'type' => 'text',
        'escape_cb' => false,
        'sanitization_cb' => false,
        'row_classes' => 'no-border',
        ),
        array(
					'name' => __( 'Sub Headline', 'cmb2' ),
					'id'   => $prefix . 'headline_sub',
					'type' => 'textarea_small',
          'escape_cb' => false,
          'sanitization_cb' => false,
          'row_classes' => 'no-border',
				),
        array(
					'name' => __( 'Headline Button Link', 'cmb2' ),
					'description' => 'Include a button link under the page headline',
					'id'   => $prefix . 'headline_button_link',
					'type' => 'text',
          'row_classes' => 'no-border',
				),
				array(
					'name' => __( 'Header Button Link Text', 'cmb2' ),
					'description' => 'Text on the header button',
					'id'   => $prefix . 'headline_button_text',
					'type' => 'text',
          'row_classes' => 'no-border',
				),
        array(
          'name' => 'Open button link in new window?',
          'id'   => $prefix . 'headline_button_new_window',
          'type' => 'checkbox'
        ),
        array(
					'name' => __( 'Child List Title', 'cmb2' ),
					'desc' => __( 'Replaces page title on child list template pages', 'cmb2' ),
					'id'   => $prefix . 'page_menu_title',
					'type' => 'text',
          'row_classes' => 'no-border',
				),
        array(
					'name'    => __( 'Hide from Child List Templates', 'cmb2' ),
					'id'      => $prefix . 'hide_from_child_list',
					'type'    => 'radio_inline',
					'options' => array(
						'yes' => __( 'Yes', 'cmb2' ),
						'no'  => __( 'No', 'cmb2' ),
					),
					'default' => 'no',
          'row_classes' => 'no-border',
				),
        array(
					'name'    => __( 'Include Dealer Area Nav', 'cmb2' ),
					'id'      => $prefix . 'dealer_area_nav',
					'type'    => 'radio_inline',
					'options' => array(
						'yes' => __( 'Yes', 'cmb2' ),
						'no'  => __( 'No', 'cmb2' ),
					),
					'default' => 'no',
          'row_classes' => 'no-border',
				),
        array(
          'name' => 'Hide page header?',
          'id'   => $prefix . 'hide_page_header',
          'type' => 'checkbox'
        ),
      ),
    );


// Page specific javascripts

$meta_boxes['Page Specific Scripts'] = array(
  'id'         => 'page_specific_scripts',
  'title'      => __( 'Page Specific Scripts', 'cmb2' ),
  'object_types'      => array( 'page','post' ), // Post type
  'context'    => 'side', //  'normal', 'advanced', or 'side'
  'priority'   => 'low', //  'high', 'core', 'default' or 'low'
  'show_names' => false, // Show field names on the left
  'closed'       => true,
  'fields'     => array(
    array(
        'description' => 'Place page-specific scripts such as conversion codes in the box below. Please include opening and closing &lt;script&gt; tags before and after each piece of code.',
        'type' => 'title',
        'id' => $prefix . 'page_scripts_title',
        'row_classes' => 'no-border clear-pad',
    ),
      array(
        'name' => __( 'Scripts', 'cmb2' ),
        'id'   => $prefix . 'page_scripts',
        'type' => 'textarea',
        'escape_cb' => false,
        'sanitization_cb' => false,
        'row_classes' => 'no-border',
      )
    ),
  );






  






// ----------------------------------------------------------------------------
// Page CTA
// ----------------------------------------------------------------------------

/*
$meta_boxes['page_cta'] = array(
  'id'         => 'page_cta',
  'title'      => __( 'Page CTA', 'cmb2' ),
  'object_types'      => array( 'page', ), // Post type
  'context'    => 'normal', //  'normal', 'advanced', or 'side'
  'priority'   => 'low', //  'high', 'core', 'default' or 'low'
  'show_names' => true, // Show field names on the left
  'closed'       => false,
  'fields'     => array(
      array(
        'name' => 'Headline',
        'id'   => $prefix . 'page_cta_headline',
        'type' => 'text',
      ),
      array(
        'name' => 'Button URL',
        'id'   => $prefix . 'page_cta_button_url',
        'type' => 'text',
        'default' => '#'
      ),
      array(
        'name' => 'Button Text',
        'id'   => $prefix . 'page_cta_button_text',
        'type' => 'text',
        'default' => 'Learn More'
      ),
    ),
  );
  */




    // ----------------------------------------------------------------------------
    // New product template - END
    // ----------------------------------------------------------------------------



	return $meta_boxes;
}

// Load scripts / css into the admin portion of the site
function mytheme_admin_load_scripts($hook) {
    //if( $hook != 'post.php' )
    //    return;

    //wp_enqueue_script( 'custom-js', get_template_directory_uri()."/admin/js/template-admin.js" );
    //wp_enqueue_style( 'custom-css', get_template_directory_uri()."/admin/css/template-admin.css" );
}
add_action('admin_enqueue_scripts', 'mytheme_admin_load_scripts');


?>
