<?php

// Render the info table, info row field

function cmb2_render_address_field_callback( $field_object, $value, $object_id, $object_type, $field_type_object ) {

    // make sure we specify each part of the value we need.
    $value = wp_parse_args( $value, array(
        'row-title' => '',
        'row-description' => '',
    ) );

    ?>
    <div><p><label for="<?php echo $field_type_object->_id( '_row_title' ); ?>">Row Title</label></p>
        <?php echo $field_type_object->input( array(
            'name'  => $field_type_object->_name( '[row-title]' ),
            'id'    => $field_type_object->_id( '_row_title' ),
            'value' => $value['row-title'],
        ) ); ?>
    </div>
    <div><p><label for="<?php echo $field_type_object->_id( '_row_description' ); ?>'">Row Description</label></p>
        <?php echo $field_type_object->input( array(
            'class' => 'crap',
            'name'  => $field_type_object->_name( '[row-description]' ),
            'id'    => $field_type_object->_id( '_row_description' ),
            'value' => $value['row-description'],
            'type'  => 'textarea',
        ) ); ?>
    </div>
    <?php

    echo $field_type_object->_desc( true );

}
add_filter( 'cmb2_render_address', 'cmb2_render_address_field_callback', 10, 5 );
?>
