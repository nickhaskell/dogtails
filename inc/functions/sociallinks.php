<?php

/* SOCIAL MEDIA SETTINGS *******************************/

// —————Add Settings to General Settings—————–
function social_settings_api_init() {
  // Add the section to general settings so we can add our
  // fields to it
  add_settings_section('social_setting_section',
  'Social sites on the web',
  'social_setting_section_callback_function',
  'general');

  // Add the field with the names and function to use for our new
  // settings, put it in our new section
  add_settings_field('general_setting_facebook',
  'Facebook Page',
  'general_setting_facebook_callback_function',
  'general',
  'social_setting_section');

  // Register our setting so that $_POST handling is done for us and
  // our callback function just has to echo the <input>
  register_setting('general','general_setting_facebook');

  add_settings_field('general_setting_twitter',
  'Twitter',
  'general_setting_twitter_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_twitter');

  add_settings_field('general_setting_pinterest',
  'Pinterest',
  'general_setting_pinterest_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_pinterest');

  add_settings_field('general_setting_youtube',
  'YouTube',
  'general_setting_youtube_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_youtube');

  add_settings_field('general_setting_instagram',
  'Instagram',
  'general_setting_instagram_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_instagram');

  add_settings_field('general_setting_gplus',
  'Google Plus',
  'general_setting_gplus_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_gplus');


  add_settings_field('general_setting_blog',
  'Blog',
  'general_setting_blog_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_blog');

  add_settings_field('general_setting_dealer',
  'Locate an International Dealer Page',
  'general_setting_dealer_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_dealer');

  add_settings_field('general_setting_dealer_redirect',
  'Dealer Login Redirect',
  'general_setting_dealer_redirect_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_dealer_redirect');

  add_settings_field('general_setting_cdbb_link',
  'Consumer\'s Digest Best Buy Link',
  'general_setting_cdbb_link_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_cdbb_link');

  add_settings_field('general_setting_store_link',
  'Dealer Store Link',
  'general_setting_store_link_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_store_link');

  add_settings_field('general_setting_email_link',
  'Email Sign up Link',
  'general_setting_email_link_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_email_link');

  add_settings_field('general_setting_dealer_login',
  'Dealer Login Link',
  'general_setting_login_link_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_login_link');

  add_settings_field('general_setting_vendor_login',
  'Vendor Login Link',
  'general_setting_vendor_link_callback_function',
  'general',
  'social_setting_section');
  register_setting('general','general_setting_vendor_link');
}


add_action('admin_init', 'social_settings_api_init');

// —————-Settings section callback function———————-
function social_setting_section_callback_function() {
echo '<p>This section is where you can save the social sites where readers can find you on the Internet.</p>';
}
function general_setting_facebook_callback_function() {
echo '<input name="general_setting_facebook" id="general_setting_facebook" class="regular-text" type="text" value="'. get_option('general_setting_facebook') .'" />';
}
function general_setting_twitter_callback_function() {
echo '<input name="general_setting_twitter" id="general_setting_twitter" class="regular-text" type="text" value="'. get_option('general_setting_twitter') .'" />';
}
function general_setting_pinterest_callback_function() {
echo '<input name="general_setting_pinterest" id="general_setting_pinterest" class="regular-text" type="text" value="'. get_option('general_setting_pinterest') .'" />';
}
function general_setting_youtube_callback_function() {
echo '<input name="general_setting_youtube" id="general_setting_youtube" class="regular-text" type="text" value="'. get_option('general_setting_youtube') .'" />';
}
function general_setting_instagram_callback_function() {
echo '<input name="general_setting_instagram" id="general_setting_instagram" class="regular-text" type="text" value="'. get_option('general_setting_instagram') .'" />';
}
function general_setting_gplus_callback_function() {
echo '<input name="general_setting_gplus" id="general_setting_gplus" class="regular-text" type="text" value="'. get_option('general_setting_gplus') .'" />';
}
function general_setting_blog_callback_function() {
echo '<input name="general_setting_blog" id="general_setting_blog" class="regular-text" type="text" value="'. get_option('general_setting_blog') .'" />';
}
function general_setting_dealer_callback_function() {
echo '<p>International dealer page link, needs to be an absolute url.</p>';
echo '<input name="general_setting_dealer" id="general_setting_dealer" class="regular-text" type="text" value="'. get_option('general_setting_dealer') .'" />';
}
function general_setting_dealer_redirect_callback_function() {
echo '<p>Page for dealers to redirect to on login, needs to be an absolute url.</p>';
echo '<input name="general_setting_dealer_redirect" id="general_setting_dealer_redirect" class="regular-text" type="text" value="'. get_option('general_setting_dealer_redirect') .'" />';
}
function general_setting_cdbb_link_callback_function() {
echo '<p>URL for Consumer\'s Digest Best Buy Seal.</p>';
echo '<input name="general_setting_cdbb_link" id="general_setting_cdbb_link" class="regular-text" type="text" value="'. get_option('general_setting_cdbb_link') .'" />';
}
function general_setting_store_link_callback_function() {
echo '<p>URL for the Dealer Store.</p>';
echo '<input name="general_setting_store_link" id="general_setting_store_link" class="regular-text" type="text" value="'. get_option('general_setting_store_link') .'" />';
}
function general_setting_email_link_callback_function() {
echo '<p>URL for your email sign up form.</p>';
echo '<input name="general_setting_email_link" id="general_setting_email_link" class="regular-text" type="text" value="'. get_option('general_setting_email_link') .'" />';
}

function general_setting_login_link_callback_function() {
echo '<p>URL for the Dealer login page.</p>';
echo '<input name="general_setting_login_link" id="general_setting_login_link" class="regular-text" type="text" value="'. get_option('general_setting_login_link') .'" />';
}
function general_setting_vendor_link_callback_function() {
echo '<p>URL for the Vendor login.</p>';
echo '<input name="general_setting_vendor_link" id="general_setting_vendor_link" class="regular-text" type="text" value="'. get_option('general_setting_vendor_link') .'" />';
}
 ?>
