<?php
  /*
  $name       = get_post_meta( $id, '_cmb2_dealer_name', true );
  $location   = get_post_meta( $id, '_cmb2_dealer_description', true );
  $owner      = get_post_meta( $id, '_cmb2_dealer_owner', true );
  $phone      = get_post_meta( $id, '_cmb2_dealer_phone', true );
  $secondary  = get_post_meta( $id, '_cmb2_dealer_secondary_phone', true );
  $toll_free  = get_post_meta( $id, '_cmb2_dealer_toll_free', true );
  $email      = get_post_meta( $id, '_cmb2_dealer_email', true );
  $link       = get_post_meta( $id, '_cmb2_dealer_link', true );
  */
?>
<?php
/*
<div class="dealers">
  <div class="row">
			<div class="dealer col-sm-6">
				<?php if ($name): ?>
					<h4><?php echo $name; ?></h4>
				<?php endif ?>
				<ul class="dealer-meta unstyled">
					<?php if ($location): ?>
						<li class="desc"><?php echo $location ?></li>
					<?php endif ?>
					<?php if ($owner): ?>
						<li><span class="subhead"><?php echo $owner ?></span></li>
					<?php endif ?>
					<?php if ($phone): ?>
						<li><span class="subhead green"><?php echo $phone ?></span></li>
					<?php endif ?>
					<?php if ($secondary): ?>
						<li><span class="subhead green"><?php echo $secondary ?></span></li>
					<?php endif ?>
					<?php if ($toll_free): ?>
						<li><span class="subhead green">Toll-free: <?php echo $toll_free ?></span></li>
					<?php endif ?>
					<?php if ($email): ?>
						<li><span class="subhead green"><a href="mailto: <?php echo $email ?>" target="_blank"><?php echo $email ?> </a></span></li>
					<?php endif ?>
					<?php if ($link): ?>
						<li><span class="subhead green"><a href=" <?php echo $link ?> " target="_blank"><?php echo $link ?></a></span></li>
					<?php endif ?>
				</ul>
			</div>
    </div>
</div>
*/
?>

<?php get_template_part('inc/partials/dealer-single'); ?>
