<?php
/**
 * The default template for displaying content
 *
 * @package Shikoku_Inu
 */
	if (get_post_meta( $id, '_yoast_wpseo_metadesc', true ) != "") {
		$meta_desc = get_post_meta( $id, '_yoast_wpseo_metadesc', true );
	}
?>
<div id="post-<?php the_ID(); ?>" class="search-item entry-content item-index-<?php echo $wp_query->current_post; ?>">
	<div class="container pad-b-30 ">
		<div class="item pad-b-30">
			<div class="search-item-inner">
				<div class="search-entry-header unmargin-last">
					<?php if (get_the_date() != ""):?>
					<div class="entry-date"><strong><?php echo get_the_date();?></strong></div>
					<?php endif;?>
					<h3 class="entry-title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h3>
				</div>
				<?php if (isset($meta_desc) && $meta_desc): ?>
					<div class="search-entry-content unmargin-last">
						<?php echo $meta_desc; ?>
					</div>
				<?php else: ?>
					<div class="search-entry-content unmargin-last">
						<?php the_excerpt(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
  	</div>
</div>
