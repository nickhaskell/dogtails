<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

 $hero_text     = woo_settings_get_option('woo_settings_hero_text');
 $sub_hero_text = woo_settings_get_option('woo_settings_sub_hero_text');
 /*
 $bg_images     = woo_settings_get_option('woo_settings_hero_images');
 $bg_image_id   = $bg_images[array_rand($bg_images, 1)]['image_id'];
 $bg_image_1x   = wp_get_attachment_image_src( $bg_image_id, 'hero_misc@1x' );
 $bg_image_2x   = wp_get_attachment_image_src( $bg_image_id, 'hero_misc@2x' );
*/

get_header( 'shop' ); ?>

<?php // include(locate_template("/inc/partials/store-hero.php")); ?>
<?php include(locate_template("/inc/partials/store-zip-lookup-slideout.php")); ?>

<div class="template-default woocommerce-storefront">
  <div class="container pad-b-120 pad-t-30">  

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
	
	
 	<div class="row shop-header">
		<div class="col-sm-12">
	    	<div class="woocommerce-products-header">
	
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

					<h1 class="woocommerce-products-header__title page-title">
						<?php woocommerce_page_title(); ?>
						<?php if ($hero_text) : ?>
						<br/><small>
						<a href="<?php echo wc_get_page_permalink( 'shop' ); ?>">
							<?php echo $hero_text; ?>
						</a>
						</small>
						<?php endif; ?>
					</h1>

				<?php endif; ?>

				<?php
					do_action( 'woocommerce_archive_description' );
				?>

		    </div>
		</div>
	</div> 



	<?php if (is_archive() && has_nav_menu( 'shop' )) : ?>
	<div class="row shop-nav">
		<div class="col-sm-12">
           <?php
             wp_nav_menu( array(
                 'menu'              => 'shop',
                 'theme_location'    => 'shop',
                 'depth'             => 1,
                 'container'         => '',
                 'container_class'   => '',
                 'container_id'      => '',
				 'menu_class'        => 'nav'
			 )
             );
           ?>
		</div>
	</div>
	<?php endif; ?>

		<?php if ( have_posts() ) : ?>


		<div class="row shop-products-meta">
			<div class="col-sm-12">

				<?php
					/**
					 * woocommerce_before_shop_loop hook.
					 *
					 * @hooked wc_print_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
				?>

			</div>
		</div>

		<div class="row shop-products">
			<div class="col-sm-12">

				<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php
							/**
							 * woocommerce_shop_loop hook.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							do_action( 'woocommerce_shop_loop' );
						?>

						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php
					/**
					 * woocommerce_after_shop_loop hook.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php
					/**
					 * woocommerce_no_products_found hook.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				?>

			<?php endif; ?>

		<?php
			/**
			 * woocommerce_after_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
		?>

		<?php
			/**
			 * woocommerce_sidebar hook.
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action( 'woocommerce_sidebar' );
		?>
						</div>
			</div>
		</div>
	</div>

<?php get_footer( 'shop' ); ?>
