<?php
/*
	Template Name: Blog Template
*/

if(!is_user_logged_in()) {
	wp_redirect( get_option('general_setting_login_link'), 302 );
}
?>
<?php get_header(); ?>
<!--noptimize-->
<style media="screen">
	<?php include "dist/styles/templates/blog.css"; ?>
</style>
<!--/noptimize-->
<?php include "inc/meta-vars.php"; // Need this to be an include so vars can be used in partials ?>
<div class="template-default template-blog">
	<div class="sub-page">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php include 'inc/partials/dealer-nav.php'; ?>
			<?php include 'inc/partials/entry-header.php'; ?>

			<div class="entry-content">
        <div class="container">
          <div class="row">
            <div class="col-sm-9 blog-posts-feed">
              <?php // Display blog posts on any page @ http://m0n.co/l
              $temp = $wp_query; $wp_query= null;
              $wp_query = new WP_Query(); $wp_query->query('showposts=6' . '&paged='.$paged);
              while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

              <?php get_template_part( 'content', 'postfeed' ); ?>

              <?php endwhile; ?>
              <?php get_template_part( 'inc/pagination' ); ?>
            </div>


            <div class="col-sm-3 blog-posts-sidebar">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('blog-widgets-right') ) : ?>
                <?php the_widget('blog-widgets-right'); ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>



      <?php wp_reset_postdata(); ?>

    </article>
  </div>
</div>

<?php get_footer(); ?>
