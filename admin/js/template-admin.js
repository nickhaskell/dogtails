(function( $ ) {

  // Initial load functions
  $(document).ready(function() {
    adminLoads();
  });

  // Load checks / handlers
  function adminLoads() {
    console.log("Loading admin scripts.");
    checkForTemplateOptions();
    templateOptionsHandler();

    flexPanelSelectionHandler();
  }

  // Check for specific show/hides on load
  function templateOptionsHandler () {
    $('#template_options input, #home_template_options input, #child_list_template_options input, #product_template_options input, #product_template_new_options input, #fork_template_options input, #store_template_options input').on('change', function(){
      validateShowHide($(this));
    });

    $('#_cmb2_main_content_columns').on('change', function(){
      validateColumnCount($(this));
    });
  }

  // Check for specific show/hides on load
  function checkForTemplateOptions () {
    $('#template_options input, #home_template_options input, #child_list_template_options input, #product_template_options input, #product_template_new_options input, #fork_template_options input, #store_template_options input').each(function(){
      validateShowHide($(this));
    });

    $('#_cmb2_main_content_columns').each(function(){
      validateColumnCount($(this));
    });
  }

  // Validates the item and if the item is checked it will show
  // it's target, otherwise it will hide it.
  function validateShowHide (item) {
    var input_val = item.val();
    var target    = $('#' + input_val);
    var checked   = item.is(':checked');

    var parent    = item.closest('li');
    var anchor    = '<span class="jump-link"> - (<a href="#' + input_val + '">Link</a>)</span>';

    // Check against bool
    if (checked) {
      target.show();
      parent.append(anchor);
    } else {
      target.hide();
      parent.find('.jump-link').remove();
    }
  }

  function validateColumnCount (item) {
    var input_val = item.val();
    var target_parent = $('#main_content_area');
    var one_col = target_parent.find('#main_content_one');
    var two_col = target_parent.find('#main_content_two');

    if (input_val == "one" || input_val == "one-fw" || input_val == "three-q") {
      one_col.show();
      two_col.hide();
    } else {
      one_col.hide();
      two_col.show();
    }
  }

  function flexPanelSelectionHandler () {
    $('.cmb-type-select select').on('change', function() {
      console.log($(this).val());
    });
  }

})(jQuery);
