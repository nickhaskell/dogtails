<?php
/**
 * Search form template
 *
 * @package Shikoku_Inu
 */
?>
<form method="get" class="sitesearch combined-input" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="field combined-input-input" name="s" id="s" placeholder="Search" maxlength="40" />
	<button type="submit" class="submit combined-input-button" name="submit"><span class="icon-search-2"></span></button>
</form>
