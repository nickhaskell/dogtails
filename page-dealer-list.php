<?php
/**
 * Template Name: Dealers List Template
 *
 * @package Shikoku_Inu
 */
include 'inc/partials/role-redirect.php';
get_header(); ?>

<?php get_template_part( 'inc/partials/hero' ); ?>

<div class="template-dealer-list template-default">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

		<?php endwhile; // end of the loop. ?>

</div>
<?php get_footer(); ?>
