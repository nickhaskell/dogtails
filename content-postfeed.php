<?php
/**
 * The default template for displaying content
 *
 * @package Shikoku_Inu
 */
?>

<div id="post-<?php the_ID(); ?>" class="blog-post pad-b-50">

		<div class="entry-header">
			<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'shikoku-inu' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h3>
			<p>Posted on <strong><?php echo get_the_date(); ?></strong></p>
		</div><!-- .entry-header -->

		<div class="entry-content pad-b-30">
			<?php the_content(); ?>

		</div><!-- .entry-content -->
		<hr/>
</div><!-- #post-<?php the_ID(); ?> -->
